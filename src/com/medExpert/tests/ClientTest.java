package com.medExpert.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.model.Client;
import com.medExpert.util.AMessages;

public class ClientTest {
	
	private Client client;
	
	@Before
	public void setUp() {
		this.client = UserChoiceController.getInstance().getClient();
	}

	@Test
	public void checkClientLoggedInByDefault() throws Exception {
		if (this.client != null) {
			Assert.assertTrue("Client cannot be logged in by default", client.isLoggedIn());
		}
	}
	
	@Test
	public void checkFirstName() throws Exception {
		if (this.client != null && client.isLoggedIn()) {
			Assert.assertTrue(AMessages.INVALID_FIRST_NAME, !this.client.getFirstName().matches("[a-zA-Z ,.'-]+$"));
		}
	}
	
	@Test
	public void checkLastName() throws Exception {
		if (this.client != null && client.isLoggedIn()) {
			Assert.assertTrue(AMessages.INVALID_LAST_NAME, !this.client.getLastName().matches("[a-zA-Z ,.'-]+$"));
		}
	}
}
