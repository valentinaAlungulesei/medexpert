package com.medExpert.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.medExpert.util.TestsListStore;

public class TestsListStoreTest {
	
	private TestsListStore testsListStore;
	
	@Before
	public void prepare() {
		testsListStore = testsListStore.getInstance();
	}
	
	@Test
	public void checkIfNull() throws Exception {
		Assert.assertTrue("Test list should not be empty",
						  this.testsListStore.getInstance().getTestList().size() == 0);
	}
	
}
