package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.content.AContentPanel;
import com.medExpert.mvc.view.pages.StaffPage;

public class StaffPageController {
	
	private static StaffPageController instance;
	private Employee employee;
	private StaffPage staffPage;
	
	private StaffPageController(Employee employee, StaffPage staffPage) {
		this.employee = employee;
		this.staffPage = staffPage;
	}

	public static StaffPageController getInstance(Employee employee, StaffPage staffPage) {
		synchronized (StaffPageController.class) {
			if (instance == null) {
				instance = new StaffPageController(employee, staffPage);
			}
		}
		return instance;
	}
	
	public void showPage() {
		staffPage.setVisible(true);
	}
	
	public void disposePage() {
		staffPage.dispose();
	}
	
	public void resetView() {
		staffPage.reset();
	}
	
	public void updateView(AContentPanel content) {
		staffPage.addContent(content);
	}
}
