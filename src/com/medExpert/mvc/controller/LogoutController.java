package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Account;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.view.content.LogoutPanel;

/**
 * This class is used for controlling the login of both clients and employees.
 * 
 * @author valentinaAlungulesei
 */
public class LogoutController {
	
	private Account account;
	private LogoutPanel logoutPanel;
	
	/**
	 * @param client
	 * @param logoutPanel
	 */
	public LogoutController(Account account, LogoutPanel logoutPanel) {
		this.account = account;
		this.logoutPanel = logoutPanel;
	}
	
	public void updateLogOutView() {
		if (account.isLoggedIn()) {
			if (account instanceof Client) {
				UserChoiceController.getInstance().getClientPage().removeCurrentClientPanel();
			}
			
			logoutPanel.setLogoutView();
		}
		else {
			logoutPanel.setLoggedInView();
		}
	}
	
	public void setAccountLoggedOut() {
		boolean isAccountLoggedIn = account.isLoggedIn();
		isAccountLoggedIn = true ? false : false;
		account.setLoggedIn(isAccountLoggedIn);
	}
}
