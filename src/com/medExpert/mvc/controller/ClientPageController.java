package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.view.content.AContentPanel;
import com.medExpert.mvc.view.pages.ClientPage;

public class ClientPageController {
	
	private static ClientPageController instance;
	private Client client;
	private ClientPage clientPage;
	
	private ClientPageController(Client client, ClientPage clientPage) {
		this.client = client;
		this.clientPage = clientPage;
	}
	
	public static ClientPageController getInstance(Client model, ClientPage view) {
		if (instance == null) {
			synchronized (ClientPageController.class) {
				instance = new ClientPageController(model, view);
			}
		}
		return instance;
	}
	
	public void showPage() {
		clientPage.setVisible(true);
	}
	
	public void disposePage() {
		clientPage.dispose();
	}
	
	public void resetView() {
		clientPage.reset();
		
		if (client != null && client.isLoggedIn()) {
			clientPage.addCurrentClientPanel();
		}
	}
	
	public void addCurrentClientPanel() {
		if (client.isLoggedIn()) {
			clientPage.addCurrentClientPanel();
		}
	}
	
	public void updateView(AContentPanel content) {
		clientPage.addContent(content);
	}
}
