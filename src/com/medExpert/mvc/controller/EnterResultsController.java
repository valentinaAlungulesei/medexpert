package com.medExpert.mvc.controller;

import java.util.List;

import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.content.EnterResultsPanel;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.Util;

public class EnterResultsController {
	
	private EnterResultsPanel panel;
	private String clientPersonalIdHash;
	private int clientId;
	private List<Appointment> appointmentsList;
	private List<Test> testsList;
	private boolean isResultEmpty;
	
	public EnterResultsController(EnterResultsPanel panel) {
		this.panel = panel;
	}
	
	public void updateLoginRequestView() {
		if (!UserChoiceController.getInstance().getEmployee().isLoggedIn()) {
			panel.remove(panel.getClientPersonalIdField());
			panel.remove(panel.getSearchAppointmentsButton());
			panel.setLoginRequestView();
		}
	}
	
	public void updateSearchAppointmentsView() {

		if (panel.getClientPersonalIdField().getText().isEmpty()) {
			panel.setEmptyClientPersonalIdView();
		}
		else if (!panel.getClientPersonalIdField().getText().matches("([0-9]){13,13}$")) {
			panel.setDigitsLimitPersonalIdInputView();
		}
		else if (!MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(
				  panel.getClientPersonalIdField().getText()))) {
			panel.setUnknownClientPersonalIdView();
		}
		else if (MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(
				 panel.getClientPersonalIdField().getText()))) {

			clientPersonalIdHash = Util.getInstance().hashPersonalData(panel.getClientPersonalIdField().getText());
			clientId = MedExpertDB.getInstance().getClientIdByPersonalIdHash(clientPersonalIdHash);
			appointmentsList = MedExpertDB.getInstance().getAppointmentsByClientId(clientId);
			panel.setSearchAppointmentsView(appointmentsList);
		}
	}
	
	public void updateTestsListView() {
		testsList = MedExpertDB.getInstance().getTestListByAppointmentId(panel.getSelectedAppointment().getId());
		panel.setTestsListView(testsList);
	}
	
	public void updateUnselectedAppointmentView() {
		panel.setUnselectedAppointmentView();
	}
	
	public void updateResultsInputView() {
	
		isResultEmpty = false;
		
		for (int index = 0; index < panel.getSelectedAppointment().getTestsList().size(); index++) {
			if (String.valueOf(panel.getTestsTable().getValueAt(index, 1)).isEmpty()) {
				isResultEmpty = true;
			}
		}
		
		if (isResultEmpty) {
			panel.setEmptyResultView();
		}
		else {
			panel.setValidResultsView();
		}
	}
	
	public void saveEnteredResults() {
		if (!isResultEmpty) {
			for (int index = 0; index < panel.getSelectedAppointment().getTestsList().size(); index++) {
		
				double result = Double.parseDouble(panel.getTestsTable().getValueAt(index, 1).toString());
				String testName = panel.getSelectedAppointment().getTestsList().get(index).trim();

				MedExpertDB.getInstance().updateTestResultByName(result, testName);
			}
		}
	}
}
