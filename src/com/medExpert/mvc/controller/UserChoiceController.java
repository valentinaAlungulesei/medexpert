package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.content.UserChoicePanel;
import com.medExpert.mvc.view.pages.ClientPage;
import com.medExpert.mvc.view.pages.StaffPage;

public class UserChoiceController {
	
	private static UserChoiceController instance;
	private Client client;
	private ClientPage clientPage;
	private Employee employee;
	private StaffPage staffPage;
	
	private UserChoiceController() {}
	
	public static UserChoiceController getInstance() {
		synchronized (UserChoiceController.class) {
			if (instance == null) {
				instance = new UserChoiceController();
			}
		}
		return instance;
	}
	
	public void updateClientChoiceView() {
		client = new Client();
		clientPage = new ClientPage();
		
		HomePageController.getInstance().disposeHomePage();
		ClientPageController.getInstance(client, clientPage).showPage();
	}
	
	public void updatePassCodeAreaView() {
		getUserChoicePanel().showPassCodeArea();
	}
	
	public void updateStaffPageView() {
		if (String.valueOf(getUserChoicePanel().getPassCodeField().getPassword()).equals("passMeIn")) {
			
			getUserChoicePanel().getPassCodeField().setText("");
			
			employee = new Employee();
			staffPage = new StaffPage();
			
			HomePageController.getInstance().disposeHomePage();
			StaffPageController.getInstance(employee, staffPage).showPage();
		}
		else {
			getUserChoicePanel().setWrongPassCodeView();
		}
	}
	
	public void resetView() {
		getUserChoicePanel().reset();
	}
	
	public Client getClient() {
		return client;
	}
	
	public void logOutClient() {
		client = null;
	}
	
	public ClientPage getClientPage() {
		return clientPage;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public void logOutEmployee() {
		employee = null;
	}
	
	public StaffPage getStaffPage() {
		return staffPage;
	}
	
	private UserChoicePanel getUserChoicePanel() {
		return HomePageController.getInstance().getHomePage().getUserChoicePanel();
	}
}
