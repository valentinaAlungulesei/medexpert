package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Account;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.content.LoginPanel;
import com.medExpert.repository.MedExpertDB;

/**
 * This class is used for controlling the login of both clients and employees.
 * 
 * @author valentinaAlungulesei
 */
public class LoginController {
	
	private Account account;
	private LoginPanel panel;
	
	/**
	 * @param account
	 * @param loginPanel
	 */
	public LoginController(Account account, LoginPanel loginPanel) {
		this.account = account;
		this.panel = loginPanel;
	}
	
	public void updateCredentialsValidationView() {
		if (account instanceof Client) {
			validateClientInput();
		}
		else if (account instanceof Employee) {
			validateEmployeeInput();
		}
	}
	
	public void setCurrentAccountData() {
		if (account instanceof Client) {
			if (MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash()) &&
				MedExpertDB.getInstance().getClientsLoginInfo().get(panel.getPersonalIdHash())
				.equals(panel.getPasswordHash())) {

				Client dbClient = MedExpertDB.getInstance().getClientByIdHash(panel.getPersonalIdHash());

				account.setId(dbClient.getId());
				((Client)account).setPersonalId(dbClient.getPersonalIdHash());
				account.setFirstName(dbClient.getFirstName());
				account.setLastName(dbClient.getLastName());
				((Client)account).setAge(dbClient.getAge());
				((Client)account).setGender(dbClient.getGender());
				account.setLoggedIn(true);
				
				ClientPageController.getInstance((Client)account, UserChoiceController.getInstance().getClientPage()).addCurrentClientPanel();
			}
		}
		else if (account instanceof Employee) {
		
			
			if (MedExpertDB.getInstance().getEmployeesLoginInfo().containsKey(panel.getUsernameField().getText()) &&
				MedExpertDB.getInstance().getEmployeesLoginInfo().get(panel.getUsernameField().getText()).equals(
				panel.getPasswordHash())) {

				int id = MedExpertDB.getInstance().getEmployeeIdByUsername(panel.getUsernameField().getText());
				Employee dbEmployee = MedExpertDB.getInstance().getEmployeeDataById(id);
				
				account.setId(id);
				account.setFirstName(dbEmployee.getFirstName());
				account.setLastName(dbEmployee.getLastName());
				((Employee)account).setDesignation(dbEmployee.getDesignation());
				((Employee)account).setSalaryRange(dbEmployee.getSalaryRange());
				account.setLoggedIn(true);
			}
		}
	}
	
	private void validateClientInput() {
		// Personal id input.
		if (panel.getPersonalIdHash() == null) {
			panel.setEmptyPersonalIdView();
		}
		else if (!panel.getPersonalIDField().getText().matches("([0-9]){13,13}$")) {
			panel.setPersonalIdDigitLimit();
		}
		else if (!MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash())) {
			panel.setUnknownPersonalIdView();
		}
		else if (!MedExpertDB.getInstance().getClientsLoginInfo().get(panel.getPersonalIdHash())
				.equals(panel.getPasswordHash()) && panel.getPasswordHash() != null) {
			panel.setWrongCredentialsView(true);
		}
		else if (MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash())) {
			panel.setValidPersonalIdView();
		}

		// Password input.
		if (panel.getPasswordHash() == null) {
			panel.setEmptyPasswordView();
		}
		else if (MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash()) &&
				 panel.getPasswordHash() == null) {
			panel.setEmptyPasswordView();
		}
		else if (MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash()) &&
				!MedExpertDB.getInstance().getClientsLoginInfo().get(panel.getPersonalIdHash())
				 .equals(panel.getPasswordHash())) {
			panel.setWrongCredentialsView(true);
		} 
		else {
			panel.setValidPasswordView();
		}
		
		// Valid input.
		if (MedExpertDB.getInstance().getClientsLoginInfo().containsKey(panel.getPersonalIdHash()) &&
			MedExpertDB.getInstance().getClientsLoginInfo().get(panel.getPersonalIdHash())
		    .equals(panel.getPasswordHash())) {
			panel.setValidCredentialsInputView();
		}
	}
	
	private void validateEmployeeInput() {
		// Username field.
		if (panel.getUsernameField().getText().isEmpty()) {
			panel.setEmptyUsernameView();
		}
		else if (!MedExpertDB.getInstance().getEmployeesLoginInfo().containsKey(panel.getUsernameField().getText())) {
			panel.setUnknownUsernameView();
		}
		else if (!MedExpertDB.getInstance().getEmployeesLoginInfo().get(panel.getUsernameField().getText()).equals(
				 panel.getPasswordHash()) && panel.getPasswordHash() != null) {
			panel.setWrongCredentialsView(false);
		}
		else {
			panel.setValidUsernameView();
		}
		
		// Password field.
		if (panel.getPasswordHash() == null) {
			panel.setEmptyPasswordView();
		}
		else if (MedExpertDB.getInstance().getEmployeesLoginInfo().containsKey(panel.getUsernameField().getText()) &&
				 panel.getPasswordHash() == null) {
			panel.setEmptyPasswordView();
		}
		else if (MedExpertDB.getInstance().getEmployeesLoginInfo().containsKey(panel.getUsernameField().getText()) &&
				!MedExpertDB.getInstance().getEmployeesLoginInfo().get(panel.getUsernameField().getText()).equals(
				 panel.getPasswordHash())) {
			panel.setWrongCredentialsView(false);
		} 
		else {
			panel.setValidPasswordView();
		}
		
		if (MedExpertDB.getInstance().getEmployeesLoginInfo().containsKey(panel.getUsernameField().getText()) &&
			MedExpertDB.getInstance().getEmployeesLoginInfo().get(panel.getUsernameField().getText()).equals(
	     	panel.getPasswordHash())) {
			panel.setValidCredentialsInputView();
		}
	}
}
