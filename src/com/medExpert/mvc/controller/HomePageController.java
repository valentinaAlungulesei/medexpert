package com.medExpert.mvc.controller;

import com.medExpert.mvc.view.pages.HomePage;

public class HomePageController {

	private static HomePageController instance;
	private HomePage homePage;
	
	private HomePageController() {}
	
	public static HomePageController getInstance() {
		if (instance == null) {
			synchronized (HomePageController.class) {
				instance = new HomePageController();
			}
		}
		return instance;
	}
	
	public void showHomePage() {
		getHomePage().setVisible(true);
	}
	
	public void disposeHomePage() {
		getHomePage().dispose();
	}
	
	public HomePage getHomePage() {
		if (homePage == null) {
			homePage = new HomePage();
		}
		return homePage;
	}
}
