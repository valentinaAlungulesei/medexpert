package com.medExpert.mvc.controller;

import java.util.List;

import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.content.ViewResultsPanel;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.Util;

public class ViewResultsController {
	
	private ViewResultsPanel panel;
	private String clientPersonalIdHash;
	private int clientId;
	private List<Appointment> appointmentsList;
	private List<Test> testsList;
	private Appointment appointment;
	
	public ViewResultsController(ViewResultsPanel panel) {
		this.panel = panel;
	}
	
	public void updateLoginRequestView() {
		if (getCurrentEmployee() != null && !getCurrentEmployee().isLoggedIn() || 
			getCurrentClient() != null && !getCurrentClient().isLoggedIn()) {
			panel.remove(panel.getAppointmentsInfoLabel());
			
			if (panel.getAppointmentsScrollPane() != null) {
				panel.remove(panel.getAppointmentsScrollPane());
			}
		
			panel.remove(panel.getClientPersonalIdField());
			panel.remove(panel.getSearchAppointmentsButton());
			panel.setLoginRequestView();
		}
	}
	
	public void updateSearchAppointmentsView() {
		if (panel.getClientPersonalIdField().getText().isEmpty()) {
			panel.setEmptyClientPersonalIdView();
		}
		else if (!panel.getClientPersonalIdField().getText().matches("([0-9]){13,13}$")) {
			panel.setDigitsLimitPersonalIdInputView();
		}
		else if (!MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(
				  panel.getClientPersonalIdField().getText()))) {
			panel.setUnknownClientPersonalIdView();
		}
		else if (MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(
				 panel.getClientPersonalIdField().getText()))) {

			clientPersonalIdHash = Util.getInstance().hashPersonalData(panel.getClientPersonalIdField().getText());
			clientId = MedExpertDB.getInstance().getClientIdByPersonalIdHash(clientPersonalIdHash);
			appointmentsList = MedExpertDB.getInstance().getAppointmentsByClientId(clientId);
			panel.setSearchAppointmentsView(appointmentsList);
		}
	}

	public void updateAppointmentTestsView() {
		testsList = MedExpertDB.getInstance().getTestListByAppointmentId(panel.getSelectedAppointment().getId());
		panel.setTestsListView(testsList);
	}
	
	public void updateUnselectedAppointmentView() {
		panel.setUnselectedAppointmentView();
	}

	private Employee getCurrentEmployee() {
		return UserChoiceController.getInstance().getEmployee();
	}
	
	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
}
