package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.view.content.NewClientPanel;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.Util;

public class NewClientController {
	
	private Client client;
	private NewClientPanel panel;
	
	private String personalIdHash;
	private String firstName;
	private String lastName;
	private String passwordHash;
	
	/**
	 * @param client
	 * @param panel
	 */
	public NewClientController(Client client, NewClientPanel panel) {
		this.client = client;
		this.panel = panel;
	}
	
	public void updateFieldsInputValidationView() {
		
		// Personal Id field.
		if (panel.getPersonalIdField().getText() == null || panel.getPersonalIdField().getText().isEmpty()) {
			panel.setEmptyPersonalIdInputView();
		}
		else if (!panel.getPersonalIdField().getText().matches("([0-9]){13,13}$")) {
			panel.setDigitsLimitPersonalIdInputView();
		}
		else if (MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(panel.getPersonalIdField().getText()))) {
			panel.setDuplicatePersonalIdInputView();
		}
		else if (panel.getPersonalIdField().getText().charAt(0) == '0') {
			panel.setWrongPersonalIdInputView();
		}
		else {
			panel.setValidPersonalIdInputView();
			personalIdHash = Util.getInstance().hashPersonalData(panel.getPersonalIdField().getText());
		}
		
		// First name field.
		if (panel.getFirstNameField().getText() == null || panel.getFirstNameField().getText().isEmpty()) {
			panel.setEmptyFirstNameInputView();
		}
		else if (!panel.getFirstNameField().getText().matches("[a-zA-Z ,.'-]+$")) {
			panel.setInvalidFirstNameInputView();
		} 
		else {
			panel.setValidFirstNameInput();
			firstName = panel.getFirstNameField().getText();
		}
		 
		// Last name field.
		if (panel.getLastNameField().getText() == null || panel.getLastNameField().getText().isEmpty()) {
			panel.setEmptyLastNameInputView();
		}
		else if (!panel.getFirstNameField().getText().matches("[a-zA-Z ,.'-]+$")) {
			panel.setInvalidLastNameInputView();
		}
		else {
			panel.setValidLastNameInput();
			lastName = panel.getLastNameField().getText();
		}
		
		// Password field.
		if (panel.getPasswordField().getPassword() == null || String.valueOf(panel.getPasswordField().getPassword()).isEmpty()) {
			panel.setEmptyPasswordInputView();
		}
		else if (!Util.getInstance().checkPasswordComplexity(String.valueOf(panel.getPasswordField().getPassword()))) {
			panel.setNotStrongEnoughtPasswordInputView();
		}
		else {
			panel.setValidPasswordInput();
		}
		
		// Confirm password field.
		if (panel.getConfirmPasswordField().getPassword() == null || String.valueOf(panel.getConfirmPasswordField().getPassword()).isEmpty()) {
			panel.setEmptyConfirmPasswordInputView();
		}
		else if (!String.valueOf(panel.getConfirmPasswordField().getPassword()).equals(
				  String.valueOf(panel.getPasswordField().getPassword()))) {
			panel.setDifferentPasswordsInputView();
		}
		else {
			panel.setValidConfirmPasswordView();
			passwordHash = Util.getInstance().hashPersonalData(String.valueOf(panel.getPasswordField().getPassword()));
		}
		
		// Valid input.
		if (personalIdHash != null && firstName != null && lastName != null && passwordHash != null) {
			boolean isClient;
			if (UserChoiceController.getInstance().getClient() != null) {
				isClient = true;
			}
			else {
				isClient = false;
			}
			panel.setValidClientDataView(isClient);
		}
	}
	
	public void saveClient() {
		if (personalIdHash != null && firstName != null && lastName != null && passwordHash != null) {
			client.setPersonalId(personalIdHash);
			client.setFirstName(firstName);
			client.setLastName(lastName);
			client.setPasswordHash(passwordHash);
			client.setExtractedFromIdAge(panel.getPersonalIdField().getText());
			client.setExtractedFromIdGender(panel.getPersonalIdField().getText());
			
			MedExpertDB.getInstance().saveClient(client);
		}
	}
}
