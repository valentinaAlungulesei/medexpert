package com.medExpert.mvc.controller;

import javax.swing.JOptionPane;

public class TestsDataReaderController {

	public TestsDataReaderController() {}
	
	public void updateInvalidFileStructure() {
		// Close the application.
		JOptionPane.showMessageDialog(HomePageController.getInstance().getHomePage(), "Due to security reasons you've been disconected!");
		HomePageController.getInstance().disposeHomePage();
	}
}
