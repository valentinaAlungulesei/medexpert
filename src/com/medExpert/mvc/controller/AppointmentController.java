package com.medExpert.mvc.controller;

import java.util.List;

import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.content.appointment.AppointmentPanel;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.TestsListStore;
import com.medExpert.util.Util;

public class AppointmentController {
	
	private Appointment appointment;
	private AppointmentPanel panel;
	private Client client;
	private String clientPersonalIdHash;
	
	/**
	 * @param appointment
	 * @param panel
	 */
	public AppointmentController(Appointment appointment, AppointmentPanel panel) {
		this.appointment = appointment;
		this.panel = panel;
	}
	
	public void updateLoggedInView() {
		if (getCurrentClient() != null && !getCurrentClient().isLoggedIn() ||
			getCurrentEmployee() != null && !getCurrentEmployee().isLoggedIn()) {
			panel.setLoginRequestView();
		}
	}

	public void updateAppointmentVlidationView() {
		
		// Client's personal id.
		if (panel.getClientPersonalIdField().getText().isEmpty()) {
			panel.setEmptyClientPersonalIdView();
		}
		else if (!panel.getClientPersonalIdField().getText().matches("([0-9]){13,13}$")) {
			panel.setDigitsLimitPersonalIdInputView();
		}
		else if (!MedExpertDB.getInstance().isIdHashInDB(Util.getInstance().hashPersonalData(
				  panel.getClientPersonalIdField().getText()))) {
			panel.setUnknownClientPersonalIdView();
		}
		else {
			panel.setValidPersonalIdView();
			clientPersonalIdHash = Util.getInstance().hashPersonalData(panel.getClientPersonalIdField().getText());
		}
		
		// Tests selection.
		if (panel.getSelectedTestsList().isEmpty()) {
			panel.setEmptyTestListSelectionView();
		} 
		else {
			panel.setValidTestsSelectionView();
		}

		// Date selection.
		if (panel.getCalendar().getSelectedDate() == 0) {
			panel.setEmptyDateSelectionView();
		} 
		else {
			panel.setValidDateSelectionView();
		}

		// Time selection.
		if (panel.getSelectedTime() == null || panel.getSelectedTime().isEmpty()) {
			panel.setEmptyHourSelectionView();
		} 
		else {
			panel.setValidHourSelectionView();
		}

		// Valid selection.
		if (!panel.getSelectedTestsList().isEmpty() &&
			 panel.getCalendar().getSelectedDate() != 0 && 
			!panel.getSelectedTime().isEmpty()) {
			panel.setValidAppointmentView();
		}
	}
	
	public void saveAppointment() {
		if (!panel.getSelectedTestsList().isEmpty() && 
			 panel.getCalendar().getSelectedDate() != 0 &&
			!panel.getSelectedTime().isEmpty()) {
			
			if (getCurrentClient() != null) {
				client = getCurrentClient();
			}
			else if (getCurrentEmployee() != null) {
				client = MedExpertDB.getInstance().getClientByIdHash(clientPersonalIdHash);
			}

			appointment.setClientId(client.getId());
			appointment.setTestsList(panel.getSelectedTestsList());
			appointment.setDate(panel.getCalendar().getSelectedDate());
			appointment.setMonth(panel.getCalendar().getSelectedMonth());
			appointment.setTime(panel.getSelectedTime());

			MedExpertDB.getInstance().saveAppointment(appointment);
			
			appointment.setId(MedExpertDB.getInstance().getAppointmentIdByClientId(client.getId()));
			
			// Save tests to DB.
			List<String> testNames = appointment.getTestsList();
			for(String name : testNames) {
				Test test = TestsListStore.getInstance().getTestByName(name);
				test.setAppointmentId(appointment.getId());
				MedExpertDB.getInstance().saveTest(test);
			}
		}
	}

	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
	
	private Employee getCurrentEmployee() {
		return UserChoiceController.getInstance().getEmployee();
	}
}
