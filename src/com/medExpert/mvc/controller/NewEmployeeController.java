package com.medExpert.mvc.controller;

import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.content.NewEmployeePanel;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.DESIGNATION;
import com.medExpert.util.SALARY_RANGE;
import com.medExpert.util.Util;

/**
 * This class is used to control the sign in of a new employee.
 * 
 * @author valentinaAlungulesei
 */
public class NewEmployeeController {
	
	private Employee employee;
	private NewEmployeePanel panel;
	private boolean isUsernameValid, isPasswordValid, arePasswordMatching, isFirstNameValid, 
					isLastNameValid, isDesignationValid, isSalaryValid = false;
	
	public NewEmployeeController(Employee employee, NewEmployeePanel panel) {
		this.employee = employee;
		this.panel = panel;
	}
	
	public void updateInputValidationView() {
		
		// Username field.
		if (panel.getUsernameField().getText() == null || panel.getUsernameField().getText().isEmpty()) {
			panel.setEmptyUsernameView();
		}
		else if (!panel.getUsernameField().getText().matches("^[a-z]+([-]?[a-z])*$")) {
			panel.setInvalidUsernameView();
		}
		else if (MedExpertDB.getInstance().isUsernameInDB(panel.getUsernameField().getText())) {
			panel.setTakenUsernameView();
		}
		else {
			panel.setValidUsernameView();
			isUsernameValid = true;
		}
		
		// Password field.
		if (String.valueOf(panel.getPasswordField().getPassword()).isEmpty()) {
			panel.setEmptyPasswordView();
		}
		else if (!Util.getInstance().checkPasswordComplexity(String.valueOf(panel.getPasswordField().getPassword()))) {
			panel.setInvalidPasswordView();
		}
		else {
			panel.setValidPasswordView();
			isPasswordValid = true;
		}
		
		// Confirm password field.
		if (String.valueOf(panel.getConfirmPasswordField().getPassword()).isEmpty()) {
			panel.setEmptyConfirmPasswordView();
		}
		else if (!String.valueOf(panel.getConfirmPasswordField().getPassword()).equals(
				  String.valueOf(panel.getPasswordField().getPassword()))) {
			panel.setDifferentConfirmPassword();
		}
		else {
			panel.setValidConfirmPasswordView();
			arePasswordMatching = true;
		}
		
		// First name field.
		if (panel.getFirstNameField().getText() == null || panel.getFirstNameField().getText().isEmpty()) {
			panel.setEmptyFirstNameView();
		}
		else if (!panel.getFirstNameField().getText().matches("[a-zA-Z ,.'-]+$")) {
			panel.setInvalidFisrtNameView();
		}
		else {
			panel.setValidFirstNameView();
			isFirstNameValid = true;
		}
		
		// Last name field.
		if (panel.getLastNameField().getText() == null || panel.getLastNameField().getText().isEmpty()) {
			panel.setEmptyLastNameView();
		}
		else if (!panel.getLastNameField().getText().matches("[a-zA-Z ,.'-]+$")) {
			panel.setInvalidLastNameView();
		}
		else {
			panel.setValidLastName();
			isLastNameValid = true;
		}
		
		// Designation combo box.
		if (panel.getDesignationComboBox().getSelectedItem().equals("")) {
			panel.setEmptyDesignationView();
		}
		else {
			isDesignationValid = true;
		}
		
		if (panel.getSalaryComboBox().getSelectedItem().equals("")) {
			panel.voidsetEmptySalaryView();
		}
		else {
			isSalaryValid = true;
		}
		
		// Valid input.
		if (isUsernameValid && isPasswordValid && arePasswordMatching && isFirstNameValid &&
			isLastNameValid && isDesignationValid && isSalaryValid ) {
			panel.setValidInputView();
		}
	}
	
	public void saveEmployee() {
		if (isUsernameValid && isPasswordValid && arePasswordMatching && isFirstNameValid &&
			isLastNameValid && isDesignationValid && isSalaryValid) {
			
			employee.setUsername(panel.getUsernameField().getText());
			employee.setPasswordHash(Util.getInstance().hashPersonalData(String.valueOf(panel.getPasswordField().getPassword())));
			employee.setFirstName(panel.getFirstNameField().getText());
			employee.setLastName(panel.getLastNameField().getText());
			employee.setDesignation(DESIGNATION.getByName(String.valueOf(panel.getDesignationComboBox().getSelectedItem())));
			employee.setSalaryRange(SALARY_RANGE.getBySalaryRange(String.valueOf(panel.getSalaryComboBox().getSelectedItem())));
			
			MedExpertDB.getInstance().saveEmployee(employee);
		}
	}
}
