package com.medExpert.mvc.model;

public class Test {
	
	private int id;
	private int appointmentId;
	private String name; 
	private double result;
	private String unit;
	private double minValue;
	private double maxValue;
	private double price;
	
	/**
	 * Called by test factory.
	 * 
	 * @param name
	 * @param price
	 * @param unit
	 * @param minValue
	 * @param maxValue
	 */
	public Test(String name, double price, String unit, double minValue, double maxValue) {
		this.name = name;
		this.price = price;
		this.unit = unit;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	/**
	 * Called by DB.
	 * 
	 * @param appointmentId
	 * @param name
	 * @param result
	 * @param minValue
	 * @param maxValue
	 */
	public Test(int appointmentId, String name, double result, String unit, double minValue, double maxValue) {
		this.appointmentId = appointmentId;	this.name = name;
		this.name = name;
		this.result = result;
		this.unit = unit;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public int getAppointmentId() {
		return appointmentId;
	}
	
	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setResult(double result) {
		this.result = result;
	}
	
	public double getResult() {
		return result;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}
	
	public double getMinValue() {
		return minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}
}
