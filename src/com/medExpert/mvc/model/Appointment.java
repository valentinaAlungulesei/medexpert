package com.medExpert.mvc.model;

import java.util.List;

public class Appointment {
	
	private int id;
	private int clientId;
	private List<String> testsList;
	private byte date;
	private String month;
	private String time;
	
	public Appointment() {}
	
	/**
	 * @param id
	 * @param clientId
	 * @param testsList
	 * @param date
	 * @param month
	 * @param time
	 */
	public Appointment(int id, int clientId, List<String> testsList, byte date, String month, String time) {
		this.id = id;
		this.clientId = clientId;
		this.testsList = testsList;
		this.date = date;
		this.month = month;
		this.time = time;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getClientId() {
		return clientId;
	}
	
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	
	public List<String> getTestsList() {
		return testsList;
	}
	
	public void setTestsList(List<String> testsList) {
		this.testsList = testsList;
	}
	
	public byte getDate() {
		return date;
	}
	
	public void setDate(byte date) {
		this.date = date;
	}
	
	public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) {
		this.month = month;
	}

	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
}
