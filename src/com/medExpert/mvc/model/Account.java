package com.medExpert.mvc.model;

public abstract class Account {
	
	private int id;
	private String firstName;
	private String lastName;
	private String hashedPassword;
	private boolean isLoggedIn;
	
	public Account() {}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	public void setPasswordHash(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
	
	public String getPasswordHash() {
		return hashedPassword;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
}
