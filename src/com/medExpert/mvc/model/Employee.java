package com.medExpert.mvc.model;

import com.medExpert.util.DESIGNATION;
import com.medExpert.util.SALARY_RANGE;

public class Employee extends Account {

	private String username;
	private DESIGNATION designation;
	private SALARY_RANGE salaryRange;
	
	public Employee() {}
	
	/**
	 * @param firstName
	 * @param lastName
	 * @param designation
	 * @param salaryRange
	 */
	public Employee(String firstName, String lastName, DESIGNATION designation, SALARY_RANGE salaryRange) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.designation = designation;
		this.salaryRange = salaryRange;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public DESIGNATION getDesignation() {
		return designation;
	}

	public void setDesignation(DESIGNATION designation) {
		this.designation = designation;
	}

	public SALARY_RANGE getSalaryRange() {
		return salaryRange;
	}
	
	public void setSalaryRange(SALARY_RANGE salary_range) {
		this.salaryRange = salary_range;
	}
}
