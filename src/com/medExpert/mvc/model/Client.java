package com.medExpert.mvc.model;

import java.time.LocalDate;

public class Client extends Account {
	
	private String personalIdHash;
	private byte age;
	private char gender;
	
	public Client() {}
	
	/**
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param age
	 * @param gender
	 */
	public Client(int id, String firstName, String lastName, byte age, char gender) {
		this.setId(id);
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.age = age;
		this.gender = gender;
	}

	public String getPersonalIdHash() {
		return personalIdHash;
	}
	
	public void setPersonalId(String personalIdHash) {
		this.personalIdHash = personalIdHash;
	}

	public byte getAge() {
		return age;
	}

	public void setAge(byte age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}
	
	/**
	 * If the id starts with '1' or '2' followed by '9' means that the person with this id is born in 1900
	 * and the age is calculated by subtracting 1900 from the current year.
	 * If the id starts with '3' or '4' followed by '8', the person with this id is born in 1800
	 * and the age is calculated by subtracting 1800 from the current year.
	 * If the id starts with '5' or '6' followed by '0', the person with this id is born in 2000
	 * and the age is calculated by subtracting 2000 from the current year.
	 * 
	 * @param plainID
	 * @return the age extracted from the given id
	 */
	public void setExtractedFromIdAge(String plainID) {
		
		char firstChar = plainID.charAt(0);
		char secondChar = plainID.charAt(1);
		String ageString = "";
		
		if ((firstChar == '1' || firstChar == '2') && secondChar == '9' || secondChar == '8') {
			ageString = "19" + plainID.substring(1, 3);
		}	
		else if ((firstChar == '3' || firstChar == '4') && secondChar == '8') {
			ageString = "18" + plainID.substring(1, 3);
		}
		else if ((firstChar == '5' || firstChar == '6') && secondChar == '0') {
			ageString = "20" + plainID.substring(1, 3);
		}
		
		age = (byte) (LocalDate.now().getYear() - Integer.parseInt(ageString));
	}

	/**
	 * Sets the gender to 'M' (male) if the given id starts with '1', '3', '5', '7' or
	 * sets it to 'F' (female) if the given id starts with '2', '4'. '6', '8' or 
	 * sets it to 'U' (unknown) if the given id starts with '9' which means that the person with 
	 * this id is foreign.
	 * 
	 * @param id
	 */
	public void setExtractedFromIdGender(String id) {
		
		char firstChar = id.charAt(0);
		
		if (firstChar == '2' || firstChar == '4' || firstChar == '6' || firstChar == '8') {
			gender = 'F'; 	// Female
		} else if (firstChar == '1' || firstChar == '3' || firstChar == '5' || firstChar == '7') {
			gender = 'M';	// Male
		} else if (firstChar == '9') {
			gender = 'U';	// Unknown
		}
	}

	@Override
	public String toString() {
		return "Client [id=" + getId() 
			+ ", firstName=" + getFirstName()
			+ ", lastName=" + getLastName()
			+ ", age=" + age 
			+ ", gender=" + gender + "]";
	}
}
