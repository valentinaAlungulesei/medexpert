package com.medExpert.mvc.view;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.medExpert.util.APath;

public class BackgroundLabel extends JLabel {
	
	private int width;
	private int height;

	public BackgroundLabel(int width, int height) {
		this.width = width;
		this.height = height;
		
		build();
	}
	
	private void build() {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource(APath.BACKGROUND_JPG));
		Image image = imageIcon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(image);

		setIcon(imageIcon);
	}
	

	public int getBackgroundImageWigth() {
		return (int) ADimension.SCREEN_WIDTH;
	}
	
	public int getBackgroundImageHeight() {
		return (int) ADimension.SCREEN_HEIGHT;
	}
}
