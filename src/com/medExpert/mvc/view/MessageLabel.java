package com.medExpert.mvc.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MessageLabel extends JLabel {
	
	private String message;
	
	public MessageLabel(String message) {
		this.message = message;
		configure();
	}

	private void configure() {
		setText(message);
		setHorizontalAlignment(SwingConstants.CENTER);
		setFont(AFont.bold15());
		setForeground(Color.white);
	}
}
