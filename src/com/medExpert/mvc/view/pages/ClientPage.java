package com.medExpert.mvc.view.pages;

import java.awt.BorderLayout;

import javax.swing.JLayeredPane;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.content.CurrentClientPanel;
import com.medExpert.mvc.view.menu.ClientMenuPanel;

public class ClientPage extends APage {

	private ClientMenuPanel clientMenuPanel;
	private CurrentClientPanel currentClientPanel;
	
	public ClientPage() {
		super(PAGE_NAME.CLIENT);
	}

	@Override
	public void buildContent() {
		setLayout(new BorderLayout());
		
		add(getClientMenuPanel(), BorderLayout.WEST);
		add(getContentLayeredPane(), BorderLayout.CENTER);
	}
	
	public ClientMenuPanel getClientMenuPanel() {
		if (clientMenuPanel == null) {
			clientMenuPanel = new ClientMenuPanel();
		}
		return clientMenuPanel;
	}
	
	public void addCurrentClientPanel() {
		getContentLayeredPane().add((getCurrentClientPanel()), JLayeredPane.DRAG_LAYER);
		repaint();
	}
	
	public void removeCurrentClientPanel() {
		getContentLayeredPane().remove(getCurrentClientPanel());
		repaint();
	}
	
	private CurrentClientPanel getCurrentClientPanel() {
		if (currentClientPanel == null) {
			currentClientPanel = new CurrentClientPanel();
			currentClientPanel.setBounds(ADimension.CURRENT_ACCOUNT_X,
					 					 0,
					 					 ADimension.CURRENT_ACCOUNT_WIDTH,
					 					 ADimension.SCREEN_HEIGHT);
		}
		return currentClientPanel;
	}
}
