package com.medExpert.mvc.view.pages;

public interface IStaffPassCodeListener {
	
	void staffButtonWasClicked();

	void enterButtonWasClicked();
	
	void enterKeyWasPressed();
}
