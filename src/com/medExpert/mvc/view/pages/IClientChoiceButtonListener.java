package com.medExpert.mvc.view.pages;

public interface IClientChoiceButtonListener {

	void clientButtonWasClicked();
}
