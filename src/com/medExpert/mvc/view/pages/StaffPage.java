package com.medExpert.mvc.view.pages;

import java.awt.BorderLayout;

import com.medExpert.mvc.view.menu.StaffMenuPanel;

public class StaffPage extends APage {
	
	private StaffMenuPanel staffMenuPanel;

	public StaffPage() {
		super(PAGE_NAME.STAFF);
	}

	@Override
	public void buildContent() {
		this.setLayout(new BorderLayout());
		
		this.add(getStaffMenuPanel(),BorderLayout.WEST);
		this.add(getContentLayeredPane(), BorderLayout.CENTER);
	}
	
	public StaffMenuPanel getStaffMenuPanel() {
		if (staffMenuPanel == null) {
			staffMenuPanel = new StaffMenuPanel();
		}
		return staffMenuPanel;
	}
}
