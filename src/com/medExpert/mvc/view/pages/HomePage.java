package com.medExpert.mvc.view.pages;

import javax.swing.JLayeredPane;

import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.content.UserChoicePanel;

public class HomePage extends APage {
	
	private UserChoicePanel userChoicePanel;
	
	public HomePage() {
		super(PAGE_NAME.HOME);
	}

	@Override
	public void buildContent() {
		getContentLayeredPane().setBounds(ADimension.SCREEN_X, ADimension.SCREEN_Y,
										  ADimension.SCREEN_WIDTH, ADimension.SCREEN_HEIGHT);
		getContentLayeredPane().add(getUserChoicePanel(), JLayeredPane.DRAG_LAYER);
		
		this.add(getContentLayeredPane());
	}
	
	public UserChoicePanel getUserChoicePanel() {
		if (userChoicePanel == null) {
			userChoicePanel = new UserChoicePanel();
			userChoicePanel.addClientButtonListener(new IClientChoiceButtonListener() {
				
				@Override
				public void clientButtonWasClicked() {
					UserChoiceController.getInstance().updateClientChoiceView();
				}
			});
			userChoicePanel.addStaffButtonListener(new IStaffPassCodeListener() {
				
				@Override
				public void staffButtonWasClicked() {
					UserChoiceController.getInstance().updatePassCodeAreaView();	
				}
				
				@Override
				public void enterButtonWasClicked() {
					UserChoiceController.getInstance().updateStaffPageView();
				}

				@Override
				public void enterKeyWasPressed() {
					UserChoiceController.getInstance().updateStaffPageView();
				}
			});
		}
		return userChoicePanel;
	}
}
