package com.medExpert.mvc.view.pages;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.BackgroundLabel;

public abstract class APage extends JFrame {
	
	private PAGE_NAME page;
	private BackgroundLabel homePageBackgroundLabel;
	private BackgroundLabel userPageBackgroundLabel;
	private JLayeredPane contentLayeredPane;

	public APage(PAGE_NAME page) {
		this.page = page;
	
		configure();
		buildContent();
	}
	
	private void configure() {
		setTitle(page.getName());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBackground(Color.cyan);
		setExtendedState(MAXIMIZED_BOTH);
		setLayout(null);
	}
	
	public abstract void buildContent();
	
	public void reset() {
		getContentLayeredPane().removeAll();
		getContentLayeredPane().add(getUserPageBackgroundLabel());
	}
	
	public void addContent(JPanel panel) {
		getContentLayeredPane().add(panel, JLayeredPane.DRAG_LAYER);
	}
	
	public JLayeredPane getContentLayeredPane() {
		if (contentLayeredPane == null) {
			contentLayeredPane = new JLayeredPane();
			if (this instanceof ClientPage || this instanceof StaffPage) {
				contentLayeredPane.add(getUserPageBackgroundLabel(), JLayeredPane.DEFAULT_LAYER);
			}
			else {
				contentLayeredPane.add(getHomePageBackgroundLabel(), JLayeredPane.DEFAULT_LAYER);
			}
		}
		return contentLayeredPane;
	}
	
	public BackgroundLabel getHomePageBackgroundLabel() {
		if (homePageBackgroundLabel == null) {
			homePageBackgroundLabel = new BackgroundLabel(ADimension.SCREEN_WIDTH, ADimension.SCREEN_HEIGHT);
			homePageBackgroundLabel.setBounds(ADimension.SCREEN_X, ADimension.SCREEN_Y,
					  				  		  ADimension.SCREEN_WIDTH, ADimension.SCREEN_HEIGHT);
		}
		return homePageBackgroundLabel;
	}
	
	public BackgroundLabel getUserPageBackgroundLabel() {
		if (userPageBackgroundLabel == null) {
			userPageBackgroundLabel = new BackgroundLabel(ADimension.SCREEN_WIDTH - ADimension.MENU_WIDTH, ADimension.SCREEN_HEIGHT);
			userPageBackgroundLabel.setBounds(0,
									  ADimension.SCREEN_Y,
					  				  ADimension.SCREEN_WIDTH - ADimension.MENU_WIDTH,
					  				  ADimension.SCREEN_HEIGHT);
		}
		return userPageBackgroundLabel;
	}
}
