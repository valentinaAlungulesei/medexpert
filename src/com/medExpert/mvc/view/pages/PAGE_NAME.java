package com.medExpert.mvc.view.pages;

public enum PAGE_NAME {
	
	HOME("MedExpert"),
	CLIENT("Client"),
	STAFF("Staff");
	
	private String name;
	
	PAGE_NAME(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
