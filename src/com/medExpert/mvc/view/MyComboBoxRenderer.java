package com.medExpert.mvc.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * Customer render for JComboBox.
 * @author valentinaAlungulesei
 */

public class MyComboBoxRenderer extends JPanel implements ListCellRenderer<String> {
	
	private JLabel labelItem = new JLabel();
	
	public MyComboBoxRenderer() {
		customise();
	}
	
	private void customise() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;

		labelItem.setOpaque(true);
		labelItem.setHorizontalAlignment(JLabel.CENTER);
		labelItem.setPreferredSize(new Dimension(ADimension.COMBO_BOX_WIDTH, ADimension.ROW_HEIGHT));
		
		add(labelItem, constraints);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends String> list,
												  String value,
												  int index,
												  boolean isSelected,
												  boolean cellHasFocus) {
		// Set hourItem text.
		labelItem.setText(value.toString());
		labelItem.setFont(AFont.bold12());
		
		if (isSelected) {
			labelItem.setBackground(AColor.darck());
			labelItem.setForeground(Color.white);
			labelItem.setText(value.toString());
		}	
		else {
			labelItem.setBackground(Color.white);
			labelItem.setForeground(Color.black);
		}
		return this;
	}
}
