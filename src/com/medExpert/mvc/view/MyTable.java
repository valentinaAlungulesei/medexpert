package com.medExpert.mvc.view;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class MyTable extends JTable {
	
	private DefaultTableCellRenderer cellRenderer;
	private DefaultTableCellRenderer testNameColHeaderRenderer;
	private DefaultTableCellRenderer defaultHeaderRenderer;
	
	/**
	 * @param data
	 * @param columnNames
	 */
	public MyTable(String[][] data, String[] columnNames) {
		super(data, columnNames);
		
		setFillsViewportHeight(true);
		setRowHeight(ADimension.ROW_HEIGHT + 10);
		setSelectionBackground(AColor.darck());
		setFont(AFont.plain15());
		setRowSelectionAllowed(true);
		getColumnModel().getColumn(0).setHeaderRenderer(getTestNameColHeaderRenderer());
		getColumnModel().getColumn(1).setCellRenderer(getMyCellRenderer());
		getTableHeader().setDefaultRenderer(getDeafultHeaderRenderer());
		getTableHeader().setPreferredSize(new Dimension(ADimension.CONTENT_X, ADimension.ROW_HEIGHT));
		getTableHeader().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, AColor.darck()));
	}
	
	public DefaultTableCellRenderer getDeafultHeaderRenderer() {
		if (defaultHeaderRenderer == null) {
			defaultHeaderRenderer = new DefaultTableCellRenderer();
			defaultHeaderRenderer.setBackground(AColor.light());
			defaultHeaderRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return defaultHeaderRenderer;
	}
	
	public DefaultTableCellRenderer getTestNameColHeaderRenderer() {
		if (testNameColHeaderRenderer == null) {
			testNameColHeaderRenderer = new DefaultTableCellRenderer();
			testNameColHeaderRenderer.setBackground(AColor.light());
			testNameColHeaderRenderer.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return testNameColHeaderRenderer;
	}
	
	public DefaultTableCellRenderer getMyCellRenderer() {
		if (cellRenderer == null) {
			cellRenderer = new DefaultTableCellRenderer();
			cellRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return cellRenderer;
	}
}
