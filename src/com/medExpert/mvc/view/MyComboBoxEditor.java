package com.medExpert.mvc.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxEditor;

/**
 * Editor for JComboBox
 * @author valentinaAlungulesei
 *
 */
public class MyComboBoxEditor extends BasicComboBoxEditor {
	
	private JPanel panel;
	private JLabel labelItem;
	private Object selectedValue;
	
	public MyComboBoxEditor() {
		customize();
	}

	private void customize() {
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		
		labelItem = new JLabel();
		labelItem.setOpaque(false);
		labelItem.setFont(AFont.bold12());
		labelItem.setForeground(Color.black);
		
		panel.setBackground(Color.white);
		panel.add(labelItem, constraints);
	}

	public Component getEditorComponent() {
		return this.panel;
	}
	
	public Object getItem() {
		return this.selectedValue;
	}

	public void setItem(Object item) {
		
		if (item == null) {
			return;
		}

		this.selectedValue = item;
		labelItem.setText(item.toString());
	}
}
