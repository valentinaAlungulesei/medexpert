package com.medExpert.mvc.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

/**
 * A custom comboBox with it's own renderer and editor.
 * 
 * @author valentinaAlungulesei
 */
public class MyComboBox extends JComboBox<String> {
	
	private String[] items;
	private DefaultComboBoxModel<String> model;
	private JLabel label;

	/**
	 * @param items
	 */
	public MyComboBox(String[] items) {
		this.items = items;
		model = new DefaultComboBoxModel<String>();
		configure();
	}
	
	public void setWarning(String message, JLabel label) {
		this.label = label;
		
		label.setText(message);
		label.setFont(AFont.bold13());
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.red);
		
		this.setBorder(new LineBorder(Color.red, 1));
		
	}

	private void configure() {
		
		for (String item : items) {
			model.addElement(item);
		}
		
		setModel(model);
		setRenderer(new MyComboBoxRenderer());
		setEditable(true);
		setEditor(new MyComboBoxEditor());
		setBorder(new LineBorder(Color.white, 1));
		setUI(new BasicComboBoxUI() {
			protected JButton createArrowButton() {
				BasicArrowButton arrowButton = new BasicArrowButton(BasicArrowButton.SOUTH, null,
																	null, AColor.darck(), null); 
				arrowButton.setBorderPainted(true);
				arrowButton.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
				return arrowButton;
			}
		});
		
		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setBorder(new LineBorder(Color.white, 1));
				if (label != null) {
					label.setText("");
				}
			}
		});
	}
}
