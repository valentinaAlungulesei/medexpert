package com.medExpert.mvc.view.content;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.Color;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.APath;

public class GoodToKnowPanel extends AContentPanel {

	private JInternalFrame internalFrame;
	private JTabbedPane tabbedPane;
	private JPanel bloodTestingTab;
	private JPanel urineTestingTab;
	private JPanel childrenTestingTab;

	public GoodToKnowPanel() {
		super(CONTENT_ID.INFO, APath.INFO_PNG);
	}

	@Override
	public void buildContent() {
		add(getInternalFrame());
	}
	
	private JInternalFrame getInternalFrame() {
		if (internalFrame == null) {
			internalFrame = new JInternalFrame("", false, false);
			internalFrame.setBounds(0, ADimension.FIELD_Y,
					ADimension.CONTENT_WIDTH, ADimension.SCREEN_HEIGHT - ADimension.FIELD_Y - ADimension.BOTTOM_INSET);
			internalFrame.add(getTabbedPane());
			internalFrame.setBorder(null);
			internalFrame.setVisible(true);
		}
		return internalFrame;
	}
	
	private JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
			tabbedPane.setBorder(new LineBorder(AColor.darck(), 1));
			tabbedPane.setFont(AFont.bold13());
			tabbedPane.setForeground(Color.black);
			tabbedPane.setOpaque(true);
			tabbedPane.setBackground(AColor.light());
			tabbedPane.add("Blood testing", getBloodTestingTab());
			tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
			tabbedPane.add("Urine testing", getUrineTestingTab());
			tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
			tabbedPane.add("Children testing", getChildrenTestingTab());
			tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		}
		return tabbedPane;
	}
	
	private JPanel getBloodTestingTab() {
		if (bloodTestingTab == null) {
			
			JTextArea textArea = new JTextArea();
			textArea.setEditable(false);
			textArea.setHighlighter(null);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setFont(AFont.plain15());
			textArea.setText("\nMedical professionals order blood tests for a variety of reasons, from monitoring medication levels to evaluating your results in the course of diagnosing a medical condition. There are multiple things you can do to prepare yourself for a blood test, both mentally and physically. Some of them are:\n"
					+ "\n1. Talk with your doctor."
					+ "\nLet your doctor know about any symptoms you've been experiencing, and ask if there are any specific blood tests that could help with investigating the cause. You need to know about the specific blood tests your doctor is ordering. Some blood tests will require special preparation to get accurate results.\n"
					+ "\n2. Discuss your medications.\n"
					+ "There are certain substances that can alter blood tests, which you may need to stop before your blood test. Prescription medications, recreational drugs, alcohol intake, vitamins, blood thinners, or over-the-counter medications can often change the results of a blood test, depending on what the blood test is for.\n"
					+ "\n3. Refrain from certain activities.\n"
					+ "There are some blood tests that can be compromised based on your activities. These tests can be altered by recent physical activity or heavy exercise, undergoing dehydration, smoking, drinking herbal teas, or sexual activity.\n"
					+ "\n4. Ask your doctor for instructions.\n"
					+ "Many routine tests do not require special preparation prior to getting your blood drawn. However, when in doubt, ask. If your physician does not give you any special instructions, it is important that you ask in order to reduce the potential that you arrive for the test without preparing sufficiently.\n");
			
			bloodTestingTab = new JPanel(new GridLayout(1, 1));
			bloodTestingTab.setBackground(Color.white);
			bloodTestingTab.add(textArea);
		}
		return bloodTestingTab;
	}
	
	private JPanel getUrineTestingTab() {
		if (urineTestingTab == null) {
			
			JTextArea textArea = new JTextArea();
			textArea.setEditable(false);
			textArea.setHighlighter(null);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setFont(AFont.plain15());
			textArea.setText("\nWhy it's done?\n\n"
					+ "A urinalysis is a common test that's done for several reasons:\n"
					+ "\n1. To check your overall health.\n"
					+ "Your doctor may recommend a urinalysis as part of a routine medical exam, pregnancy checkup, pre-surgery preparation, or on hospital admission to screen for a variety of disorders, such as diabetes, kidney disease and liver disease.\n"
					+ "\n2. To diagnose a medical condition.\n"
					+ "Your doctor may suggest a urinalysis if you're experiencing abdominal pain, back pain, frequent or painful urination, blood in your urine, or other urinary problems. A urinalysis may help diagnose the cause of these symptoms.\n"
					+ "\n3. To monitor a medical condition.\n"
					+ "If you've been diagnosed with a medical condition, such as kidney disease or a urinary tract disease, your doctor may recommend a urinalysis on a regular basis to monitor your condition and treatment.");
			
			urineTestingTab = new JPanel(new GridLayout(1, 1));
			urineTestingTab.setBackground(Color.white);
			urineTestingTab.add(textArea);
		}
		return urineTestingTab;
	}

	public JPanel getChildrenTestingTab() {
		if (childrenTestingTab == null) {
			
			JTextArea textArea = new JTextArea();
			textArea.setEditable(false);
			textArea.setHighlighter(null);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setFont(AFont.plain15());
			textArea.setText("\nChildren typically require fewer tests than adults, but there are times when children need lab tests and a helping hand to get them through the procedure. A caring grownup can help the child cope with any physical pain or discomfort as well as any fear, anxiety, or emotional reactions that may occur as the sample is collected. Below are some general recommendations on helping children through these medical procedures as well as some specific tips on blood, urine and stool specimens, and throat culture sample collections.\n"
			+ "\n1. Prepare the Child.\n"
			+ "Calmly explain how the sample will be collected and why, giving the child time to adjust to the idea before anyone touches his or her body.\n"
			+ "\n2. Encourage Rehearsing.\n"
			+ "At home or in a setting comfortable for the child, suggest ways the child can rehearse. The child can practice some techniques at home or pretend with a doll or stuffed toy as the patient.\n"
			+ "\n2. Help the Child Put it in Perspective.\n"
			+ "Relate the part of the procedure the child may find overwhelming to something the child has mastered or is familiar with; for example, explain that this will be over as fast as it takes for you to climb stairs at home or in less time than a certain song can be sung.\n"
			+ "\n4. Plan a Reward.\n"
			+ "Telling the child about something fun you plan to do together after the lab test can be helpful as encouragement and distraction.\n"
			+ "\n");
			
			childrenTestingTab = new JPanel(new GridLayout(1, 1));
			childrenTestingTab.setBackground(Color.white);
			childrenTestingTab.add(textArea);
		}
		return childrenTestingTab;
	}
}
