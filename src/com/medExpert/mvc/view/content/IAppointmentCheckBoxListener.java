package com.medExpert.mvc.view.content;

public interface IAppointmentCheckBoxListener {
	
	public void checkBoxIsSelected();
	
	public void checkBoxIsDeselected();
}
