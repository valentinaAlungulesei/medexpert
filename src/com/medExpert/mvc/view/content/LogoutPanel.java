package com.medExpert.mvc.view.content;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.mvc.view.progressBar.MyProgressBar;
import com.medExpert.mvc.view.progressBar.UpdateProgressBarTask;
import com.medExpert.util.APath;

public class LogoutPanel extends AContentPanel {
	
	private MyProgressBar progressBar;
	private MessageLabel messageLabel;

	public LogoutPanel() {
		super(CONTENT_ID.LOG_OUT, APath.ACCOUNT_PNG);
	}

	@Override
	public void buildContent() {}
	
	public void setLogoutView() {
		UpdateProgressBarTask task = new UpdateProgressBarTask(getProgressBar());
		this.add(getProgressBar());
		task.start();
	}
	
	public void setLoggedInView() {
		this.add(getMessageLabel());
		this.repaint();
	}
	
	public MyProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new MyProgressBar(0, 100);
			progressBar.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y,
						          ADimension.FIELD_WIDTH, ADimension.ROW_HEIGHT);
		}
		return progressBar;
	}
	
	private MessageLabel getMessageLabel() {
		if (messageLabel == null) {
			messageLabel = new MessageLabel("You are not currently logged in.");
			messageLabel.setBounds(0, ADimension.FIELD_Y, ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		}
		return messageLabel;
	}
}
