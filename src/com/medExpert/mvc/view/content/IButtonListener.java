package com.medExpert.mvc.view.content;

public interface IButtonListener {

	public void buttonWasClicked();
}
