package com.medExpert.mvc.view.content;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.menu.CONTENT_ID;

public abstract class AContentPanel extends JPanel {
	
	private CONTENT_ID content_ID;
	private String iconPath;
	private JLabel titleLabel;
	
	public AContentPanel(CONTENT_ID content_ID, String iconPath) {
		this.content_ID = content_ID;
		this.iconPath = iconPath;
		configure();
		buildContent();
	}

	public abstract void buildContent();
	
	public void setLoginRequestView() {
		MessageLabel messageLabel = new MessageLabel("This action requires you to log in.");
		messageLabel.setBounds(0, ADimension.FIELD_Y, ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		
		MessageLabel loginRequestLabel = new MessageLabel("Please, go to log in screen.");
		loginRequestLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT, ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		
		this.add(messageLabel);
		this.add(loginRequestLabel);
		this.repaint();
	}
	
	private void configure() {
		this.setBounds(ADimension.CONTENT_X, 0, ADimension.CONTENT_WIDTH, ADimension.SCREEN_HEIGHT);
		this.setOpaque(false);
		this.setLayout(null);
		
		// Set the title.
		this.add(getTitleLabel());
	}
	
	private JLabel getTitleLabel() {
		if (titleLabel == null) {
			titleLabel = new JLabel();
			
			titleLabel.setBounds(0, 0, ADimension.CONTENT_WIDTH, ADimension.TITLE_HEIGHT);
			titleLabel.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, AColor.darck()));
			titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
			titleLabel.setIcon(new ImageIcon(getClass().getResource(this.iconPath)));
			titleLabel.setIconTextGap(30);
			titleLabel.setText(this.content_ID.getName());
			titleLabel.setFont(AFont.bold15());
		}
		return titleLabel;
	}
}
