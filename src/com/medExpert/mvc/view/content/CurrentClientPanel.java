package com.medExpert.mvc.view.content;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.repository.MedExpertDB;

public class CurrentClientPanel extends JPanel {
	
	private JLabel accountFullNameLabel;
	private JPanel accountInfoPanel;
	
	public CurrentClientPanel() {
		setOpaque(false);
		setLayout(null);
		
		add(getAccountFullNameLabel());
		add(getAccountInfoPanel());
	}

	private JLabel getAccountFullNameLabel() {
		if (accountFullNameLabel == null) { 
			accountFullNameLabel = new JLabel("About " + getCurrentClient().getFullName());
			accountFullNameLabel.setOpaque(false);
			accountFullNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
			accountFullNameLabel.setFont(AFont.bold15());
			accountFullNameLabel.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, AColor.darck()));
			accountFullNameLabel.setBounds(20, 0, ADimension.CURRENT_ACCOUNT_WIDTH, ADimension.TITLE_HEIGHT);
		}
		return accountFullNameLabel;
	}
	
	private JPanel getAccountInfoPanel() {
		if (accountInfoPanel == null) {
			
			accountInfoPanel = new JPanel();
			accountInfoPanel = new JPanel(new GridLayout(3, 1, 0, 25));
			accountInfoPanel.setOpaque(false);
			accountInfoPanel.setBounds(ADimension.CURRENT_ACCOUNT_WIDTH / 6,
					   				   ADimension.FIELD_Y,
					   				   ADimension.CURRENT_ACCOUNT_WIDTH - (ADimension.CURRENT_ACCOUNT_WIDTH / 3),
					   				   4 * ADimension.FIELD_HEIGHT);
			accountInfoPanel.add(buildTextField("Gender", String.valueOf(getCurrentClient().getGender())));
			accountInfoPanel.add(buildTextField("Age", String.valueOf(getCurrentClient().getAge())));
			accountInfoPanel.add(buildTextField("Scheduled appointment", getAppointmentText()));
		}
		return accountInfoPanel;
	}
	
	private JTextField buildTextField(String title, String value) {
		
		JTextField field = new JTextField();
		field.setOpaque(false);
		field.setEditable(false);
		field.setCaretColor(Color.white);
		field.setForeground(Color.black);
		field.setSelectionColor(Color.white);
		field.setFont(AFont.plain15());
		field.setHorizontalAlignment(SwingConstants.CENTER);
		field.setText(value);
		field.setBorder(BorderFactory.createTitledBorder(
				    	new LineBorder(Color.black, 1), 
				    	title, 
				    	TitledBorder.LEFT,
				    	TitledBorder.DEFAULT_POSITION, 
				    	AFont.italic12(), 
				    	Color.black));	
		
		return field;
	}
	
	private String getAppointmentText() {
		String text = "";
		if (MedExpertDB.getInstance().getAppointmentsByClientId(getCurrentClient().getId()).size() == 0) {
			return new String(" - ");
		} else {
			List<Appointment> appointments = MedExpertDB.getInstance().getAppointmentsByClientId(getCurrentClient().getId());
			StringBuilder textBuilder = new StringBuilder();
			
			for (Appointment a : appointments) {
				textBuilder.append(a.getDate() + " " + a.getMonth() + " at " + a.getTime());
				textBuilder.append(System.lineSeparator());
			}
			text = textBuilder.toString();
		}
		return text;
	}
	
	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
}
