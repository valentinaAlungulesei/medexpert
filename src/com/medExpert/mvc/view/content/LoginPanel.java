package com.medExpert.mvc.view.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.MyPasswordField;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;
import com.medExpert.util.Util;

public class LoginPanel extends AContentPanel {
	
	private MyTextField personalIdField;
	private MyTextField usernameField;
	private JLabel personalIDWarningLabel;
	private MyPasswordField passwordField;
	private JLabel passwordWarningLabel;
	private MyButton loginButton;

	private List<IButtonListener> loginButtonListeners;
	
	public LoginPanel() {
		super(CONTENT_ID.CLIENT_LOG_IN, APath.ACCOUNT_PNG);
		loginButtonListeners = new ArrayList<IButtonListener>();
	}
	
	public void addLoginButtonListener(IButtonListener listener) {
		this.loginButtonListeners.add(listener);
	}

	@Override
	public void buildContent() {
		if (UserChoiceController.getInstance().getClient() != null) {
			this.add(getPersonalIDField());
		}
		else if (UserChoiceController.getInstance().getEmployee() != null) {
			this.add(getUsernameField());
		}
		
		this.add(getPasswordField());
		this.add(getLoginButton());
	}
	
	public MyTextField getPersonalIDField() {
		if (personalIdField == null) {
			personalIdField = new MyTextField("Personal ID");
			// Limit the number of input characters to 13 (no of digits within a romanian ID).
			personalIdField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (personalIdField.getText().length() >= 13) {
						personalIdField.setToolTipText(AMessages.LIMIT_13_TOOLTIP);
						e.consume();
					}
				}
			});
			personalIdField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y, 
									  ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return personalIdField;
	}
	
	public MyTextField getUsernameField() {
		if (usernameField == null) {
			usernameField = new MyTextField("Username");
			usernameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y, 
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return usernameField;
	}
	
	private MyPasswordField getPasswordField() {
		if (passwordField == null) {
			passwordField = new MyPasswordField("Password");
			passwordField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return passwordField;
	}
	
	private MyButton getLoginButton() {
		if (loginButton == null) {
			loginButton = new MyButton("Log in");
			loginButton.setBounds((ADimension.CONTENT_WIDTH / 2) - (ADimension.BUTTON_WIDTH / 2), 
								   ADimension.FIELD_Y + (2 * ADimension.FIELD_Y_SPACING),
								   ADimension.BUTTON_WIDTH,
								   ADimension.BUTTON_HEIGHT);
			loginButton.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						for (IButtonListener listener : loginButtonListeners) {
							listener.buttonWasClicked();
						}
					}
				}
			});
			loginButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					for (IButtonListener listener : loginButtonListeners) {
						listener.buttonWasClicked();
					}
				}
			});
		}
		return loginButton;
	}

	public String getPersonalIdHash() {
		if (getPersonalIDField().getText() == null || getPersonalIDField().getText().isEmpty()) {
			return null;
		}
		return Util.getInstance().hashPersonalData(getPersonalIDField().getText());
	}
	
	public String getPasswordHash() {
		if (String.valueOf(getPasswordField().getPassword()) == null || String.valueOf(getPasswordField().getPassword()).isEmpty()) {
			return null;
		}
		return Util.getInstance().hashPersonalData(String.valueOf(getPasswordField().getPassword()));
	}
	
	public void setEmptyPersonalIdView() {
		this.add(getPersonalIDWarningLabel());
		getPersonalIDField().setWarning(AMessages.EMPTY_FIELD, getPersonalIDWarningLabel());
	}
	
	public void setPersonalIdDigitLimit() {
		this.add(getPersonalIDWarningLabel());
		getPersonalIDField().setWarning("The ID must contain 13 digits.", getPersonalIDWarningLabel());
	}
	
	public void setUnknownPersonalIdView() {
		this.add(getPersonalIDWarningLabel());
		getPersonalIDField().setWarning("A client whit this personal ID is not registred.", getPersonalIDWarningLabel());
	}
	
	public void setValidPersonalIdView() {
		this.remove(getPersonalIDWarningLabel());
	}
	
	public void setEmptyUsernameView() {
		this.add(getPersonalIDWarningLabel());
		getUsernameField().setWarning(AMessages.EMPTY_FIELD, getPersonalIDWarningLabel());
	}
	
	public void setUnknownUsernameView() {
		this.add(getPersonalIDWarningLabel());
		getUsernameField().setWarning("An employee with this username is not registred.", getPersonalIDWarningLabel());
	}
	
	public void setValidUsernameView() {
		this.remove(getPersonalIDWarningLabel());
	}
	
	/**
	 * @param isClient
	 */
	public void setWrongCredentialsView(boolean isClient) {
		this.add(getPersonalIDWarningLabel());
		this.add(getPasswordWarningLabel());
		
		if (isClient) {
			getPersonalIDField().setWarning(AMessages.WRONG_CREDENTIALS, getPersonalIDWarningLabel());
		}
		else {
			getUsernameField().setWarning(AMessages.WRONG_CREDENTIALS, getPersonalIDWarningLabel());
		}
		getPasswordField().setWarning(AMessages.WRONG_CREDENTIALS, getPasswordWarningLabel());
	}
	
	public void setEmptyPasswordView() {
		this.add(getPasswordWarningLabel());
		getPasswordField().setWarning(AMessages.EMPTY_FIELD, getPasswordWarningLabel());
	}
	
	public void setValidPasswordView(){
		this.remove(getPasswordWarningLabel());
	}

	public void setValidCredentialsInputView() {
		this.remove(getPersonalIDField());
		this.remove(getUsernameField());
		this.remove(getPersonalIDWarningLabel());
		this.remove(getPasswordField());
		this.remove(getPasswordWarningLabel());
		this.remove(getLoginButton());
		
		MessageLabel messageLabel = new MessageLabel(AMessages.LOGIN_WELCOME);
		messageLabel.setBounds(0, ADimension.FIELD_Y, ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
	
		this.add(messageLabel);
		this.repaint();
	}

	private JLabel getPersonalIDWarningLabel() {
		if (personalIDWarningLabel == null) {
			personalIDWarningLabel = new JLabel();
			personalIDWarningLabel.setBounds(ADimension.FIELD_X,
				       						 ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
				       						 ADimension.FIELD_WIDTH,
				       						 ADimension.LABEL_HEIGHT);
		}
		return personalIDWarningLabel;
	}
	
	private JLabel getPasswordWarningLabel() {
		if (passwordWarningLabel == null) {
			passwordWarningLabel = new JLabel();
			passwordWarningLabel.setBounds(ADimension.FIELD_X,
										   ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING + ADimension.FIELD_HEIGHT,
										   ADimension.FIELD_WIDTH,
										   ADimension.LABEL_HEIGHT);
		}
		return passwordWarningLabel;
	}
}
