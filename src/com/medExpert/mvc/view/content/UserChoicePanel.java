package com.medExpert.mvc.view.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.MyPasswordField;
import com.medExpert.mvc.view.pages.IClientChoiceButtonListener;
import com.medExpert.mvc.view.pages.IStaffPassCodeListener;
import com.medExpert.util.APath;

public class UserChoicePanel extends JPanel {
	
	private MyButton clientButton;
	private MyButton staffButton;
	private MyPasswordField passCodeField;
	private JLabel warningLabel;
	private JButton enterButton;
	private JButton goBackButton;

	private List<IClientChoiceButtonListener> clientButtonListeners;
	private List<IStaffPassCodeListener> staffButtonListeners;
	
	public UserChoicePanel() {
		build();
		clientButtonListeners = new ArrayList<IClientChoiceButtonListener>();
		staffButtonListeners = new ArrayList<IStaffPassCodeListener>();
	}
	
	public void addClientButtonListener(IClientChoiceButtonListener listener) {
		this.clientButtonListeners.add(listener);
	}
	
	public void addStaffButtonListener(IStaffPassCodeListener listener) {
		this.staffButtonListeners.add(listener);
	}

	private void build() {
		setOpaque(false);
		setLayout(null);
		setBounds(300, 300, 500, 300);
		
		// Set welcome message
		JLabel welcomeLabel = new JLabel("Welcome to MedExpert!");
		welcomeLabel.setFont(AFont.bold40());
		welcomeLabel.setBounds(5, 0, 500, 50);
		
		// Set motto message
		JLabel mottoLabel = new JLabel("We are your source for advancing health.");
		mottoLabel.setFont(AFont.plain20());
		mottoLabel.setBounds(50, 80, 500, ADimension.ROW_HEIGHT);
		
		add(welcomeLabel);
		add(mottoLabel);
		add(getClientButton());
		add(getStaffButton());
	}
	
	public MyButton getClientButton() {
		if (clientButton == null) {
			clientButton = new MyButton("CLIENT");
			clientButton.setBounds(90, 150, 125, ADimension.BUTTON_HEIGHT);
			clientButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IClientChoiceButtonListener listener : clientButtonListeners) {
						listener.clientButtonWasClicked();
					}
				}
			});
		}
		return clientButton;
	}
	
	public MyButton getStaffButton() {
		if (staffButton == null) {
			staffButton = new MyButton("STAFF");
			staffButton.setBounds(235, 150, 125, ADimension.BUTTON_HEIGHT);
			staffButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IStaffPassCodeListener listener : staffButtonListeners) {
						listener.staffButtonWasClicked();
					}
				}
			});
		}
		return staffButton;
	}
	
	public void showPassCodeArea() {
		this.remove(getClientButton());
		this.remove(getStaffButton());
		
		this.add(getGoBackButton());
		this.add(getPassCodeField());
		this.add(getEnterButton());
		this.repaint();
	}
	
	public void setWrongPassCodeView() {
		this.add(getWarningLabel());
		this.getPassCodeField().setText("");
		this.getPassCodeField().setWarning("Wrong pass code!", getWarningLabel());
		this.repaint();
	}
	
	public MyPasswordField getPassCodeField() {
		if (passCodeField == null) {
			passCodeField = new MyPasswordField("Pass code");
			passCodeField.setBounds(135, 150, 200, ADimension.BUTTON_HEIGHT);
			passCodeField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						for (IStaffPassCodeListener listener : staffButtonListeners) {
							listener.enterKeyWasPressed();
						}
					}
				}
			}); 
		}
		return passCodeField;
	}
	
	private JLabel getWarningLabel() {
		if (warningLabel == null) {
			warningLabel = new JLabel();
			warningLabel.setBounds(135, 150 + ADimension.BUTTON_HEIGHT, 200, 15);
		}
		return warningLabel;
	}
	
	private JButton getEnterButton() {
		if (enterButton == null) {
			enterButton = new JButton(new ImageIcon(getClass().getResource(APath.ENTER_PNG)));
			enterButton.setBounds(345, 150, 40,ADimension.BUTTON_HEIGHT);
			enterButton.setBorderPainted(false);
			enterButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					for (IStaffPassCodeListener listener : staffButtonListeners) {
						listener.enterButtonWasClicked();
					}
				}
			});
		}
		return enterButton;
	}
	
	private JButton getGoBackButton() {
		if (goBackButton == null) {
			goBackButton = new JButton(new ImageIcon(getClass().getResource(APath.BACK_PNG)));
			goBackButton.setBounds(90, 150, 40, ADimension.BUTTON_HEIGHT);
			goBackButton.setContentAreaFilled(false);
			goBackButton.setBorderPainted(false);
			goBackButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					reset();
				}	
			});
		}
		return goBackButton;
	}

	public void reset() {
		remove(getGoBackButton());
		remove(getPassCodeField());
		remove(getEnterButton());
		remove(getWarningLabel());
		
		add(getClientButton());
		add(getStaffButton());
		repaint();
	}
}
