package com.medExpert.mvc.view.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.MyPasswordField;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;
import com.medExpert.util.Util;

public class NewClientPanel extends AContentPanel {

	private MyTextField personalIdField;
	private JLabel personalIdWarningLabel;
	private MyTextField firstNameField;
	private JLabel firstNameWarningLabel;
	private MyTextField lastNameField;
	private JLabel lastNameWarningLabel;
	private PasswordComplexityInfoLabel passwordComplexityLabel;
	private MyPasswordField passwordField;
	private JLabel passwordWarningLabel;
	private MyPasswordField confirmPasswordField;
	private JLabel confirmPasswordWarningLabel;
	private MyButton createAccountButton;
	
	private List<IButtonListener> listeners;
	
	public NewClientPanel() {
		super(CONTENT_ID.NEW_CLIENT, APath.ACCOUNT_PNG);

		listeners = new ArrayList<IButtonListener>();
	}

	@Override
	public void buildContent() {
		this.add(getPersonalIdField());
		this.add(getFirstNameField());
		this.add(getLastNameField());
		this.add(getPasswordComplexityLabel());
		this.add(getPasswordField());
		this.add(getConfirmPasswordField());
		this.add(getCreateAccountButton());
	}
	
	public void addNewAccountButtonListener(IButtonListener listener) {
		this.listeners.add(listener);
	}
	
	public void setEmptyPersonalIdInputView() {
		this.add(getPersonalIdWarningLabel());
		getPersonalIdField().setWarning(AMessages.EMPTY_FIELD, getPersonalIdWarningLabel());
	}
	
	public void setDigitsLimitPersonalIdInputView() {
		this.add(getPersonalIdWarningLabel());
		getPersonalIdField().setWarning(AMessages.ID_SIZE, getPersonalIdWarningLabel());
	}
	
	public void setDuplicatePersonalIdInputView() {
		this.add(getPersonalIdWarningLabel());
		getPersonalIdField().setWarning("An account with this ID already exists.", getPersonalIdWarningLabel());
	}
	
	public void setWrongPersonalIdInputView() {
		this.add(getPersonalIdWarningLabel());
		getPersonalIdField().setWarning("Personal ID cannot start with 0.", getPersonalIdWarningLabel());
	}
	
	public void setValidPersonalIdInputView() {
		this.remove(getPersonalIdWarningLabel());
	}
	
	public void setEmptyFirstNameInputView() {
		this.add(getFirstNameWarningLabel());
		getFirstNameField().setWarning(AMessages.EMPTY_FIELD, getFirstNameWarningLabel());
	}
	
	public void setInvalidFirstNameInputView() {
		this.add(getFirstNameWarningLabel());
		getFirstNameField().setWarning(AMessages.INVALID_FIRST_NAME, getFirstNameWarningLabel());
	}
	
	public void setValidFirstNameInput() {
		this.remove(getFirstNameWarningLabel());
		getFirstNameField().setText(Util.getInstance().capitalizeAllFirstLetters(getFirstNameField().getText()));
	}
	
	public void setEmptyLastNameInputView() {
		this.add(getLastNameWarningLabel());
		getLastNameField().setWarning(AMessages.EMPTY_FIELD, getLastNameWarningLabel());
	}
	
	public void setInvalidLastNameInputView() {
		this.add(getLastNameWarningLabel());
		getLastNameField().setWarning(AMessages.INVALID_LAST_NAME, getLastNameWarningLabel());
	}
	
	public void setValidLastNameInput() {
		this.remove(getLastNameWarningLabel());
		getLastNameField().setText(Util.getInstance().capitalizeAllFirstLetters(getLastNameField().getText()));
	}
	
	public void setEmptyPasswordInputView() {
		this.add(getPasswordWarningLabel());
		getPasswordField().setWarning(AMessages.EMPTY_FIELD, getPasswordWarningLabel());
	}
	
	public void setNotStrongEnoughtPasswordInputView() {
		this.add(getPasswordWarningLabel());
		getPasswordField().setWarning(AMessages.NOT_STRONG_PASSWORD, getPasswordWarningLabel());
	}
	
	public void setValidPasswordInput() {
		this.remove(getPasswordWarningLabel());
	}
	
	public void setEmptyConfirmPasswordInputView() {
		this.add(getConfirmPasswordWarningLabel());
		getConfirmPasswordField().setWarning(AMessages.EMPTY_FIELD, getConfirmPasswordWarningLabel());
	}
	
	public void setDifferentPasswordsInputView() {
		this.add(getConfirmPasswordWarningLabel());
		getConfirmPasswordField().setWarning(AMessages.DIFFERENT_PASSWORD, getConfirmPasswordWarningLabel());
	}
	
	public void setValidConfirmPasswordView() {
		this.remove(getConfirmPasswordWarningLabel());
	}
	
	public void setValidClientDataView(boolean isCLient) {
		this.remove(getPersonalIdField());
		this.remove(getFirstNameField());
		this.remove(getLastNameField());
		this.remove(getPasswordComplexityLabel());
		this.remove(getPasswordField());
		this.remove(getConfirmPasswordField());
		
		if (isCLient) {
			this.add(getLoginRequestMessageLabel());
		}
		this.add(getSuccessfulMessageLabel());
	}
	
	public MyTextField getPersonalIdField() {
		if (personalIdField == null) {
			personalIdField = new MyTextField("Personal ID");
			// Limit the number of input characters to 13.
			personalIdField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (personalIdField.getText().length() >= 13) {
						e.consume();
						personalIdField.setToolTipText(AMessages.LIMIT_13_TOOLTIP);
					}
				}
			});
			personalIdField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y,
							  		  ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return personalIdField;
	}
	
	public MyTextField getFirstNameField() {
		if (firstNameField == null) {
			firstNameField = new MyTextField("First name");
			firstNameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
									 ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return firstNameField;
	}
	
	public MyTextField getLastNameField() {
		if (lastNameField == null) {
			lastNameField = new MyTextField("Last name");
			lastNameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (2 * ADimension.FIELD_Y_SPACING),
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return lastNameField;
	}
	
	private PasswordComplexityInfoLabel getPasswordComplexityLabel() {
		if (passwordComplexityLabel == null) {
			passwordComplexityLabel = new PasswordComplexityInfoLabel();
			passwordComplexityLabel.setBounds(ADimension.FIELD_X,
											  ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING),
											  ADimension.FIELD_WIDTH,
											  3 * ADimension.FIELD_HEIGHT);
		}
		return passwordComplexityLabel;
	}
	
	public MyPasswordField getPasswordField() {
		if (passwordField == null) {
			passwordField = new MyPasswordField("Password");
			passwordField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (5 * ADimension.FIELD_Y_SPACING),
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return passwordField;
	}

	public MyPasswordField getConfirmPasswordField() {
		if (confirmPasswordField == null) {
			confirmPasswordField = new MyPasswordField("Confirm password");
			confirmPasswordField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (6 * ADimension.FIELD_Y_SPACING),
										   ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return confirmPasswordField;
	}
	
	public MyButton getCreateAccountButton() {
		if (createAccountButton == null) {
			createAccountButton = new MyButton("Create account");
			createAccountButton.setBounds(ADimension.BUTTON_X, ADimension.BUTTON_Y, ADimension.BUTTON_WIDTH, ADimension.BUTTON_HEIGHT);
			createAccountButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IButtonListener listener : listeners) {
						listener.buttonWasClicked();
					}
					repaint();
				}
			});
		}
		return createAccountButton;
	}

	private JLabel getPersonalIdWarningLabel() {
		if (personalIdWarningLabel == null) {
			personalIdWarningLabel = new JLabel();
			personalIdWarningLabel.setBounds(ADimension.FIELD_X,
											 ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
											 ADimension.FIELD_WIDTH,
											 ADimension.INFO_LABEL_HEIGHT);
		}
		return personalIdWarningLabel;
	}
	
	private JLabel getFirstNameWarningLabel() {
		if (firstNameWarningLabel == null) {
			firstNameWarningLabel = new JLabel();
			firstNameWarningLabel.setBounds(ADimension.FIELD_X,
					  						ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + ADimension.FIELD_Y_SPACING,
					  						ADimension.FIELD_WIDTH,
					  						ADimension.INFO_LABEL_HEIGHT);
		}
		return firstNameWarningLabel;
	}
	
	private JLabel getLastNameWarningLabel() {
		if (lastNameWarningLabel == null) {
			lastNameWarningLabel = new JLabel();
			lastNameWarningLabel.setBounds(ADimension.FIELD_X,
					   					   ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (2 * ADimension.FIELD_Y_SPACING),
					   					   ADimension.FIELD_WIDTH,
					   					   ADimension.INFO_LABEL_HEIGHT);
		}
		return lastNameWarningLabel;
	}
	
	private JLabel getPasswordWarningLabel() {
		if (passwordWarningLabel == null) {
			passwordWarningLabel = new JLabel();
			passwordWarningLabel.setBounds(ADimension.FIELD_X,
					   					   ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (5 * ADimension.FIELD_Y_SPACING),
					   					   ADimension.FIELD_WIDTH,
					   					   ADimension.INFO_LABEL_HEIGHT);
		}
		return passwordWarningLabel;
	}
	
	private JLabel getConfirmPasswordWarningLabel() {
		if (confirmPasswordWarningLabel == null) {
			confirmPasswordWarningLabel = new JLabel();
			confirmPasswordWarningLabel .setBounds(ADimension.FIELD_X,
					   							   ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (6 * ADimension.FIELD_Y_SPACING),
					   							   ADimension.FIELD_WIDTH,
					   							   ADimension.INFO_LABEL_HEIGHT);
		}
		return confirmPasswordWarningLabel;
	}
	

	private MessageLabel getSuccessfulMessageLabel() {
		MessageLabel messageLabel = new MessageLabel("The account was succesfully created.");
		messageLabel.setBounds(0, ADimension.FIELD_Y,
				               ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		return messageLabel;
	}
	
	private MessageLabel getLoginRequestMessageLabel() {
		MessageLabel messageLabel = new MessageLabel("Go to login screen.");
		messageLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
				               ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		return messageLabel;
	}	
}
