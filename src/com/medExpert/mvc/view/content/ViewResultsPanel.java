package com.medExpert.mvc.view.content;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.MyTable;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.content.appointment.InfoLabel;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;

public class ViewResultsPanel extends AContentPanel {

	private MyTextField clientPersonalIdField;
	private JLabel clientPersonalIdWarningLabel;
	private MyButton searchAppointmentsButton;
	private JScrollPane appointmentsScrollPane;
	private List<JCheckBox> appointmentCheckBoxes;
	private Appointment selectedAppointment;
	private InfoLabel appointmentsInfoLabel;
	private JScrollPane testsListScrollPane;
	private MyTable testsTable;
	
	private List<IButtonListener> searchButtonListeners;
	private List<IAppointmentCheckBoxListener> checkBoxListeners;

	public ViewResultsPanel() {
		super(CONTENT_ID.VIEW_RESULTS, APath.RESULTS_PNG);
		searchButtonListeners = new ArrayList<IButtonListener>();
		checkBoxListeners = new ArrayList<IAppointmentCheckBoxListener>();
	}

	@Override
	public void buildContent() {
		if (UserChoiceController.getInstance().getClient() != null) {
			List<Appointment> appointments = MedExpertDB.getInstance().getAppointmentsByClientId(
					UserChoiceController.getInstance().getClient().getId());
			
			getAppointmentsInfoLabel().setBounds(0, ADimension.FIELD_Y, ADimension.CONTENT_WIDTH, ADimension.INFO_LABEL_HEIGHT);
			getAppointmentsScrollPane(appointments).setBounds(0, ADimension.FIELD_Y + ADimension.INFO_LABEL_HEIGHT,
													 ADimension.CONTENT_WIDTH, (2 + appointments.size()) * ADimension.INFO_LABEL_HEIGHT);
			this.add(getAppointmentsInfoLabel());
			this.add(getAppointmentsScrollPane(appointments));
		}
		else if (UserChoiceController.getInstance().getEmployee() != null) {
			this.add(getClientPersonalIdField());
			this.add(getSearchAppointmentsButton());
		}
	}
	
	public void addSearchTestsButtonListeners(IButtonListener listener) {
		this.searchButtonListeners.add(listener);
	}

	public void addCheckBoxListener(IAppointmentCheckBoxListener listener) {
		this.checkBoxListeners.add(listener);
	}
	
	public void setEmptyClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.EMPTY_FIELD, getClientPersonalIdWarningLabel());
	}
	
	public void setUnknownClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning("A client with this ID is not registered.", getClientPersonalIdWarningLabel());
	}
	
	public void setDigitsLimitPersonalIdInputView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.ID_SIZE, getClientPersonalIdWarningLabel());
	}

	public void setSearchAppointmentsView(List<Appointment> appointmentsList) {
		this.remove(getClientPersonalIdWarningLabel());
		this.add(getAppointmentsInfoLabel());
		this.add(getAppointmentsScrollPane(appointmentsList));
	}
	
	public void setTestsListView(List<Test> testsList) {
		if (testsListScrollPane != null) {
			this.remove(getTestsListScrollPane());
		}
		this.testsListScrollPane = null;
		this.add(getTestsListScrollPane(testsList));
	}
	
	public void setAppointmentTestsView(List<Test> testList) {
		this.add(getTestsListScrollPane(testList));
	}
	
	public void setUnselectedAppointmentView() {
		this.remove(getTestsListScrollPane());
	}
	
	public Appointment getSelectedAppointment() {
		return selectedAppointment;
	}
	
	public MyTextField getClientPersonalIdField() {
		if (clientPersonalIdField == null) {
			clientPersonalIdField = new MyTextField("Enter client's personal ID");
			clientPersonalIdField.setBounds(0, ADimension.FIELD_Y, ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
			clientPersonalIdField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (clientPersonalIdField.getText().length() >= 13) {
						clientPersonalIdField.setToolTipText(AMessages.LIMIT_13_TOOLTIP);
						e.consume();
					}
				}
			});
		}
		return clientPersonalIdField;
	}
	
	public MyButton getSearchAppointmentsButton() {
		if (searchAppointmentsButton == null) {
			searchAppointmentsButton = new MyButton("Search appointments");
			searchAppointmentsButton.setBounds(ADimension.FIELD_WIDTH + (ADimension.CONTENT_WIDTH - ADimension.FIELD_WIDTH - ADimension.BUTTON_WIDTH),
										ADimension.FIELD_Y + ((ADimension.FIELD_HEIGHT - ADimension.BUTTON_HEIGHT) / 2),
										ADimension.BUTTON_WIDTH,
										ADimension.BUTTON_HEIGHT);
			searchAppointmentsButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IButtonListener listener : searchButtonListeners) {
						listener.buttonWasClicked();
					}
					repaint();
				}
			});
		}
		return searchAppointmentsButton;
	}
	
	public JScrollPane getAppointmentsScrollPane(List<Appointment> appointmentList) {
		if (appointmentsScrollPane == null) {
			
			JPanel panel = new JPanel(new GridLayout(appointmentList.size(), 1));
			panel.setBackground(Color.white);

			appointmentCheckBoxes = new ArrayList<JCheckBox>();
			for (Appointment appointment : appointmentList) {
				JCheckBox checkBox = buildCheckBox(appointment);
				appointmentCheckBoxes.add(checkBox);
				panel.add(checkBox);
			}
			
			
			appointmentsScrollPane = new JScrollPane(panel);
			appointmentsScrollPane.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING + ADimension.INFO_LABEL_HEIGHT,
					 						 ADimension.CONTENT_WIDTH, 2 * ADimension.ROW_HEIGHT);
			appointmentsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			appointmentsScrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
		}
		return appointmentsScrollPane;
	}
	
	public JScrollPane getAppointmentsScrollPane() {
		return appointmentsScrollPane;
	}
	
	public JCheckBox buildCheckBox(Appointment appointment) {
		JCheckBox checkBox = new JCheckBox();
		checkBox.setText(appointment.getDate() + " " + appointment.getMonth() + " at " + appointment.getTime());
		checkBox.setFont(AFont.plain15());
		checkBox.setPreferredSize(new Dimension(ADimension.CONTENT_WIDTH - 20, ADimension.ROW_HEIGHT));
		checkBox.addMouseListener(new MouseAdapter() {
				
			@Override
			public void mouseReleased(MouseEvent e) {
				if (checkBox.isSelected()) {
					
					for (JCheckBox checkBox : appointmentCheckBoxes) {
						checkBox.setSelected(false);
					}
					
					checkBox.setSelected(true);
					selectedAppointment = appointment;
					
					for (IAppointmentCheckBoxListener listener : checkBoxListeners) {
						listener.checkBoxIsSelected();
					}
				} 
				else if (!checkBox.isSelected()) {
					for (IAppointmentCheckBoxListener listener : checkBoxListeners) {
						listener.checkBoxIsDeselected();
					}
				}
				ViewResultsPanel.this.repaint();
			}
		});
		return checkBox;
	}
	
	public JScrollPane getTestsListScrollPane(List<Test> testsList) {
		if (testsListScrollPane == null) {
			
		String[] colNames = {"   Test", "Result", "Unit"};
			
			String[] tests = new String[testsList.size()];
			for (int index = 0; index < testsList.size(); index ++) {
				tests[index] = testsList.get(index).getName();
			}

			Double[] results = new Double[testsList.size()];
			for (int index = 0; index < testsList.size(); index ++) {
				results[index] = testsList.get(index).getResult();
			}
			
			String[] units = new String[testsList.size()];
			for (int index = 0; index < testsList.size(); index ++) {
				units[index] = testsList.get(index).getUnit();
			}
			
			// Populate data matrix.
			String[][] data = new String[testsList.size()][colNames.length];
			for (int rowIndex = 0; rowIndex < testsList.size(); rowIndex++) {
				data[rowIndex][0] = "   " + tests[rowIndex];
				data[rowIndex][1] = "" + results[rowIndex];
				data[rowIndex][2] = units[rowIndex];
			}
	
			testsTable = new MyTable(data, colNames);
			testsTable.setDefaultEditor(Object.class, null);
			testsTable.getColumnModel().getColumn(2).setCellRenderer(testsTable.getMyCellRenderer());
			
			testsListScrollPane = new JScrollPane(testsTable);
			testsListScrollPane.setBounds(0,
					ADimension.FIELD_Y + (2 * ADimension.FIELD_Y_SPACING) + ADimension.ROW_HEIGHT + ADimension.INFO_LABEL_HEIGHT,
					ADimension.CONTENT_WIDTH, (testsList.size() + 2) * ADimension.ROW_HEIGHT);
			testsListScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			testsListScrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
		}
		return testsListScrollPane;
	}
	
	private JScrollPane getTestsListScrollPane() {
		return this.testsListScrollPane;
	}
	
	public MyTable getTestsTable() {
		return testsTable;
	}
	
	private JLabel getClientPersonalIdWarningLabel() {
		if (clientPersonalIdWarningLabel == null) {
			clientPersonalIdWarningLabel = new JLabel();
			clientPersonalIdWarningLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
												   ADimension.FIELD_WIDTH,ADimension.INFO_LABEL_HEIGHT);
		}
		return clientPersonalIdWarningLabel;
	}
	
	public InfoLabel getAppointmentsInfoLabel() {
		if (appointmentsInfoLabel == null) {
			appointmentsInfoLabel = new InfoLabel("Select appointment");
			appointmentsInfoLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
									 ADimension.CONTENT_WIDTH + 10, ADimension.INFO_LABEL_HEIGHT);
		}
		return appointmentsInfoLabel;
	}
}
