package com.medExpert.mvc.view.content.appointment;

public interface IScheduleAppointmentButtonListener {

	public void scheduleAppointmentButtonWasClicked();
}
