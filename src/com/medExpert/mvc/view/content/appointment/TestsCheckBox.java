package com.medExpert.mvc.view.content.appointment;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JCheckBox;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;

public class TestsCheckBox extends JCheckBox {
	
	private String testName;
	private List<String> selectedTestsList;
	
	public TestsCheckBox(String testName, List<String> selectedTestsList) {
		this.testName = testName;
		this.selectedTestsList = selectedTestsList;
		customise();
	}

	private void customise() {
		setText(testName);
		setFont(AFont.plain15());
		setPreferredSize(new Dimension(ADimension.CALENDAR_WIDTH - 20, ADimension.ROW_HEIGHT));
		addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (isSelected()) {
					selectedTestsList.add(testName);
				} else {
					selectedTestsList.remove(testName);
				}
			}
		});
	}
}
