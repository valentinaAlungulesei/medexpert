package com.medExpert.mvc.view.content.appointment;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.medExpert.mvc.view.AFont;

public class InfoLabel extends JLabel {
	
	private String title;
	
	public InfoLabel(String title) {
		this.title = title;
		configure();
	}

	private void configure() {
		setText(title);
		setFont(AFont.italic13());
		setForeground(Color.white);
		setHorizontalAlignment(SwingConstants.LEFT);
	}
}
