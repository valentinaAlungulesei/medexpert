package com.medExpert.mvc.view.content.appointment;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.MyComboBox;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.content.AContentPanel;
import com.medExpert.mvc.view.content.appointment.calendar.MyCalendar;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;
import com.medExpert.util.TestsListStore;

public class AppointmentPanel extends AContentPanel {

	private List<String> selectedTestsList;
	private String selectedTime;

	private MyTextField clientPersonalIdField;
	private JLabel clientPersonalIdWarningLabel;
	private InfoLabel testListInfoLabel;
	private JScrollPane testListScrollPane;
	private InfoLabel dateInfoLabel;
	private MyCalendar calendar;
	private InfoLabel timeInfoLabel;
	private MyComboBox hoursComboBox;
	private MyButton submitButton;
	private InfoLabel warningLabel;
	private MessageLabel validAppointmentLabel;
	private JPanel appointmentDataPanel;
	
	List<IScheduleAppointmentButtonListener> sheduleAppointmentListenersList;

	public AppointmentPanel() {
		super(CONTENT_ID.APPOINTMENT, APath.CALENDAR_PNG);
		sheduleAppointmentListenersList = new ArrayList<IScheduleAppointmentButtonListener>();
	}

	@Override
	public void buildContent() {
		
		if (UserChoiceController.getInstance().getEmployee() != null &&
			UserChoiceController.getInstance().getEmployee().isLoggedIn()) {
			this.getTestListInfoLabel().setBounds(ADimension.FIELD_X,
												  ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
												  ADimension.CALENDAR_WIDTH,
												  ADimension.INFO_LABEL_HEIGHT);
			this.getTestListScrollPane().setBounds(ADimension.FIELD_X,
												   ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING + ADimension.INFO_LABEL_HEIGHT,
												   ADimension.CALENDAR_WIDTH,
												   2 * ADimension.FIELD_HEIGHT);
			this.add(getClientPersonalIdField());
		}
		
		this.add(getTestListInfoLabel());
		this.add(getTestListScrollPane());
		this.add(getDateInfoLabel());
		this.add(getTimeInfoLabel());
		this.add(getCalendar());
		this.add(getHoursComboBox());
		this.add(getWarningLabel());
		this.add(getSubmitButton());
	}
	
	public void setLoginRequestView() {
		this.remove(getTestListInfoLabel());
		this.remove(getTestListScrollPane());
		this.remove(getDateInfoLabel());
		this.remove(getTimeInfoLabel());
		this.remove(getCalendar());
		this.remove(getHoursComboBox());
		this.remove(getWarningLabel());
		this.remove(getSubmitButton());
		
		super.setLoginRequestView();
		this.repaint();
	}
	
	public void setEmptyClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.EMPTY_FIELD, getClientPersonalIdWarningLabel());
	}
	
	public void setUnknownClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning("A client with this ID is not registered.", getClientPersonalIdWarningLabel());
	}
	
	public void setDigitsLimitPersonalIdInputView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.ID_SIZE, getClientPersonalIdWarningLabel());
	}
	
	public void setValidPersonalIdView() {
		this.remove(getClientPersonalIdWarningLabel());
	}
	
	public void setEmptyTestListSelectionView() {
		getTestListScrollPane().setBorder((new LineBorder(Color.red, 1)));
		getWarningLabel().setVisible(true);
	}

	public void setValidTestsSelectionView() {
		getTestListScrollPane().setBorder((new LineBorder(Color.white, 1)));
		getWarningLabel().setVisible(false);
	}
	
	public void setEmptyDateSelectionView() {
		getCalendar().setBorder(new LineBorder(Color.red, 1));
		warningLabel.setVisible(true);
	}
	
	public void setValidDateSelectionView() {
		getCalendar().setBorder(new LineBorder(Color.white, 1));
		getWarningLabel().setVisible(false);
	}
	
	public void setEmptyHourSelectionView() {
		getHoursComboBox().setBorder(new LineBorder(Color.red, 1));
		getWarningLabel().setVisible(true);
	}
	
	public void setValidHourSelectionView() {
		getHoursComboBox().setBorder(new LineBorder(Color.white, 1));
		getWarningLabel().setVisible(false);
	}
	
	public void setValidAppointmentView() {
		getWarningLabel().setVisible(false);
		this.remove(getClientPersonalIdField());
		this.remove(getClientPersonalIdWarningLabel());
		this.remove(getTestListInfoLabel());
		this.remove(getTestListScrollPane());
		this.remove(getDateInfoLabel());
		this.remove(getCalendar());
		this.remove(getTimeInfoLabel());
		this.remove(getHoursComboBox());
		this.remove(getSubmitButton());

		this.add(getValidAppointmentLabel());
		this.add(getAppointmentDataTextArea());
		this.repaint();
	}
	
	public List<String> getSelectedTestsList() {
		if (selectedTestsList == null) {
			selectedTestsList = new ArrayList<String>();
		}
		return selectedTestsList;
	}
	
	public String getSelectedTime() {
		return selectedTime;
	}
	
	public void addAppointmentScheduleListener(IScheduleAppointmentButtonListener listener) {
		this.sheduleAppointmentListenersList.add(listener);
	}
	
	public MyTextField getClientPersonalIdField() {
		if (clientPersonalIdField == null) {
			clientPersonalIdField = new MyTextField("Client's personal ID");
			clientPersonalIdField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y,
											ADimension.CALENDAR_WIDTH, ADimension.FIELD_HEIGHT);
			// Limit the number of input characters to 13
			clientPersonalIdField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (clientPersonalIdField.getText().length() >= 13) {
						e.consume();
						clientPersonalIdField.setToolTipText(AMessages.LIMIT_13_TOOLTIP);
					}
				}
			});
		}
		return clientPersonalIdField;
	}
	
	private JScrollPane getTestListScrollPane() {
		if (testListScrollPane == null) {
	
			JPanel panel = new JPanel(new GridLayout(TestsListStore.getInstance().getTestList().size(), 1));
			panel.setBackground(Color.white);

			for (String test : TestsListStore.getInstance().getTestPricesMap().keySet()) {
				panel.add(new TestsCheckBox(test, getSelectedTestsList()));
			}
		
			testListScrollPane = new JScrollPane(panel);
			testListScrollPane.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + ADimension.INFO_LABEL_HEIGHT,
										 ADimension.CALENDAR_WIDTH, (2 * ADimension.FIELD_Y_SPACING) + ADimension.LABEL_HEIGHT);
			testListScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			testListScrollPane.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		}
		return testListScrollPane;
	}

	public MyCalendar getCalendar() {
		if (calendar == null) {
			calendar = new MyCalendar();
			calendar.setBounds(ADimension.FIELD_X,
							   ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING),
							   ADimension.CALENDAR_WIDTH,
							   ADimension.CALENDAR_HEIGHT);
			calendar.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					calendar.setBorder(BorderFactory.createLineBorder(Color.white, 1));
				}
			});
		}
		return calendar;
	}

	private MyComboBox getHoursComboBox() {
		if (hoursComboBox == null) {
			
			selectedTime = "";

			String[] hours = {"", "  8 : 00", "  9 : 00", "10 : 00", "11 : 00", "12 : 00",
							   "13 : 00", "14 : 00", "15 : 00", "16 : 00" };
			
			hoursComboBox = new MyComboBox(hours);
			hoursComboBox.setBounds((ADimension.CONTENT_WIDTH / 2) - (ADimension.BUTTON_WIDTH / 2), 
					ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING) + ADimension.CALENDAR_HEIGHT + ADimension.FIELD_HEIGHT - 10,
					ADimension.COMBO_BOX_WIDTH, ADimension.ROW_HEIGHT);
			hoursComboBox.setSelectedIndex(0);
			hoursComboBox.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					selectedTime = hoursComboBox.getSelectedItem().toString().replaceAll(" ", "");
				}
			});
		}
		return hoursComboBox;
	}

	public MyButton getSubmitButton() {
		if (submitButton == null) {
			submitButton = new MyButton("Submit");
			submitButton.setBounds(ADimension.BUTTON_X, ADimension.BUTTON_Y, ADimension.COMBO_BOX_WIDTH, ADimension.BUTTON_HEIGHT);
			submitButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					for (IScheduleAppointmentButtonListener listener : sheduleAppointmentListenersList) {
						listener.scheduleAppointmentButtonWasClicked();
					}
				}
			});
		}
		return submitButton;
	}
	
	private JLabel getClientPersonalIdWarningLabel() {
		if (clientPersonalIdWarningLabel == null) {
			clientPersonalIdWarningLabel = new JLabel();
			clientPersonalIdWarningLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
												   ADimension.CONTENT_WIDTH, ADimension.INFO_LABEL_HEIGHT);
		}
		return clientPersonalIdWarningLabel;
	}
	
	private InfoLabel getWarningLabel() {
		if (warningLabel == null) {
			warningLabel = new InfoLabel("Fields marked with * must be completed!");
			warningLabel.setForeground(Color.red);
			warningLabel.setBounds(ADimension.FIELD_X, ADimension.BUTTON_Y - ADimension.INFO_LABEL_HEIGHT,
								   ADimension.CONTENT_WIDTH, ADimension.INFO_LABEL_HEIGHT);
			warningLabel.setVisible(false);
		}
		return warningLabel;
	}
	
	private InfoLabel getTestListInfoLabel() {
		if (testListInfoLabel == null) {
			testListInfoLabel = new InfoLabel("Select tests *");
			testListInfoLabel.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y,
									    ADimension.FIELD_WIDTH, ADimension.INFO_LABEL_HEIGHT);
		}
		return testListInfoLabel;
	}
	
	private InfoLabel getDateInfoLabel() {
		if (dateInfoLabel == null) {
			dateInfoLabel = new InfoLabel("Select date *");
			dateInfoLabel.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING) - ADimension.INFO_LABEL_HEIGHT,
									ADimension.CALENDAR_WIDTH, ADimension.INFO_LABEL_HEIGHT);
		}
		return dateInfoLabel;
	}
	
	private InfoLabel getTimeInfoLabel() {
		if (timeInfoLabel == null) {
			timeInfoLabel = new InfoLabel("Select time *");
			timeInfoLabel.setBounds((ADimension.CONTENT_WIDTH / 2) - (ADimension.BUTTON_WIDTH / 2),
					ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING) + ADimension.CALENDAR_HEIGHT + ADimension.FIELD_HEIGHT - ADimension.INFO_LABEL_HEIGHT - 10,
									ADimension.FIELD_WIDTH, ADimension.INFO_LABEL_HEIGHT);
		}
		return timeInfoLabel;
	}
	
	private MessageLabel getValidAppointmentLabel() {
		if (validAppointmentLabel == null) {
			String message = "The appointment has been sheduled succsessfully.";
			
			validAppointmentLabel = new MessageLabel(message);
			validAppointmentLabel.setBounds(0, ADimension.FIELD_Y,
											ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		}
		return validAppointmentLabel;
	}
	
	private JPanel getAppointmentDataTextArea() {
		if (appointmentDataPanel == null) {
			appointmentDataPanel = new JPanel(new GridLayout(4, 2, 1, 1));
			appointmentDataPanel.setOpaque(false);
			appointmentDataPanel.setBounds(0,
										   ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
										   ADimension.CONTENT_WIDTH - (ADimension.CONTENT_WIDTH / 4),
										   getSelectedTestsList().size() * ADimension.FIELD_Y_SPACING);
			appointmentDataPanel.add(buildDataTitleLabel("Tests: "));
			appointmentDataPanel.add(buildTestsListDataPanel());
			appointmentDataPanel.add(buildDataTitleLabel("Date: "));
			appointmentDataPanel.add(buildDataTextArea( getCalendar().getSelectedDate() + " " + getCalendar().getSelectedMonth()));
			appointmentDataPanel.add(buildDataTitleLabel("Time: "));
			appointmentDataPanel.add(buildDataTextArea(getSelectedTime()));
		}
		return appointmentDataPanel;
	}
	
	private JLabel buildDataTitleLabel(String title) {
		JLabel label = new JLabel(title);
		label.setFont(AFont.bold15());
		label.setForeground(Color.white);
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setVerticalAlignment(SwingConstants.TOP);
		
		return label;
	}
	
	private JPanel buildTestsListDataPanel() {
		JPanel panel = new JPanel(new GridLayout(getSelectedTestsList().size(), 1, 1, 1));
		panel.setOpaque(false);
		
		for (String test : getSelectedTestsList()) {
			panel.add(buildDataTextArea(test));
		}
		return panel;
	}
	
	private JTextArea buildDataTextArea(String data) {
		JTextArea textArea = new JTextArea();
		textArea.setText(data);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setFont(AFont.plain15());
		textArea.setForeground(Color.white);
		textArea.setEditable(false);
		textArea.setOpaque(false);
		
		return textArea;
	}
}
