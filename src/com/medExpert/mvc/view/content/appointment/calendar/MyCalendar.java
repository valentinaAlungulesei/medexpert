package com.medExpert.mvc.view.content.appointment.calendar;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicArrowButton;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.util.AMessages;

public class MyCalendar extends JPanel {

	private int currentYear;
	private Month currentMonth;
	private String selectedMonth;
	private byte selectedDate;
	
	private BasicArrowButton previousMonthButton, nextMonthButton;
	private JLabel currentMonthLabel;
	private JPanel monthPanel;
	private JLabel daysNamesLabel;
	private JPanel daysPanel;
	private List<DateButton> dateButtonsList;
	
	public MyCalendar() {
		this.currentYear = LocalDate.now().getYear();
		this.currentMonth = LocalDate.now().getMonth();
		
		configure();
		build();
	}
	
	public List<DateButton> getDateButtonsList() {
		if (dateButtonsList == null) {
			dateButtonsList = new ArrayList<DateButton>();
		}
		return dateButtonsList;
	}

	public String getSelectedMonth() {
		return selectedMonth;
	}
	
	public byte getSelectedDate() {
		return selectedDate;
	}

	private void configure() {
		setSize(ADimension.CALENDAR_WIDTH, ADimension.CALENDAR_HEIGHT);
		setBackground(Color.white);
		setLayout(null);
		setVisible(true);
	}
	
	private void build() {
		add(getPreviousMonthButton());
		add(getCurrentMonthLabel());
		add(getNextMonthButton());
		add(getMonthPanel());
	}
	
	private BasicArrowButton getPreviousMonthButton() {
		if (previousMonthButton == null) {
			previousMonthButton = new BasicArrowButton(BasicArrowButton.WEST, null, null, AColor.darck(), null);
			previousMonthButton.setBorderPainted(true);
			previousMonthButton.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
			previousMonthButton.setBounds(5, 5, ADimension.ROW_HEIGHT - 2, ADimension.ROW_HEIGHT - 2);
			previousMonthButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					currentMonth = currentMonth.minus(1);
					getCurrentMonthLabel().setText(currentMonth + " " + currentYear);
					nextMonthButton.setEnabled(true); // If it was disabled.
					
					// Disables the previousMonthButton if the currentMonth is first of the year.	
					if (currentMonth == Month.JANUARY) {
						previousMonthButton.setEnabled(false);
					} 
					updateMonthPanel();
				}
			});
		}
		return previousMonthButton;
	}

	private BasicArrowButton getNextMonthButton() {
		if (nextMonthButton == null) {
			nextMonthButton = new BasicArrowButton(BasicArrowButton.EAST, null, null, AColor.darck(), null);
			nextMonthButton.setBounds(ADimension.CALENDAR_WIDTH - 5 - ADimension.ROW_HEIGHT, 5,
									  ADimension.ROW_HEIGHT - 2, ADimension.ROW_HEIGHT - 2);
			nextMonthButton.setBorderPainted(true);
			nextMonthButton.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
			nextMonthButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					currentMonth = currentMonth.plus(1);
					getCurrentMonthLabel().setText(currentMonth + " " + currentYear);
					previousMonthButton.setEnabled(true); // if it was disabled.

					// Disables the nextMonthButton if the currentMonth is last of the year.
					if (currentMonth == Month.DECEMBER) {
						nextMonthButton.setEnabled(false);
					} 
					updateMonthPanel();
				}
			});
		}
		return nextMonthButton;
	}
	
	private JLabel getCurrentMonthLabel() {
		if (currentMonthLabel == null) {
			currentMonthLabel = new JLabel(currentMonth + "  " + currentYear);
			currentMonthLabel.setHorizontalAlignment(SwingConstants.CENTER);
			currentMonthLabel.setVerticalAlignment(SwingConstants.BOTTOM);
			currentMonthLabel.setFont(AFont.bold12());
			currentMonthLabel.setBounds(ADimension.CALENDAR_WIDTH / 4, 0,
										ADimension.CALENDAR_WIDTH / 2, ADimension.ROW_HEIGHT);
		}
		return currentMonthLabel;
	}
	
	private JPanel getMonthPanel() {
		if (monthPanel == null) {
			monthPanel = new JPanel();
			monthPanel.setBounds(ADimension.MONTH_PANEL_X_INSET,
								 ADimension.ROW_HEIGHT + (2 * ADimension.CALENDAR_SPACING),
								 ADimension.CALENDAR_WIDTH - (2 * ADimension.MONTH_PANEL_X_INSET),
								 ADimension.CALENDAR_HEIGHT - (4 * ADimension.CALENDAR_SPACING) - ADimension.ROW_HEIGHT);
			monthPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
			monthPanel.setLayout(null);
			monthPanel.add(getDaysNamesLabel());
			monthPanel.add(getDaysPanel());
		}
		return monthPanel;
	}
	
	private JLabel getDaysNamesLabel() {
		if (daysNamesLabel == null) {
			daysNamesLabel = new JLabel();
			daysNamesLabel.setLayout(new GridLayout(1, 7));
			daysNamesLabel.setBounds(ADimension.CALENDAR_SPACING, 
								     ADimension.CALENDAR_SPACING,
									 ADimension.CALENDAR_WIDTH - (2 * ADimension.CALENDAR_SPACING) - (2 * ADimension.MONTH_PANEL_X_INSET),
									 ADimension.INFO_LABEL_HEIGHT);
			
			for (DayOfWeek day : DayOfWeek.values()) {
				JLabel dayName = new JLabel(day.name().substring(0, 3) + "");
				dayName.setHorizontalAlignment(SwingConstants.CENTER);
				dayName.setFont(AFont.bold11());
				daysNamesLabel.add(dayName);
			}
		}
		return daysNamesLabel;
	}
	
	private JPanel getDaysPanel() {
		if (daysPanel == null) {
			GridLayout gridLayout = new GridLayout(6, 7, 3, 3);
			
			daysPanel = new JPanel(gridLayout);
			daysPanel.setBounds(ADimension.CALENDAR_SPACING,
								ADimension.INFO_LABEL_HEIGHT + (2 * ADimension.CALENDAR_SPACING), 
								ADimension.CALENDAR_WIDTH - (2 * ADimension.MONTH_PANEL_X_INSET) - (2 * ADimension.CALENDAR_SPACING),
								200);

			// Get the maximum number of days within the current month.
			int daysInMonth = Month.of(currentMonth.getValue()).maxLength();
			
			// Check if it's a leap year.
			if (!((currentYear % 400 == 0 || currentYear % 4 == 0) && currentYear % 100 != 0)) {
				if (currentMonth == Month.FEBRUARY) {
					daysInMonth = (Month.of(currentMonth.getValue()).maxLength()) - 1;
				}
			}
			
			// Get day of week integer value (MONDAY = 1 ... SUN = 7) for 1st of the current month.		  
			int firstDayOfWeekValue = DayOfWeek.from(LocalDate.of(currentYear, currentMonth, 1)).getValue();
			
			// Get day of week integer value for last day of month.
			int lastDayOfWeekValue = DayOfWeek.from(LocalDate.of(currentYear, currentMonth, daysInMonth)).getValue();

			// Print initial space if date 1 isn't MONDAY.
			for (int i = 1; i < firstDayOfWeekValue; i++) {
				daysPanel.add(new JLabel());
			}
			
			// Print the days of the month starting from 1st.
			int day;
			for (day = 1; day <= daysInMonth; day++) {
				
				DateButton dateButton = new DateButton((byte)day);
				checkAvailabaleDate(dateButton);
				getDateButtonsList().add(dateButton);
				
				if (dateButton.isEnabled()) {
					dateButton.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent event) {

							for (DateButton button : getDateButtonsList()) {
								button.setSelected(false);
								selectedMonth = "";
								selectedDate = 0;
							}
							dateButton.setSelected(true);
							selectedMonth = currentMonth.toString();
							selectedDate = Byte.parseByte(dateButton.getText());
						}
					});
				}
				daysPanel.add(dateButton);
			}
			
			// Set the number grid layout rows to display the buttons at the same size.
			if (firstDayOfWeekValue < 6) {
				gridLayout.setRows(5);
				daysPanel.setBounds(ADimension.CALENDAR_SPACING,
									ADimension.INFO_LABEL_HEIGHT + (2 * ADimension.CALENDAR_SPACING), 
									ADimension.CALENDAR_WIDTH - (2 * ADimension.MONTH_PANEL_X_INSET) - (2 * ADimension.CALENDAR_SPACING),
									166);
			}
			else if (firstDayOfWeekValue == 1 && lastDayOfWeekValue == 7) {
				gridLayout.setRows(4);
				daysPanel.setBounds(ADimension.CALENDAR_SPACING,
									ADimension.INFO_LABEL_HEIGHT + (2 * ADimension.CALENDAR_SPACING), 
									ADimension.CALENDAR_WIDTH - (2 * ADimension.MONTH_PANEL_X_INSET) - (2 * ADimension.CALENDAR_SPACING),
									132);
			}
			else if (firstDayOfWeekValue >= 6 && lastDayOfWeekValue >= 5) {
				gridLayout.setRows(5);
				daysPanel.setBounds(ADimension.CALENDAR_SPACING,
									ADimension.INFO_LABEL_HEIGHT + (2 * ADimension.CALENDAR_SPACING), 
									ADimension.CALENDAR_WIDTH - (2 * ADimension.MONTH_PANEL_X_INSET) - (2 * ADimension.CALENDAR_SPACING),
									166);
			}
			
			// Print free spaces if last day of month isn't Sunday to fill the grid row.
			for (int i = lastDayOfWeekValue; i < 7; i++) {
				daysPanel.add(new JLabel());
			}
		}
		return daysPanel;
	}
	
	private void checkAvailabaleDate(DateButton dateButton) {
		
		int dateButtonInt = Integer.parseInt(dateButton.getText());
		DayOfWeek dayOfWeek = DayOfWeek.from(LocalDate.of(currentYear, currentMonth, dateButtonInt));
	
		// Disable weekend days buttons.
		if (currentMonth.getValue() == LocalDate.now().getMonthValue() &&
				(dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY) &&
			dateButtonInt > LocalDate.now().getDayOfMonth()) {
			dateButton.setEnabled(false);
			dateButton.setToolTipText(AMessages.WEEKEND_DAYS);	
		} 
		else if (currentMonth.getValue() > LocalDate.now().getMonthValue() &&
				(dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)) {
			dateButton.setEnabled(false);
			dateButton.setToolTipText(AMessages.WEEKEND_DAYS);
		}
		// Disable past days buttons.
		else if (currentMonth.getValue() == LocalDate.now().getMonthValue() && 
				 dateButtonInt < LocalDate.now().getDayOfMonth()) {
			dateButton.setEnabled(false);
			dateButton.setToolTipText(AMessages.PAST_DAYS);
		}
		// Disable datesList from past months.
		else if (currentMonth.getValue() < LocalDate.now().getMonthValue()) {
			dateButton.setEnabled(false);
			dateButton.setToolTipText(AMessages.PAST_DAYS);
		} 
	}
	
	private void updateMonthPanel() {
		remove(getMonthPanel());
		
		// Set monthPanel and daysPanel to null so they can update their current month.
		monthPanel = null;
		daysPanel = null;
		
		add(getMonthPanel());
		repaint();
	}
}
