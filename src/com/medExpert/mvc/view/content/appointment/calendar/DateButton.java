package com.medExpert.mvc.view.content.appointment.calendar;

import java.time.LocalDate;

import javax.swing.JButton;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.AFont;

public class DateButton extends JButton {
	
	private byte date;
	
	public DateButton(byte date) {
		this.date = date;
		configure();
	}

	private void configure() {
		setText(String.valueOf(date));
		setFont(AFont.bold12());
		
		highlightCurrentDate();
	}

	private void highlightCurrentDate() {
		if (date == LocalDate.now().getDayOfMonth()) {
			setForeground(AColor.darck());
		}
	}
}
