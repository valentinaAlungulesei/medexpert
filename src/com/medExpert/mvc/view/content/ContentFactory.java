package com.medExpert.mvc.view.content;

import com.medExpert.mvc.view.content.appointment.AppointmentPanel;
import com.medExpert.mvc.view.menu.CONTENT_ID;

/**
 * Sigleton Design Pattern is implemented.
 * 
 * @author valentinaAlungulesei
 */
public class ContentFactory implements IContentFactory {
	
	private static ContentFactory instance;
	
	private ContentFactory() {}
	
	public static ContentFactory getInstance() {
		synchronized (ContentFactory.class) {
			if (instance == null) {
				instance = new ContentFactory();
			}
		}
		return instance;
	}

	@Override
	public AContentPanel createContent(CONTENT_ID content_ID) {
		
		switch (content_ID) {
			case CLIENT_LOG_IN : return new LoginPanel();
			case LOG_OUT : return new LogoutPanel();
			case NEW_CLIENT : return new NewClientPanel();
			case NEW_EMPLOYEE : return new NewEmployeePanel();
			case TESTS : return new TestListPanel();
			case PRICES : return new PricesListPanel();
			case ENTER_RESULTS : return new EnterResultsPanel();
			case VIEW_RESULTS : return new ViewResultsPanel();
			case APPOINTMENT : return new AppointmentPanel();
			case INFO : return new GoodToKnowPanel();

			default : break;
		}
		return null;
	}
}
