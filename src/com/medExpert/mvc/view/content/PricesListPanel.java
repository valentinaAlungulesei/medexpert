package com.medExpert.mvc.view.content;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.MyTable;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.APath;
import com.medExpert.util.TestsListStore;

public class PricesListPanel extends AContentPanel {

	public PricesListPanel() {
		super(CONTENT_ID.PRICES, APath.PRICES_PNG);
	}

	@Override
	public void buildContent() {
		// Prepare columns and data for the table.
		String[] columnNames = { "   Test", "   Price" };
		String[][] data = getTableData();

		// Create the table.
		MyTable table = new MyTable(data, columnNames);
		table.setDefaultEditor(Object.class, null);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(0, 115, ADimension.CONTENT_WIDTH, ADimension.SCREEN_HEIGHT - 115 - ADimension.BOTTOM_INSET);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));

		this.add(scrollPane);

	}

	private String[][] getTableData() {

		// Prepare tests array for data.
		String[] names = new String[TestsListStore.getInstance().getTestPricesMap().keySet().size()];
		TestsListStore.getInstance().getTestPricesMap().keySet().toArray(names);

		// Prepare prices array for data.
		Double[] prices = new Double[TestsListStore.getInstance().getTestPricesMap().values().size()];
		TestsListStore.getInstance().getTestPricesMap().values().toArray(prices);

		// Add tests and prices to data matrix
		String[][] data = new String[names.length][prices.length];
		for (int testIndex = 0; testIndex < names.length; testIndex++) {
			data[testIndex][0] = "   " + names[testIndex];
			for (int priceIndex = 0; priceIndex < prices.length; priceIndex++) {
				data[priceIndex][1] = "   " + prices[priceIndex];
			}
		}
		return data;
	}
}
