package com.medExpert.mvc.view.content;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.APath;
import com.medExpert.util.TestsListStore;

public class TestListPanel extends AContentPanel {

	public TestListPanel() {
		super(CONTENT_ID.TESTS, APath.TESTS_PNG);
	}

	@Override
	public void buildContent() {
		
		DefaultListModel<String> testsListModel = new DefaultListModel<String>();
		for (Test test : TestsListStore.getInstance().getTestList()) {
			testsListModel.addElement("  ✓  " +  test.getName());
		}
		
		JList<String> testList = new JList<String>(testsListModel);
		testList.setFont(AFont.plain15());
		testList.setFixedCellHeight(40);
		testList.setSelectedValue(null, true);
		testList.setOpaque(true);
		testList.setSelectionBackground(AColor.darck());
	
		JScrollPane testsScrollPane = new JScrollPane(testList);
		testsScrollPane.setBounds(0, ADimension.FIELD_Y,
				ADimension.CONTENT_WIDTH, ADimension.SCREEN_HEIGHT - ADimension.FIELD_Y - ADimension.BOTTOM_INSET);
		testsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		testsScrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
	
		this.add(testsScrollPane);
	}
}
