package com.medExpert.mvc.view.content;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;

public class PasswordComplexityInfoLabel extends JLabel {
	
	public PasswordComplexityInfoLabel() {
		build();
	}


	private void build() {
		setLayout( null);
		
		JTextArea passwordInfoText = new JTextArea();
		passwordInfoText.setText("The password shoud have minimum 5 characters,"
							   + " at           least one number, one lower case letter, one upper"
							   + "                       case letter and one special character.");
		passwordInfoText.setFont(AFont.italic12());
		passwordInfoText.setForeground(Color.white);
		passwordInfoText.setEditable(false);
		passwordInfoText.setHighlighter(null);
		passwordInfoText.setWrapStyleWord(true);
		passwordInfoText.setLineWrap(true);
		passwordInfoText.setOpaque(false);
		passwordInfoText.setBounds(ADimension.FIELD_WIDTH / 16, 0,
								   ADimension.FIELD_WIDTH , ADimension.FIELD_HEIGHT);
		
		JLabel digitsLabel = buildTextLabel("0 - 9");
		digitsLabel.setBounds(ADimension.FIELD_WIDTH / 4, 45,
							  ADimension.FIELD_WIDTH / 2, ADimension.LABEL_HEIGHT);
		
		JLabel lowerCaseLabel = buildTextLabel("a - z");
		lowerCaseLabel.setBounds(ADimension.FIELD_WIDTH / 4, 70,
								 ADimension.FIELD_WIDTH / 2, ADimension.LABEL_HEIGHT);
		
		JLabel upperCaseLabel = buildTextLabel("A - Z");
		upperCaseLabel.setBounds(ADimension.FIELD_WIDTH / 4, 95,
								 ADimension.FIELD_WIDTH / 2, ADimension.LABEL_HEIGHT);
		
		JLabel specialCharLabel = buildTextLabel("@ # $ % ^ & + =");
		specialCharLabel.setBounds(ADimension.FIELD_WIDTH / 4, 120,
								   ADimension.FIELD_WIDTH / 2, ADimension.LABEL_HEIGHT);
		
		add(passwordInfoText);
		add(digitsLabel);
		add(lowerCaseLabel);
		add(upperCaseLabel);
		add(specialCharLabel);
	}
	
	private JLabel buildTextLabel(String text) {
		JLabel label = new JLabel(text);
		label.setFont(AFont.italic12());
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.white);
		label.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
		return label;
	}
}
