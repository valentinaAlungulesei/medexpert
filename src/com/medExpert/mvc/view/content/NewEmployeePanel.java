package com.medExpert.mvc.view.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.MessageLabel;
import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.MyComboBox;
import com.medExpert.mvc.view.MyPasswordField;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.content.appointment.InfoLabel;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;
import com.medExpert.util.DESIGNATION;
import com.medExpert.util.SALARY_RANGE;
import com.medExpert.util.Util;

public class NewEmployeePanel extends AContentPanel {
	
	private MyTextField usernameField;
	private JLabel usernameWarningLabel;
	private MyPasswordField passwordField;
	private JLabel passwordWarningLabel;
	private PasswordComplexityInfoLabel passwordComplexityLabel;
	private MyPasswordField confirmPasswordField;
	private JLabel confirmPasswordWarningLabel;
	private MyTextField firstNameField;
	private JLabel firstNameWarningLabel;
	private MyTextField lastNameField;
	private JLabel lastNameWarningLabel;
	private InfoLabel designationInFoInfoLabel;
	private MyComboBox designationComboBox;
	private JLabel designationWarningLbel;
	private InfoLabel salaryInfoLabel;
	private MyComboBox salaryComboBox;
	private JLabel salaryWarningLabel;
	private MyButton createAccountButton;
	private MessageLabel successfullMessageLabel;
	
	private List<IButtonListener> listeners;

	public NewEmployeePanel() {
		super(CONTENT_ID.NEW_CLIENT, APath.ACCOUNT_PNG);
		listeners = new ArrayList<IButtonListener>();
	}

	@Override
	public void buildContent() {
		this.add(getUsernameField());
		this.add(getFirstNameField());
		this.add(getLastNameField());
		this.add(getPasswordComplexityLabel());
		this.add(getPasswordField());
		this.add(getConfirmPasswordField());
		this.add(getDesignationInFoInfoLabel());
		this.add(getDesignationComboBox());
		this.add(getSalaryComboBox());
		this.add(getSalaryInfoLabel());
		this.add(getCreateAccountButton());
	}
	
	public void addNewEmployeeButtonLister(IButtonListener listener) {
		this.listeners.add(listener);
	}
	
	public MyTextField getUsernameField() {
		if (usernameField == null) {
			usernameField = new MyTextField("Username");
			usernameField.setToolTipText("firstname-lastname");
			usernameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y,
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return usernameField;
	}
	
	public MyPasswordField getPasswordField() {
		if (passwordField == null) {
			passwordField = new MyPasswordField("Password");
			passwordField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (3 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
									ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return passwordField;
	}
	
	public MyPasswordField getConfirmPasswordField() {
		if (confirmPasswordField == null) {
			confirmPasswordField = new MyPasswordField("Confirm password");
			confirmPasswordField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (4 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
										   ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return confirmPasswordField;
	}
	
	public MyTextField getFirstNameField() {
		if (firstNameField == null) {
			firstNameField = new MyTextField("First name");
			firstNameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (5 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
									 ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return firstNameField;
	}
	
	public MyTextField getLastNameField() {
		if (lastNameField == null) {
			lastNameField = new MyTextField("Last name");
			lastNameField.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + (6 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
								    ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
		}
		return lastNameField;
	}
	
	public MyComboBox getDesignationComboBox() {
		if (designationComboBox == null) {
			
			String[] items = new String[DESIGNATION.values().length + 1];
			items[0] = "";
			
			for (int index = 1; index < DESIGNATION.values().length + 1; index++) {
				items[index] = DESIGNATION.values()[index - 1].getName();
			}
			
			designationComboBox = new MyComboBox(items);
			designationComboBox.setSelectedIndex(0);
			designationComboBox.setBounds(ADimension.FIELD_X,
										  ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING),
				    					  (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
				    					  ADimension.ROW_HEIGHT);
		}
		return designationComboBox;
	}
	
	public MyComboBox getSalaryComboBox() {
		if (salaryComboBox == null) {
			String[] items = new String[SALARY_RANGE.values().length + 1];
			items[0] = "";
			
			for (int index = 1; index < SALARY_RANGE.values().length + 1; index++) {
				items[index] = SALARY_RANGE.values()[index - 1].getSalary();
			}
			
			salaryComboBox = new MyComboBox(items);
			salaryComboBox.setSelectedIndex(0);
			salaryComboBox.setBounds(ADimension.FIELD_X + (ADimension.FIELD_WIDTH / 2) + (ADimension.FIELD_WIDTH / 10),
									 ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING),
				    				 (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
				    				 ADimension.ROW_HEIGHT);
		}
		return salaryComboBox;
	}
	
	public MyButton getCreateAccountButton() {
		if (createAccountButton == null) {
			createAccountButton = new MyButton("Create account");
			createAccountButton.setBounds(ADimension.FIELD_X + (ADimension.BUTTON_WIDTH / 2),
										  ADimension.FIELD_Y + (8 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
										  ADimension.BUTTON_WIDTH,
										  ADimension.BUTTON_HEIGHT);
			createAccountButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IButtonListener listener : listeners) {
						listener.buttonWasClicked();
					}
					repaint();
				}
			});
		}
		return createAccountButton;
	}
	
	public void setEmptyUsernameView() {
		this.add(getUsernameWarningLabel());
		getUsernameField().setWarning(AMessages.EMPTY_FIELD, getUsernameWarningLabel());
	}
	
	public void setInvalidUsernameView() {
		this.add(getUsernameWarningLabel());
		getUsernameField().setWarning("Username pattern: \"firstname-lastname\".", getUsernameWarningLabel());
	}
	
	public void setTakenUsernameView() {
		this.add(getUsernameWarningLabel());
		getUsernameField().setWarning("An account with this username alredy exists.", getUsernameWarningLabel());
	}
	
	public void setValidUsernameView() {
		this.remove(getUsernameWarningLabel());
	}
	
	public void setEmptyPasswordView() {
		this.add(getPasswordWarningLabel());
		getPasswordField().setWarning(AMessages.EMPTY_FIELD, getPasswordWarningLabel());
	}
	
	public void setInvalidPasswordView() {
		this.add(getPasswordWarningLabel());
		getPasswordField().setWarning(AMessages.NOT_STRONG_PASSWORD, getPasswordWarningLabel());
	}
	
	public void setValidPasswordView() {
		this.remove(getPasswordWarningLabel());
	}
	
	public void setEmptyConfirmPasswordView() {
		this.add(getConfirmPasswordWarningLabel());
		getConfirmPasswordField().setWarning(AMessages.EMPTY_FIELD, getConfirmPasswordWarningLabel());
	}
	
	public void setDifferentConfirmPassword() {
		this.add(getConfirmPasswordWarningLabel());
		getConfirmPasswordField().setWarning(AMessages.DIFFERENT_PASSWORD, getConfirmPasswordWarningLabel());
	}
	
	public void setValidConfirmPasswordView() {
		this.remove(getConfirmPasswordWarningLabel());
	}
	
	public void setEmptyFirstNameView() {
		this.add(getFirstNameWarningLabel());
		getFirstNameField().setWarning(AMessages.EMPTY_FIELD, getFirstNameWarningLabel());
	}
	
	public void setInvalidFisrtNameView() {
		this.add(getFirstNameWarningLabel());
		getFirstNameField().setWarning(AMessages.INVALID_FIRST_NAME, getFirstNameWarningLabel());
	}
	
	public void setValidFirstNameView() {
		this.remove(getFirstNameWarningLabel());
		getFirstNameField().setText(Util.getInstance().capitalizeAllFirstLetters(getFirstNameField().getText()));
	}
	
	public void setEmptyLastNameView() {
		this.add(getLastNameWarningLabel());
		getLastNameField().setWarning(AMessages.EMPTY_FIELD, getLastNameWarningLabel());
	}
	
	public void setInvalidLastNameView() {
		this.add(getLastNameWarningLabel());
		getLastNameField().setWarning(AMessages.INVALID_LAST_NAME, getLastNameWarningLabel());
	}
	
	public void setValidLastName() {
		this.remove(getLastNameWarningLabel());
		getLastNameField().setText(Util.getInstance().capitalizeAllFirstLetters(getLastNameField().getText()));
	}
	
	public void setEmptyDesignationView() {
		this.add(getDesignationWarningLbel());
		getDesignationComboBox().setWarning(AMessages.COMBO_BOX_SELECTION, getDesignationWarningLbel());
	}
	
	public void voidsetEmptySalaryView() {
		this.add(getSalaryWarningLabel());
		getSalaryComboBox().setWarning(AMessages.COMBO_BOX_SELECTION, getSalaryWarningLabel());
	}
	
	public void setValidInputView() {
		this.remove(getUsernameField());
		this.remove(getPasswordComplexityLabel());
		this.remove(getPasswordField());
		this.remove(getConfirmPasswordField());
		this.remove(getFirstNameField());
		this.remove(getLastNameField());
		this.remove(getDesignationInFoInfoLabel());
		this.remove(getDesignationComboBox());
		this.remove(getSalaryInfoLabel());
		this.remove(getSalaryComboBox());
		this.remove(getCreateAccountButton());
		
		this.add(getSuccessfulMessageLabel());
		this.add(getLoginRequestMessageLabel());
		this.repaint();
	}
	
	private JLabel getUsernameWarningLabel() {
		if (usernameWarningLabel == null) {
			usernameWarningLabel = new JLabel();
			usernameWarningLabel.setBounds(ADimension.FIELD_X, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
										   ADimension.FIELD_WIDTH, ADimension.INFO_LABEL_HEIGHT);
		}
		return usernameWarningLabel;
	}
	
	private PasswordComplexityInfoLabel getPasswordComplexityLabel() {
		if (passwordComplexityLabel == null) {
			passwordComplexityLabel = new PasswordComplexityInfoLabel();
			passwordComplexityLabel.setBounds(ADimension.FIELD_X,
											  ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + ADimension.LABEL_HEIGHT,
											  ADimension.FIELD_WIDTH,
											  3 * ADimension.FIELD_HEIGHT);
		}
		return passwordComplexityLabel;
	}
	
	private JLabel getPasswordWarningLabel() {
		if (passwordWarningLabel == null) {
			passwordWarningLabel = new JLabel();
			passwordWarningLabel.setBounds(ADimension.FIELD_X,
										   ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (3 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
					   					   ADimension.FIELD_WIDTH,
					   					   ADimension.INFO_LABEL_HEIGHT);
		}
		return passwordWarningLabel;
	}
	
	private JLabel getConfirmPasswordWarningLabel() {
		if (confirmPasswordWarningLabel == null) {
			confirmPasswordWarningLabel = new JLabel();
			confirmPasswordWarningLabel.setBounds(ADimension.FIELD_X,
												  ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (4 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
					   							  ADimension.FIELD_WIDTH,
					   							  ADimension.INFO_LABEL_HEIGHT);
		}
		return confirmPasswordWarningLabel;
	}
	
	private JLabel getFirstNameWarningLabel() {
		if (firstNameWarningLabel == null) {
			firstNameWarningLabel = new JLabel();
			firstNameWarningLabel.setBounds(ADimension.FIELD_X,
											ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (5 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
											ADimension.FIELD_WIDTH,
											ADimension.INFO_LABEL_HEIGHT);
		}
		return firstNameWarningLabel;
	}
	
	private JLabel getLastNameWarningLabel() {
		if (lastNameWarningLabel == null) {
			lastNameWarningLabel = new JLabel();
			lastNameWarningLabel.setBounds(ADimension.FIELD_X,
					  					   ADimension.FIELD_Y + ADimension.FIELD_HEIGHT + (6 * ADimension.FIELD_Y_SPACING) - ADimension.LABEL_HEIGHT,
					  					   ADimension.FIELD_WIDTH,
					  					   ADimension.INFO_LABEL_HEIGHT);
		}
		return lastNameWarningLabel;
	}
	
	private InfoLabel getDesignationInFoInfoLabel() {
		if (designationInFoInfoLabel == null) {
			designationInFoInfoLabel = new InfoLabel("Designation *");
			designationInFoInfoLabel.setBounds(ADimension.FIELD_X,
											   ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING) - ADimension.INFO_LABEL_HEIGHT,
											   (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
											   ADimension.INFO_LABEL_HEIGHT);
		}
		return designationInFoInfoLabel;
	}
	
	private JLabel getDesignationWarningLbel() {
		if (designationWarningLbel == null) {
			designationWarningLbel = new JLabel();
			designationWarningLbel.setBounds(ADimension.FIELD_X,
					   						 ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING) + ADimension.ROW_HEIGHT,
					   						 (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
					   						 ADimension.INFO_LABEL_HEIGHT);
		}
		return designationWarningLbel;
	}
	
	private InfoLabel getSalaryInfoLabel() {
		if (salaryInfoLabel == null) {
			salaryInfoLabel = new InfoLabel("Salary range *");
			salaryInfoLabel.setBounds(ADimension.FIELD_X + (ADimension.FIELD_WIDTH / 2) + (ADimension.FIELD_WIDTH / 10),
					 				  ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING) - ADimension.INFO_LABEL_HEIGHT,
					 				  (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
					 				  ADimension.INFO_LABEL_HEIGHT);
		}
		return salaryInfoLabel;
	}
	
	private JLabel getSalaryWarningLabel() {
		if (salaryWarningLabel == null) {
			salaryWarningLabel = new JLabel();
			salaryWarningLabel.setBounds(ADimension.FIELD_X + (ADimension.FIELD_WIDTH / 2) + (ADimension.FIELD_WIDTH / 10),
	 				  					 ADimension.FIELD_Y + (7 * ADimension.FIELD_Y_SPACING) + ADimension.ROW_HEIGHT,
	 				  					 (ADimension.FIELD_WIDTH / 2) - (ADimension.FIELD_WIDTH / 10),
	 				  					 ADimension.INFO_LABEL_HEIGHT);
		}
		return salaryWarningLabel;
	}
	
	private MessageLabel getSuccessfulMessageLabel() {
		MessageLabel messageLabel = new MessageLabel("Your account was succesfully created.");
		messageLabel.setBounds(0, ADimension.FIELD_Y,
				               ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		return messageLabel;
	}
	
	private MessageLabel getLoginRequestMessageLabel() {
		MessageLabel messageLabel = new MessageLabel("Go to login screen.");
		messageLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
				               ADimension.CONTENT_WIDTH, ADimension.LABEL_HEIGHT);
		return messageLabel;
	}
}
