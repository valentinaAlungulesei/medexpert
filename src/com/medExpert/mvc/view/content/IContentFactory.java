package com.medExpert.mvc.view.content;

import com.medExpert.mvc.view.menu.CONTENT_ID;

public interface IContentFactory {
	
	/**
	 * @param content_ID
	 * @return AContentPanel 
	 */
	public AContentPanel createContent(CONTENT_ID content_ID);

}
