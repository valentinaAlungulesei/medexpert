package com.medExpert.mvc.view.content;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Test;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.MyButton;
import com.medExpert.mvc.view.MyTable;
import com.medExpert.mvc.view.MyTextField;
import com.medExpert.mvc.view.content.appointment.InfoLabel;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.util.AMessages;
import com.medExpert.util.APath;

public class EnterResultsPanel extends AContentPanel {
	
	private MyTextField clientPersonalIdField;
	private MyButton searchAppointmentsButton;
	private JLabel clientPersonalIdWarningLabel;
	private InfoLabel appointmentInfoLabel;
	private JScrollPane appointmentsScrollPane;
	private List<JCheckBox> appointmentCheckBoxes;
	private InfoLabel testsInfoLabel;
	private MyTable testsTable;
	private JLabel resultsWarningLabel;
	private JScrollPane testListScrollPane;
	private MyButton submitButton;

	private Appointment selectedAppointment;
	private List<IButtonListener> searchTestsButtonListeners;
	private List<IAppointmentCheckBoxListener> appointmentsCheckBoxListeners;
	private List<IButtonListener> submitButtonListeners;

	public EnterResultsPanel() {
		super(CONTENT_ID.ENTER_RESULTS, APath.RESULTS_PNG);
		searchTestsButtonListeners = new ArrayList<IButtonListener>();
		appointmentsCheckBoxListeners = new ArrayList<IAppointmentCheckBoxListener>();
		submitButtonListeners = new ArrayList<IButtonListener>();
	}

	@Override
	public void buildContent() {
		this.add(getClientPersonalIdField());
		this.add(getSearchAppointmentsButton());
	}
	
	public void addSearchAppointmentsButtonListeners(IButtonListener listener) {
		this.searchTestsButtonListeners.add(listener);
	}
	
	public void addAppointmentsCheckBoxListener(IAppointmentCheckBoxListener listener) {
		this.appointmentsCheckBoxListeners.add(listener);
	}
	
	public void addSubmitResultsListener(IButtonListener listener) {
		this.submitButtonListeners.add(listener);
	}
	
	public void setEmptyClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.EMPTY_FIELD, getClientPersonalIdWarningLabel());
	}
	
	public void setUnknownClientPersonalIdView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning("A client with this ID is not registered.", getClientPersonalIdWarningLabel());
	}
	
	public void setDigitsLimitPersonalIdInputView() {
		this.add(getClientPersonalIdWarningLabel());
		getClientPersonalIdField().setWarning(AMessages.ID_SIZE, getClientPersonalIdWarningLabel());
	}

	public void setSearchAppointmentsView(List<Appointment> appointmentsList) {
		this.remove(getClientPersonalIdWarningLabel());
		this.add(getAppontmentInfoLabel());
		this.add(getAppointmentsScrollPane(appointmentsList));
	}
	
	public void setTestsListView(List<Test> testsList) {
		if (testListScrollPane != null) {
			this.remove(getTestsListScrollPane());
		}
		this.testListScrollPane = null;
		this.add(getTestsInfoLabel());
		this.add(getTestsListScrollPane(testsList));
		this.add(getSubmitButton());
	}
	
	public void setUnselectedAppointmentView() {
		this.remove(getTestsInfoLabel());
		this.remove(getTestsListScrollPane());
		this.remove(getSubmitButton());
	}
	
	public void setEmptyResultView() {
		this.add(getResultsWarningLabel());
		getTestsScrollPane().setBorder(BorderFactory.createLineBorder(Color.red, 1));
	}
	
	public void setValidResultsView() {
		this.remove(getResultsWarningLabel());
		this.remove(getTestsListScrollPane());
		this.remove(getSubmitButton());
	}
	
	public MyTextField getClientPersonalIdField() {
		if (clientPersonalIdField == null) {
			clientPersonalIdField = new MyTextField("Enter client's personal ID");
			clientPersonalIdField.setBounds(0, ADimension.FIELD_Y, ADimension.FIELD_WIDTH, ADimension.FIELD_HEIGHT);
			clientPersonalIdField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (clientPersonalIdField.getText().length() >= 13) {
						clientPersonalIdField.setToolTipText(AMessages.LIMIT_13_TOOLTIP);
						e.consume();
					}
				}
			});
		}
		return clientPersonalIdField;
	}
	
	public MyButton getSearchAppointmentsButton() {
		if (searchAppointmentsButton == null) {
			searchAppointmentsButton = new MyButton("Search appointments");
			searchAppointmentsButton.setBounds(ADimension.FIELD_WIDTH + (ADimension.CONTENT_WIDTH - ADimension.FIELD_WIDTH - ADimension.BUTTON_WIDTH),
										ADimension.FIELD_Y + ((ADimension.FIELD_HEIGHT - ADimension.BUTTON_HEIGHT) / 2),
										ADimension.BUTTON_WIDTH,
										ADimension.BUTTON_HEIGHT);
			searchAppointmentsButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IButtonListener listener : searchTestsButtonListeners) {
						listener.buttonWasClicked();
					}
					repaint();
				}
			});
		}
		return searchAppointmentsButton;
	}
	
	public JScrollPane getAppointmentsScrollPane(List<Appointment> appointmentList) {
		if (appointmentsScrollPane == null) {
		
			JPanel panel = new JPanel(new GridLayout(appointmentList.size(), 1));
			panel.setBackground(Color.white);

			appointmentCheckBoxes = new ArrayList<JCheckBox>();
			for (Appointment appointment : appointmentList) {
				JCheckBox checkBox = buildCheckBox(appointment);
				appointmentCheckBoxes.add(checkBox);
				panel.add(checkBox);
			}
	
			appointmentsScrollPane = new JScrollPane(panel);
			appointmentsScrollPane.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING + ADimension.INFO_LABEL_HEIGHT,
											 ADimension.CONTENT_WIDTH, 2 * ADimension.ROW_HEIGHT);
			appointmentsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			appointmentsScrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
		}
		return appointmentsScrollPane;
	}
	
	private JCheckBox buildCheckBox(Appointment appointment) {
		JCheckBox checkBox = new JCheckBox();
		checkBox.setText(appointment.getDate() + " " + appointment.getMonth() + " at " + appointment.getTime());
		checkBox.setFont(AFont.plain15());
		checkBox.setPreferredSize(new Dimension(ADimension.CONTENT_WIDTH - 20, ADimension.ROW_HEIGHT));
		checkBox.addMouseListener(new MouseAdapter() {
				
			@Override
			public void mouseReleased(MouseEvent e) {
				if (checkBox.isSelected()) {
					
					for (JCheckBox checkBox : appointmentCheckBoxes) {
						checkBox.setSelected(false);
					}
					
					checkBox.setSelected(true);
					selectedAppointment = appointment;
					
					for (IAppointmentCheckBoxListener listener : appointmentsCheckBoxListeners) {
						listener.checkBoxIsSelected();
					}
				} 
				else if (!checkBox.isSelected()) {
					for (IAppointmentCheckBoxListener listener : appointmentsCheckBoxListeners) {
						listener.checkBoxIsDeselected();
					}
				}
				EnterResultsPanel.this.repaint();
			}
		});
		return checkBox;
	}
	
	public Appointment getSelectedAppointment() {
		return selectedAppointment;
	}
	
	public JScrollPane getTestsListScrollPane(List<Test> testsList) {
		if (testListScrollPane == null) {

			String[] colNames = { "   Test", "Result", "Unit"};

			String[] testNames = new String[testsList.size()];
			for (int index = 0; index < testsList.size(); index++) {
				testNames[index] = testsList.get(index).getName();
			}

			Double[] results = new Double[testsList.size()];
			for (int index = 0; index < testsList.size(); index++) {
				results[index] = testsList.get(index).getResult();
			}
			
			String[] units = new String[testsList.size()];
			for (int index = 0; index < testsList.size(); index++) {
				units[index] = testsList.get(index).getUnit();
			}

			// Populate data matrix.
			String[][] data = new String[testsList.size()][colNames.length];
			for (int index = 0; index < testNames.length; index++) {
				data[index][0] = "   " + testNames[index];
				data[index][1] = String.valueOf(results[index]);
				data[index][2] = units[index];
			}

			testsTable = new MyTable(data, colNames);
			testsTable.getColumnModel().getColumn(2).setCellRenderer(testsTable.getMyCellRenderer());

			testListScrollPane = new JScrollPane(testsTable);
			testListScrollPane.setBounds(0,
					ADimension.FIELD_Y + (2 * ADimension.FIELD_Y_SPACING) + ADimension.ROW_HEIGHT + ADimension.INFO_LABEL_HEIGHT,
					ADimension.CONTENT_WIDTH, (testsList.size() + 2) * ADimension.ROW_HEIGHT);
			testListScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			testListScrollPane.setBorder(BorderFactory.createLineBorder(AColor.darck(), 1));
		}
		return testListScrollPane;
	}
	
	public JScrollPane getTestsListScrollPane() {
		return this.testListScrollPane;
	}
	
	public MyTable getTestsTable() {
		return testsTable;
	}
	
	private InfoLabel getAppontmentInfoLabel() {
		if (appointmentInfoLabel == null) {
			appointmentInfoLabel = new InfoLabel("Select an appointment");
			appointmentInfoLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_Y_SPACING,
										   ADimension.CONTENT_WIDTH + 10, ADimension.INFO_LABEL_HEIGHT);
		}
		return appointmentInfoLabel;
	}
	
	private InfoLabel getTestsInfoLabel() {
		if (testsInfoLabel == null) {
			testsInfoLabel = new InfoLabel("Double click on result value to uptade it");
			testsInfoLabel.setBounds(0, ADimension.FIELD_Y + (2 * ADimension.FIELD_Y_SPACING) + ADimension.ROW_HEIGHT,
					   				 ADimension.CONTENT_WIDTH + 10, ADimension.INFO_LABEL_HEIGHT);
		}
		return testsInfoLabel;
	}
	
	private JScrollPane getTestsScrollPane() {
		return this.testListScrollPane;
	}
	
	private JLabel getResultsWarningLabel() {
		if (resultsWarningLabel == null) {
			resultsWarningLabel = new JLabel("All results must be completed.");
			resultsWarningLabel.setFont(AFont.bold13());
			resultsWarningLabel.setHorizontalAlignment(SwingConstants.CENTER);
			resultsWarningLabel.setForeground(Color.red);
			resultsWarningLabel.setBounds(0,
										  ADimension.BUTTON_Y - ADimension.INFO_LABEL_HEIGHT,
										  ADimension.CONTENT_WIDTH,
										  ADimension.INFO_LABEL_HEIGHT);
		}
		return resultsWarningLabel;
	}
	
	private MyButton getSubmitButton() {
		if (submitButton == null) {
			submitButton = new MyButton("Submit");
			submitButton.setBounds(ADimension.BUTTON_X, ADimension.BUTTON_Y, ADimension.BUTTON_WIDTH, ADimension.BUTTON_HEIGHT);
			submitButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					for (IButtonListener listener : submitButtonListeners) {
						listener.buttonWasClicked();
					}
					repaint();
				}
			});
		}
		return submitButton;
	}
	
	private JLabel getClientPersonalIdWarningLabel() {
		if (clientPersonalIdWarningLabel == null) {
			clientPersonalIdWarningLabel = new JLabel();
			clientPersonalIdWarningLabel.setBounds(0, ADimension.FIELD_Y + ADimension.FIELD_HEIGHT,
												   ADimension.FIELD_WIDTH,ADimension.INFO_LABEL_HEIGHT);
		}
		return clientPersonalIdWarningLabel;
	}
}
