package com.medExpert.mvc.view.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.medExpert.mvc.controller.AppointmentController;
import com.medExpert.mvc.controller.ClientPageController;
import com.medExpert.mvc.controller.EnterResultsController;
import com.medExpert.mvc.controller.NewClientController;
import com.medExpert.mvc.controller.StaffPageController;
import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.controller.ViewResultsController;
import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.content.AContentPanel;
import com.medExpert.mvc.view.content.ContentFactory;
import com.medExpert.mvc.view.content.EnterResultsPanel;
import com.medExpert.mvc.view.content.IButtonListener;
import com.medExpert.mvc.view.content.IAppointmentCheckBoxListener;
import com.medExpert.mvc.view.content.NewClientPanel;
import com.medExpert.mvc.view.content.ViewResultsPanel;
import com.medExpert.mvc.view.content.appointment.AppointmentPanel;
import com.medExpert.mvc.view.content.appointment.IScheduleAppointmentButtonListener;
import com.medExpert.mvc.view.pages.ClientPage;
import com.medExpert.mvc.view.pages.StaffPage;

public class MenuOptionButton extends JButton {
	
	private String iconPath;
	private CONTENT_ID content_id;
	
	/**
	 * @param iconPath
	 * @param content_id
	 */
	public MenuOptionButton(String iconPath, CONTENT_ID content_id) {
		this.iconPath = iconPath;
		this.content_id = content_id;
		configureMenuButton();
	}

	private void configureMenuButton() {

		JLabel iconLabel = new JLabel();
		iconLabel.setIcon(new ImageIcon(getClass().getResource(iconPath)));
		iconLabel.setBounds(10, 0, 50, ADimension.MENU_BUTTON_H);
		
		JLabel textLabel = new JLabel();
		textLabel.setText(this.content_id.getName());
		textLabel.setFont(AFont.bold15());
		textLabel.setBounds(90, 25, 250, ADimension.LABEL_HEIGHT);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setPreferredSize(this.getPreferredSize());
		panel.setBackground(Color.white);

		panel.add(iconLabel);
		panel.add(textLabel);
	
		setPreferredSize(new Dimension(ADimension.MENU_WIDTH, ADimension.MENU_BUTTON_H));
		setBorder(null);
		setBorderPainted(false);
		addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseEntered(MouseEvent event) {
				panel.setBackground(AColor.light());
				setBorderPainted(true);
				setBorder(BorderFactory.createMatteBorder(0, 4, 0, 0, AColor.darck()));
			}
			
			@Override
			public void mouseExited(MouseEvent event) {
				panel.setBackground(Color.white);
				setBorder(null);
			}
			
			@Override
			public void mouseReleased(MouseEvent event) {

				AContentPanel content = ContentFactory.getInstance().createContent(content_id);
				
				if (getTopLevelAncestor().getClass() == ClientPage.class) {
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).resetView();
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).updateView(content);
				}
				else if (getTopLevelAncestor().getClass() == StaffPage.class) {
					StaffPageController.getInstance(getCurrentEmployee(), getStaffPage()).resetView();
					StaffPageController.getInstance(getCurrentEmployee(), getStaffPage()).updateView(content);
				}

				if (content instanceof NewClientPanel) {
					NewClientController controller = new NewClientController(new Client(), (NewClientPanel) content);
				
					((NewClientPanel) content).addNewAccountButtonListener(new IButtonListener() {
						
						@Override
						public void buttonWasClicked() {
							controller.updateFieldsInputValidationView();
							controller.saveClient();
						}
					});
				}
				else if (content instanceof AppointmentPanel) {
					AppointmentController controller = new AppointmentController(new Appointment(),(AppointmentPanel) content);
					controller.updateLoggedInView();
					((AppointmentPanel) content).addAppointmentScheduleListener(new IScheduleAppointmentButtonListener() {

						@Override
						public void scheduleAppointmentButtonWasClicked() {
							controller.updateAppointmentVlidationView();
							controller.saveAppointment();
						}
					});
				}
				else if (content instanceof EnterResultsPanel) {
					EnterResultsController controller = new EnterResultsController((EnterResultsPanel) content);
					controller.updateLoginRequestView();
					
					((EnterResultsPanel) content).addSearchAppointmentsButtonListeners(new IButtonListener() {
						
						@Override
						public void buttonWasClicked() {
							controller.updateSearchAppointmentsView();
						}
					});
					
					((EnterResultsPanel) content).addAppointmentsCheckBoxListener(new IAppointmentCheckBoxListener() {
						
						@Override
						public void checkBoxIsSelected() {
							controller.updateTestsListView();
						}
						
						@Override
						public void checkBoxIsDeselected() {
							controller.updateUnselectedAppointmentView();
						}
					});
					
					((EnterResultsPanel) content).addSubmitResultsListener(new IButtonListener() {
						
						@Override
						public void buttonWasClicked() {
							controller.updateResultsInputView();
							controller.saveEnteredResults();
						}
					});
				}
				else if (content instanceof ViewResultsPanel) {
					ViewResultsController controller = new ViewResultsController((ViewResultsPanel) content);
					controller.updateLoginRequestView();
					
					((ViewResultsPanel) content).addSearchTestsButtonListeners(new IButtonListener() {
						
						@Override
						public void buttonWasClicked() {
							controller.updateSearchAppointmentsView();
						}
					});
					
					((ViewResultsPanel) content).addCheckBoxListener(new IAppointmentCheckBoxListener() {
						
						@Override
						public void checkBoxIsSelected() {
							controller.updateAppointmentTestsView();
						}
						
						@Override
						public void checkBoxIsDeselected() {
							controller.updateUnselectedAppointmentView();
						}
					});
				}
			}
		});

		add(panel);
	}
	
	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
	
	private ClientPage getClientPage() {
		return UserChoiceController.getInstance().getClientPage();
	}
	
	private Employee getCurrentEmployee() {
		return UserChoiceController.getInstance().getEmployee();
	}
	
	private StaffPage getStaffPage() {
		return UserChoiceController.getInstance().getStaffPage();
	}
}
