package com.medExpert.mvc.view.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import com.medExpert.mvc.view.ADimension;

public abstract class AMenuPanel extends JPanel {
	
	private MenuHeaderPanel menuHeaderPanel;
	
	public AMenuPanel() {
		build();
		addMenuOptionButtons();
	}
	
	private void build() {
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(ADimension.MENU_WIDTH, 0));
		setBackground(Color.white);
		
		add(getMenuHeaderPanel());		
	}

	protected abstract void addMenuOptionButtons();
	
	public MenuHeaderPanel getMenuHeaderPanel() {
		if (menuHeaderPanel == null) {
			menuHeaderPanel = new MenuHeaderPanel();
		}
		return menuHeaderPanel;
	}
}
