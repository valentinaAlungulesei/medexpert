package com.medExpert.mvc.view.menu;

public enum CONTENT_ID {
	
	NEW_EMPLOYEE("Sign in"),
	NEW_CLIENT("Create new account"),
	CLIENT_LOG_IN("Log in"),
	LOG_OUT("Log out"),
	TESTS("Tests"),
	PRICES("Prices"),
	APPOINTMENT("Schedule appointment"),
	VIEW_RESULTS("View results"),
	ENTER_RESULTS("Enter results"),
	INFO("Good to know");
	
	String name;
	
	private CONTENT_ID(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
