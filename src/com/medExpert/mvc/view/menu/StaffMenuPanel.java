package com.medExpert.mvc.view.menu;

import com.medExpert.util.APath;

public class StaffMenuPanel extends AMenuPanel {
	
	private MenuOptionButton createClientAccountOptionButton;
	private MenuOptionButton testsOptionButton;
	private MenuOptionButton pricesOptionButton;
	private MenuOptionButton appointmentOptionButton;
	private MenuOptionButton enterResultsOptionButton;
	private MenuOptionButton viewResultsOptionButton;
	private MenuOptionButton infoOptionButton;

	@Override
	protected void addMenuOptionButtons() {
		add(getCreateClientAccountOptionButton());
		add(getTestsOptionButton());
		add(getPricesOptionButton());
		add(getAppointmentOptionButton());
		add(getEnterResultsOptionButton());
		add(getViewResultsOptionButton());
		add(getInfoOptionButton());
	}
	
	public MenuOptionButton getCreateClientAccountOptionButton() {
		if (createClientAccountOptionButton == null) {
			createClientAccountOptionButton = new MenuOptionButton(APath.ACCOUNT_PNG, CONTENT_ID.NEW_CLIENT);
		}
		return createClientAccountOptionButton;
	}
	
	private MenuOptionButton getTestsOptionButton() {
		if (testsOptionButton == null) {
			testsOptionButton = new MenuOptionButton(APath.TESTS_PNG, CONTENT_ID.TESTS);
		}
		return testsOptionButton;
	}
	
	private MenuOptionButton getPricesOptionButton() {
		if (pricesOptionButton == null) {
			pricesOptionButton = new MenuOptionButton(APath.PRICES_PNG, CONTENT_ID.PRICES);
		}
		return pricesOptionButton;
	}
	
	private MenuOptionButton getAppointmentOptionButton() {
		if (appointmentOptionButton == null) {
			appointmentOptionButton = new MenuOptionButton(APath.CALENDAR_PNG, CONTENT_ID.APPOINTMENT);
		}
		return appointmentOptionButton;
	}
	
	public MenuOptionButton getEnterResultsOptionButton() {
		if (enterResultsOptionButton == null) {
			enterResultsOptionButton = new MenuOptionButton(APath.RESULTS_PNG, CONTENT_ID.ENTER_RESULTS);
		}
		return enterResultsOptionButton;
	}
	
	public MenuOptionButton getViewResultsOptionButton() {
		if (viewResultsOptionButton == null) {
			viewResultsOptionButton = new MenuOptionButton(APath.RESULTS_PNG, CONTENT_ID.VIEW_RESULTS);
		}
		return viewResultsOptionButton;
	}
	
	public MenuOptionButton getInfoOptionButton() {
		if (infoOptionButton == null) {
			infoOptionButton = new MenuOptionButton(APath.INFO_PNG, CONTENT_ID.INFO);
		}
		return infoOptionButton;
	}
}
