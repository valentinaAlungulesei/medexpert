package com.medExpert.mvc.view.menu;

import com.medExpert.util.APath;

public class ClientMenuPanel extends AMenuPanel {
	
	private MenuOptionButton testsOptionButton;
	private MenuOptionButton pricesOptionButton;
	private MenuOptionButton appointmentOptionButton;
	private MenuOptionButton resultsOptionButton;
	private MenuOptionButton infoOptionButton;

	@Override
	protected void addMenuOptionButtons() {
		add(getTestsOptionButton());
		add(getPricesOptionButton());
		add(getAppointmentOptionButton());
		add(getResultsOptionButton());
		add(getInfoOptionButton());
	}
	
	private MenuOptionButton getTestsOptionButton() {
		if (testsOptionButton == null) {
			testsOptionButton = new MenuOptionButton(APath.TESTS_PNG, CONTENT_ID.TESTS);
		}
		return testsOptionButton;
	}
	
	private MenuOptionButton getPricesOptionButton() {
		if (pricesOptionButton == null) {
			pricesOptionButton = new MenuOptionButton(APath.PRICES_PNG, CONTENT_ID.PRICES);
		}
		return pricesOptionButton;
	}
	
	private MenuOptionButton getAppointmentOptionButton() {
		if (appointmentOptionButton == null) {
			appointmentOptionButton = new MenuOptionButton(APath.CALENDAR_PNG, CONTENT_ID.APPOINTMENT);
		}
		return appointmentOptionButton;
	}
	
	public MenuOptionButton getResultsOptionButton() {
		if (resultsOptionButton == null) {
			resultsOptionButton = new MenuOptionButton(APath.RESULTS_PNG, CONTENT_ID.VIEW_RESULTS);
		}
		return resultsOptionButton;
	}
	
	public MenuOptionButton getInfoOptionButton() {
		if (infoOptionButton == null) {
			infoOptionButton = new MenuOptionButton(APath.INFO_PNG, CONTENT_ID.INFO);
		}
		return infoOptionButton;
	}
}
