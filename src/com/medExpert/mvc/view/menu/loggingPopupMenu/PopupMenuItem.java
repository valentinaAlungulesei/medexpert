package com.medExpert.mvc.view.menu.loggingPopupMenu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;

public class PopupMenuItem extends JMenuItem {
	
	private String name;
	private ActionListener actionListener;
	
	/**
	 * @param name
	 * @param actionListener
	 */
	public PopupMenuItem(String name, ActionListener actionListener) {
		this.name = name;
		this.actionListener = actionListener;

		configureAccountMenuItem();
	}

	private void configureAccountMenuItem() {
		
		JLabel label = new JLabel("   " + name);
		label.setOpaque(true);
		label.setFont(AFont.plain15());
		label.setBackground(Color.white);

		setBorder(null);
		setBorderPainted(false);
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(200, ADimension.ROW_HEIGHT));
		addActionListener(actionListener);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label.setBackground(AColor.light());
				setBorderPainted(true);
				setBorder(BorderFactory.createMatteBorder(0, 4, 0, 0, AColor.darck()));
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				label.setBackground(Color.white);
				setBorder(null);
			}
		});
		
		add(label, BorderLayout.CENTER);
	}
}
