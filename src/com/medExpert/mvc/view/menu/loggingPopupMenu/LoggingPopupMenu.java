package com.medExpert.mvc.view.menu.loggingPopupMenu;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.medExpert.mvc.controller.ClientPageController;
import com.medExpert.mvc.controller.LoginController;
import com.medExpert.mvc.controller.LogoutController;
import com.medExpert.mvc.controller.NewClientController;
import com.medExpert.mvc.controller.NewEmployeeController;
import com.medExpert.mvc.controller.StaffPageController;
import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.model.Account;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.content.AContentPanel;
import com.medExpert.mvc.view.content.LoginPanel;
import com.medExpert.mvc.view.content.ContentFactory;
import com.medExpert.mvc.view.content.IButtonListener;
import com.medExpert.mvc.view.content.LogoutPanel;
import com.medExpert.mvc.view.content.NewClientPanel;
import com.medExpert.mvc.view.content.NewEmployeePanel;
import com.medExpert.mvc.view.menu.CONTENT_ID;
import com.medExpert.mvc.view.pages.ClientPage;
import com.medExpert.mvc.view.pages.StaffPage;

public class LoggingPopupMenu extends JPopupMenu {

	private PopupMenuItem loginMenuItem;
	private PopupMenuItem logoutMenuItem;
	private PopupMenuItem newClientAccountMenuItem;
	private PopupMenuItem newEmployeeAccountMenuItem;

	public LoggingPopupMenu() {
		
		this.setBorder(new LineBorder(AColor.light(), 1));

		this.add(getLoginMenuItem());
		this.add(getLogoutMenuItem());
		
		if (getCurrentClient() != null) {
			this.add(getNewCLientAccountMenuItem());
		}
		
		if (getCurrentEmployee() != null) {
			this.add(getNewEmployeeAccountMenuItem());
		}
	}
	
	private PopupMenuItem getLoginMenuItem() {
		AContentPanel content = ContentFactory.getInstance().createContent(CONTENT_ID.CLIENT_LOG_IN);
		LoginController controller = new LoginController(getCurrentAccount(), (LoginPanel) content);
		
		if (loginMenuItem == null) {
			loginMenuItem = new PopupMenuItem("Log in", new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
	
					if (getCurrentClient() != null) {
						ClientPageController.getInstance(getCurrentClient(), getClientPage()).resetView();
						ClientPageController.getInstance(getCurrentClient(), getClientPage()).updateView(content);
					}
					else if (getCurrentEmployee() != null) {
						StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).resetView();
						StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).updateView(content);
					}
					
					((LoginPanel) content).addLoginButtonListener(new IButtonListener() {

						@Override
						public void buttonWasClicked() {
							controller.updateCredentialsValidationView();
							controller.setCurrentAccountData();
						}
					});
				}
			});
		}
		return loginMenuItem;
	}
	
	private PopupMenuItem getLogoutMenuItem() {
		if (logoutMenuItem == null) {
			
			AContentPanel content = ContentFactory.getInstance().createContent(CONTENT_ID.LOG_OUT);
			LogoutController controller = new LogoutController(getCurrentAccount(), (LogoutPanel) content);
			
			logoutMenuItem = new PopupMenuItem("Log out", new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (getCurrentClient() != null) {
						ClientPageController.getInstance(getCurrentClient(), getClientPage()).resetView();
						ClientPageController.getInstance(getCurrentClient(), getClientPage()).updateView(content);
					}
					else if (getCurrentEmployee() != null) {
						StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).resetView();
						StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).updateView(content);
					}
					controller.updateLogOutView();
					controller.setAccountLoggedOut();
				}
			});
		}
		return logoutMenuItem;
	}
	
	private PopupMenuItem getNewCLientAccountMenuItem() {
		if (newClientAccountMenuItem == null) {
			
			AContentPanel content = ContentFactory.getInstance().createContent(CONTENT_ID.NEW_CLIENT);
			NewClientController controller = new NewClientController(getCurrentClient(),(NewClientPanel) content);

			newClientAccountMenuItem = new PopupMenuItem("Create new account", new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).resetView();
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).updateView(content);
						
					((NewClientPanel) content).addNewAccountButtonListener(new IButtonListener() {
							
						@Override
						public void buttonWasClicked() {
							controller.updateFieldsInputValidationView();
							controller.saveClient();
						}
					});			
				}
			});
		}
		return newClientAccountMenuItem;
	}
	
	public PopupMenuItem getNewEmployeeAccountMenuItem() {
		if (newEmployeeAccountMenuItem == null) {
			AContentPanel content = ContentFactory.getInstance().createContent(CONTENT_ID.NEW_EMPLOYEE);
			NewEmployeeController controller = new NewEmployeeController(getCurrentEmployee(), (NewEmployeePanel) content);
			
			newEmployeeAccountMenuItem = new PopupMenuItem("Sign in", new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).resetView();
					StaffPageController.getInstance(getCurrentEmployee(), getCurrentStaffPage()).updateView(content);
					
					((NewEmployeePanel) content).addNewEmployeeButtonLister(new IButtonListener() {
						
						@Override
						public void buttonWasClicked() {
							controller.updateInputValidationView();
							controller.saveEmployee();
						}
					});
				}
			});
		}
		return newEmployeeAccountMenuItem;
	}
	
	private Account getCurrentAccount() {
		if (getCurrentClient() != null) {
			return getCurrentClient();
		}
		else if (getCurrentEmployee() != null) {
			return getCurrentEmployee();
		}
		return null;
	}
	
	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
	
	private ClientPage getClientPage() {
		return UserChoiceController.getInstance().getClientPage();
	}
	
	private Employee getCurrentEmployee() {
		return UserChoiceController.getInstance().getEmployee();
	}
	
	private StaffPage getCurrentStaffPage() {
		return UserChoiceController.getInstance().getStaffPage();
	}
}
