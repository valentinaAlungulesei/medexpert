package com.medExpert.mvc.view.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.medExpert.mvc.controller.ClientPageController;
import com.medExpert.mvc.controller.StaffPageController;
import com.medExpert.mvc.controller.UserChoiceController;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.controller.HomePageController;
import com.medExpert.mvc.view.AColor;
import com.medExpert.mvc.view.ADimension;
import com.medExpert.mvc.view.AFont;
import com.medExpert.mvc.view.menu.loggingPopupMenu.LoggingPopupMenu;
import com.medExpert.mvc.view.pages.ClientPage;
import com.medExpert.mvc.view.pages.StaffPage;
import com.medExpert.util.APath;

public class MenuHeaderPanel extends JPanel {

	private JButton goBackButton;
	private JButton accountButton;

	public MenuHeaderPanel() {
		build();
	}

	private void build() {

		// Set "MEDEXPERT" label.
		JLabel logoTextLabel = new JLabel("MedExpert");
		logoTextLabel.setHorizontalAlignment(SwingConstants.CENTER);
		logoTextLabel.setFont(AFont.bold30());
		logoTextLabel.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, AColor.darck()));
		logoTextLabel.setBounds(0, 10, ADimension.MENU_WIDTH, 50);

		// Set "MENU" label under the "MEDEXPERT" label.
		JLabel menuLabel = new JLabel("MENU");
		menuLabel.setFont(AFont.plain15());
		menuLabel.setForeground(Color.LIGHT_GRAY);
		menuLabel.setBounds((ADimension.MENU_WIDTH / 2) - 30, 70, 50, ADimension.LABEL_HEIGHT);

		this.setLayout(null);
		this.setPreferredSize(new Dimension(ADimension.MENU_WIDTH, 90));
		this.setBackground(Color.white);
		
		this.add(getGoBackButton());
		this.add(logoTextLabel);
		this.add(menuLabel);
		this.add(getAccountButton());
	}
	
	private JButton getGoBackButton() {
		goBackButton = new JButton();
		goBackButton.setIcon(new ImageIcon(getClass().getResource(APath.GO_BACK_PNG)));
		goBackButton.setBorderPainted(false);
		goBackButton.setBounds(5, 13, 40, 40);
		goBackButton.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseEntered(MouseEvent e) {
				goBackButton.setOpaque(true);
				goBackButton.setContentAreaFilled(true);
				goBackButton.setBackground(AColor.light());
				goBackButton.setBorderPainted(true);
				goBackButton.setBorder(BorderFactory.createLineBorder(AColor.darck(),  2));
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				goBackButton.setContentAreaFilled(false);
				goBackButton.setBorderPainted(false);
			}
			
			@Override
			public void mouseReleased(MouseEvent event) {
				goBackButton.setContentAreaFilled(false);
				goBackButton.setBorderPainted(false);
				
				if (getTopLevelAncestor().getClass() == ClientPage.class) {
					UserChoiceController.getInstance().logOutClient();
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).resetView();
					ClientPageController.getInstance(getCurrentClient(), getClientPage()).disposePage();
				}
				else if (getTopLevelAncestor().getClass() == StaffPage.class) {
					UserChoiceController.getInstance().logOutEmployee();
					StaffPageController.getInstance(getCurrentEmployee(), getStaffPage()).resetView();
					StaffPageController.getInstance(getCurrentEmployee(), getStaffPage()).disposePage();
					UserChoiceController.getInstance().resetView();
				}
				HomePageController.getInstance().showHomePage();
			}
		});
		return goBackButton;
	}
	
	public JButton getAccountButton() {
		accountButton = new JButton();
		accountButton.setBounds(ADimension.MENU_WIDTH - 50, 13, 40, 40);
		accountButton.setPreferredSize(new Dimension(40, 40));
		accountButton.setBorderPainted(false);
		accountButton.setIcon(new ImageIcon(getClass().getResource(APath.ACCOUNT_PNG)));
		accountButton.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseEntered(MouseEvent e) {
				accountButton.setOpaque(true);
				accountButton.setContentAreaFilled(true);
				accountButton.setBackground(AColor.light());
				accountButton.setBorderPainted(true);
				accountButton.setBorder(BorderFactory.createLineBorder(AColor.darck(),  2));

				showPopupSignMenu(e);
				repaint();
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				accountButton.setContentAreaFilled(false);
				accountButton.setBorderPainted(false);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				showPopupSignMenu(e);
			}
			
			private void showPopupSignMenu(MouseEvent e) {
				if (e.isPopupTrigger()) {
					LoggingPopupMenu loggingPopupMenu = new LoggingPopupMenu();
					loggingPopupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		return accountButton;
	}
	
	private Client getCurrentClient() {
		return UserChoiceController.getInstance().getClient();
	}
	
	private ClientPage getClientPage() {
		return UserChoiceController.getInstance().getClientPage();
	}
	
	private Employee getCurrentEmployee() {
		return UserChoiceController.getInstance().getEmployee();
	}
	
	private StaffPage getStaffPage() {
		return UserChoiceController.getInstance().getStaffPage();
	}
}
