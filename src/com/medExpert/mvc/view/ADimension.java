package com.medExpert.mvc.view;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;

public abstract class ADimension {
	
	// Get local screen free space configuration.
	private static GraphicsEnvironment grEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
	private static GraphicsConfiguration grConfiguration = grEnvironment.getDefaultScreenDevice().getDefaultConfiguration();
	private static Rectangle bounds = grConfiguration.getBounds();
	private static Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(grConfiguration);
	
	// Free space dimensions.
	public static final int SCREEN_X  = bounds.x;
	public static final int SCREEN_Y = bounds.y;
	public static final int SCREEN_WIDTH = bounds.width;
	public static final int SCREEN_HEIGHT = bounds.height;
	
	// Menu.
	public static final int MENU_WIDTH = SCREEN_WIDTH / 5;
	public static final int MENU_BUTTON_H = 70;
	
	// Content.
	public static final int CONTENT_X = SCREEN_WIDTH / 9;
	public static final int CONTENT_WIDTH = (int)(SCREEN_WIDTH / 3);
	public static final int TITLE_HEIGHT = 65;
	public static final int INFO_LABEL_HEIGHT = 15;
	
	// Current account.
	public static final int CURRENT_ACCOUNT_X = SCREEN_WIDTH - (2 * MENU_WIDTH);
	public static final int CURRENT_ACCOUNT_WIDTH = MENU_WIDTH;
	
	// Appointment.
	public static final int CALENDAR_WIDTH = 300;
	public static final int CALENDAR_HEIGHT = 280;
	public static final int CALENDAR_SPACING = 5;
	public static final int MONTH_PANEL_X_INSET = 20;
	public static final int COMBO_BOX_WIDTH = CONTENT_WIDTH / 4;
	public static final int HOUR_PICKER_X = CONTENT_WIDTH - COMBO_BOX_WIDTH;
	public static final int ROW_HEIGHT = 30;
	
	// Buttons.
	public static final int BUTTON_WIDTH = 150;
	public static final int BUTTON_HEIGHT = 40;
	public static final int BUTTON_X = (CONTENT_WIDTH / 2) - (BUTTON_WIDTH / 2); 
	public static final int BUTTON_Y = SCREEN_HEIGHT - (4 * BUTTON_HEIGHT); 
	public static final int REGISTER_BUTTON_X = (CONTENT_WIDTH / 2) - (BUTTON_WIDTH / 2);
	public static final int REGISTER_BUTTON_Y = SCREEN_HEIGHT - (5 * BUTTON_HEIGHT);
	
	// fields from CreateNewAppointmentPanel
	public static final int FIELD_X = CONTENT_WIDTH / 6;
	public static final int FIELD_Y = 115;
	public static final int FIELD_WIDTH = (int)(CONTENT_WIDTH / 1.5d);
	public static final int FIELD_HEIGHT = 50;
	public static final int FIELD_Y_SPACING = 80;
	public static final int LABEL_HEIGHT = 20;
	
	public static final int BOTTOM_INSET = 2 * screenInsets.bottom;
}
