package com.medExpert.mvc.view;

import java.awt.Font;

public abstract class AFont {
	
	
	// Bold fonts
	public static Font bold11() {
		return new Font("Helvetica", Font.BOLD, 11);
	}
	
	public static Font bold12() {
		return new Font("Helvetica", Font.BOLD, 12);
	}
	
	public static Font bold13() {
		return new Font("Helvetica", Font.BOLD, 13);
	}
	
	public static Font bold15() {
		return new Font("Helvetica", Font.BOLD, 15);
	}
	
	public static Font bold30() {
		return new Font("Helvetica", Font.BOLD, 30);
	}
	
	public static Font bold40() {
		return new Font("Helvetica", Font.BOLD, 40);
	}
	
	
	// Italic fonts
	public static Font italic12() {
		return new Font("Helvetica", Font.ITALIC, 12);
	}
	
	public static Font italic13() {
		return new Font("Helvetica", Font.ITALIC, 13);
	}

	// Plain fonts
	public static Font plain15() {
		return new Font("Helvetica", Font.PLAIN, 15);
	}
	
	public static Font plain20() {
		return new Font("Helvetica", Font.PLAIN, 20);
	}
}
