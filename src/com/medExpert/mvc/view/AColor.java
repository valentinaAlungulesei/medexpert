package com.medExpert.mvc.view;

import java.awt.Color;

public abstract class AColor {

	public static Color darck() {
		return new Color(0, 90, 90);
	}
	
	public static Color light() {
		return new Color(227,255,255);
	}
}
