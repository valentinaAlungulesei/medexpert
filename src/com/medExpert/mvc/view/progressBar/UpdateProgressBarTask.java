package com.medExpert.mvc.view.progressBar;

import javax.swing.SwingUtilities;

public class UpdateProgressBarTask extends Thread {

	private MyProgressBar progressBar;

	public UpdateProgressBarTask(MyProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public void run() {

		for (int index = 0; index <= 100; index++) {

			final int counter = index;

			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					progressBar.setValue(counter);
				}
			});
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				System.err.println("An error has occured with the progress bar!");
				e.printStackTrace();
			}
		}
		progressBar.setStringPainted(true);
		progressBar.setString("You have been logged out successfully!");
	}
}
