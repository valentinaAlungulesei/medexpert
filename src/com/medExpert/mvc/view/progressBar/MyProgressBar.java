package com.medExpert.mvc.view.progressBar;

import javax.swing.BorderFactory;
import javax.swing.JProgressBar;

import com.medExpert.mvc.view.AColor;

public class MyProgressBar extends JProgressBar {
	
	/**
	 * @param min
	 * @param max
	 */
	public MyProgressBar(int min, int max) {
		super(min, max);
		configure();
	}

	private void configure() {
		setDoubleBuffered(true);
		setUI(new MyProgressBarUI());
		setBorder(BorderFactory.createLineBorder(AColor.light(), 1));
	}
}
