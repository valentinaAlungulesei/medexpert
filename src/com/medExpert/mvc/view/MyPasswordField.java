package com.medExpert.mvc.view;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class MyPasswordField extends JPasswordField {
	
	private String fieldTitile;
	
	public MyPasswordField(String fieldTitile) {
		this.fieldTitile = fieldTitile;
		
		configure();
	}

	private void configure() {
		setOpaque(false);
		setCaretColor(Color.white);
		setForeground(Color.white);
		setSelectionColor(Color.white);
		setFont(AFont.bold15());
		setAlignmentY(CENTER_ALIGNMENT);
		setHorizontalAlignment(SwingConstants.CENTER);
		setBorder(BorderFactory.createTitledBorder(
				  new LineBorder(Color.white, 1), 
				  fieldTitile + " *", 
				  TitledBorder.LEFT,
				  TitledBorder.DEFAULT_POSITION, 
				  AFont.italic12(), 
				  Color.white));	
	}

	public void setWarning(String warningMessage, JLabel warningLabel) {
		// Add the specified warning message under the field 
		// and set it's border and colour to be red.
		warningLabel.setText(warningMessage);
		warningLabel.setFont(AFont.bold13());
		warningLabel.setHorizontalAlignment(SwingConstants.CENTER);
		warningLabel.setForeground(Color.red);
		
		setBorder(BorderFactory.createTitledBorder(
				 new LineBorder(Color.red, 1), 
				 fieldTitile + " *", 
				 TitledBorder.LEFT,
				 TitledBorder.DEFAULT_POSITION, 
				 AFont.italic12(), 
				 Color.white));
		
		addMouseListener(new MouseAdapter() {
			// Reset field.
			@Override
			public void mouseReleased(MouseEvent e) {
				
				warningLabel.setText("");
				setText("");
				
				setBorder(BorderFactory.createTitledBorder(
						 new LineBorder(Color.white, 1), 
						 fieldTitile + " *", 
						 TitledBorder.LEFT,
						 TitledBorder.DEFAULT_POSITION, 
						 AFont.italic12(), 
						 Color.white));
			}
		});
	}
}
