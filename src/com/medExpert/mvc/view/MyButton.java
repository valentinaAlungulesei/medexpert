package com.medExpert.mvc.view;

import javax.swing.JButton;
import javax.swing.SwingConstants;

public class MyButton extends JButton {
	
	private String name;
	
	/**
	 * @param name
	 */
	public MyButton(String name) {
		this.name = name;

		build();
	}

	private void build() {
		setText(name);
		setHorizontalAlignment(SwingConstants.CENTER);
		setFont(AFont.plain15());
		setContentAreaFilled(true);
		setBorderPainted(true);
		setFocusPainted(false);
	}
}
