package com.medExpert;

import javax.swing.SwingUtilities;

import com.medExpert.mvc.controller.HomePageController;
import com.medExpert.repository.MedExpertDB;
import com.medExpert.util.TestDataReader;

/**
 * Entry point of the application.
 * 
 * @author valentinaAlungulesei
 */
public class MedExpert {

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				HomePageController.getInstance().showHomePage();
				TestDataReader.getInstance().readFromFile();
				MedExpertDB.getInstance().createAppointmentsTableIfNotExists(); 
				MedExpertDB.getInstance().createClientsTableIfNotExists();
				MedExpertDB.getInstance().createTestsTableIfNotExists();
				MedExpertDB.getInstance().createEmployeesTableIfNotExists();
			}
		});
	}
}
