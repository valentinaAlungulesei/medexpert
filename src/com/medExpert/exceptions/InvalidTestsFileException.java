package com.medExpert.exceptions;

import com.medExpert.mvc.controller.TestsDataReaderController;

public class InvalidTestsFileException extends Exception {

	public InvalidTestsFileException(String message) {
		super(message);
		new TestsDataReaderController().updateInvalidFileStructure();
	}
}
