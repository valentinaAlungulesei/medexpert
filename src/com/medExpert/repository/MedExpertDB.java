package com.medExpert.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.medExpert.mvc.model.Appointment;
import com.medExpert.mvc.model.Client;
import com.medExpert.mvc.model.Employee;
import com.medExpert.mvc.model.Test;
import com.medExpert.util.DESIGNATION;
import com.medExpert.util.SALARY_RANGE;

public class MedExpertDB {
	
	private static MedExpertDB instance;
	private String path;
	private Connection connection;
	private Statement statement;
	
	private MedExpertDB() {
		path = "jdbc:sqlite:medExpert.db";
	}
	
	public static MedExpertDB getInstance() {
		synchronized (MedExpertDB.class) {
			if (instance == null) {
				instance = new MedExpertDB();
			}
		}
		return instance;
	}
	
	// CLIENTS --------------------------------------------------------------------------
	
	public void createClientsTableIfNotExists() {
		
		String sql = "CREATE TABLE IF NOT EXISTS clients ("
				   + "client_id INTEGER PRIMARY KEY AUTOINCREMENT, "
				   + "client_personal_id_hash TEXT, "
				   + "client_password_hash TEXT, "
				   + "client_first_name TEXT, "
				   + "client_last_name TEXT, "
				   + "client_age INTEGER, "
				   + "client_gender CHARACTER)";
		try {
			openConnection();
			statement.executeUpdate(sql);
		}	
		catch (SQLException e) {
			System.err.print("An error occurred while creating 'clients' table.");
			e.printStackTrace();
		} 
		finally {
			closeConnection();
		}
	}
	
	/**
	 * @param idHashToCheck
	 * @return true, if id is in DB
	 */
	public boolean isIdHashInDB(String idHashToCheck) {
		
		String sql = "SELECT COUNT (*) client_personal_id_hash "
				   + "FROM clients "
				   + "WHERE client_personal_id_hash = '" + idHashToCheck + "'";
		
		try {
			openConnection();
			ResultSet idCountResult = statement.executeQuery(sql);
			int count = idCountResult.getInt("client_personal_id_hash");

			if (count != 0) {
				return true;
			}
			return false;
		}
		catch (SQLException e) {
			System.err.println("An error occurred while checking if the client exists in DB!");
			e.printStackTrace();
			return false;
		}
		finally {
			closeConnection();
		}
	}
	
	/**
	 * @return clientsMap with personalIdHash as 'key' & passwordHash as 'value'.
	 */
	public HashMap<String, String> getClientsLoginInfo() {
		
		String sql = "SELECT * FROM clients";
		HashMap<String, String> clientsMap = new HashMap<String, String>();
		
		try { 
			openConnection();
			ResultSet resultSet = statement.executeQuery(sql);
		
			while (resultSet.next()) {
				String personalIdHash = resultSet.getString("client_personal_id_hash");
				String passwordHash = resultSet.getString("client_password_hash");
				
				clientsMap.put(personalIdHash, passwordHash);
			}
		}	
		catch (SQLException e) {
			System.err.println("An error accurred while getting clients login info from DB!");
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
		return clientsMap;
	}
	
	/**
	 * @param client
	 */
	public void saveClient(Client client) {
		String sql = "INSERT INTO clients("
				   + "client_personal_id_hash, client_password_hash, client_first_name, "
				   + "client_last_name, client_age, client_gender) VALUES ('"
				   + client.getPersonalIdHash() + "', '"
				   + client.getPasswordHash() + "', '"
				   + client.getFirstName() + "', '"
				   + client.getLastName() + "', " 
				   + client.getAge() + ", '" 
				   + client.getGender() + "')";
		try { 
			openConnection();
			statement.executeUpdate(sql);
			
			System.out.println("Client account '" + client.getFullName() + "' has been added to DB.");
		}
		catch (SQLException e) {
			System.err.println("An error occurred while adding the client '" + client.getFullName() + "' to DB!");
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
	}
	
	public Client getClientByIdHash(String idHash) {
		
		Client client = null;
		
		String idSql = "SELECT (client_id) FROM clients WHERE client_personal_id_hash = '" + idHash + "'";
		String firstNameSql = "SELECT (client_first_name) FROM clients WHERE client_personal_id_hash = '" + idHash + "'";
		String lastNameSql = "SELECT (client_last_name) FROM clients WHERE client_personal_id_hash = '" + idHash + "'";
		String ageSql = "SELECT (client_age) FROM clients WHERE client_personal_id_hash = '" + idHash + "'";
		String genderSql = "SELECT (client_gender) FROM clients WHERE client_personal_id_hash = '" + idHash + "'";
		
		try {
			openConnection();
			
			ResultSet idResult = statement.executeQuery(idSql);
			int id = idResult.getInt("client_id");
			
			ResultSet firstNameResult = statement.executeQuery(firstNameSql);
			String firstName = firstNameResult.getString("client_first_name");

			ResultSet lastNameResult = statement.executeQuery(lastNameSql);
			String lastName = lastNameResult.getString("client_last_name");
			
			ResultSet ageResult = statement.executeQuery(ageSql);
			byte age = ageResult.getByte("client_age");
			
			ResultSet genderResult = statement.executeQuery(genderSql);
			char gender = genderResult.getString("client_gender").charAt(0);
		
			client = new Client(id, firstName, lastName, age, gender);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while getting the client object by his personal id.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return client;
	}
	
	public int getClientIdByPersonalIdHash(String personalIdHash) {
		String sql = "SELECT client_id FROM clients WHERE client_personal_id_hash ='" + personalIdHash + "'";
		int id = 0;
		try {
			openConnection();
			ResultSet idResult =  statement.executeQuery(sql);
			id = idResult.getInt("client_id");
			
		} catch (SQLException e) {
			System.err.print("An error occurred while getting the client id by his personal id hash.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return id;
	}
	
	// APPOINTMENTS --------------------------------------------------------------------------
	
	public void createAppointmentsTableIfNotExists() {

		String sql = "CREATE TABLE IF NOT EXISTS appointments ("
				   + "appointment_id INTEGER PRIMARY KEY AUTOINCREMENT,"
				   + "appointment_tests_list TEXT, "
				   + "appointment_date INTEGER, "
				   + "appointment_month TEXT, "
				   + "appointment_time TEXT, "
				   + "client_id INTEGER)";
		try {
			openConnection();
			statement.executeUpdate(sql);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while creating 'appointments' table.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param appointment
	 */
	public void saveAppointment(Appointment appointment) {
		
		String sql = "INSERT INTO appointments "
				   + "(appointment_tests_list, appointment_date, appointment_month, appointment_time, client_id) VALUES ('"
				   + appointment.getTestsList() + "', " 
				   + appointment.getDate() + ", '"
				   + appointment.getMonth() + "', '"
				   + appointment.getTime() + "', " 
				   + appointment.getClientId() + ")";
		try {
			openConnection();
			statement.executeUpdate(sql);
			
			System.out.println("Appointment was saved successfully.");
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while saving appointment.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param clintId
	 * @return id
	 */
	public int getAppointmentIdByClientId(int clientId) {
		
		int id = 0;
		String sql = "SELECT (appointment_id) FROM appointments WHERE client_id = " + clientId;
		
		try {
			openConnection();
			ResultSet idResult = statement.executeQuery(sql);
			id = idResult.getInt("appointment_id");
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while getting appointment id by client id.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return id;
	}
	
	/**
	 * @param clientId
	 * @return appointmentsList
	 */
	public List<Appointment> getAppointmentsByClientId(int clientId) {
		List<Appointment> appointmentsList = new ArrayList<Appointment>();
		List<String> testsList;
		String sql = "SELECT * FROM appointments WHERE client_id=" + clientId;
	
		try {
			openConnection();
			ResultSet result = statement.executeQuery(sql);
			
			while (result.next()) {
				String[] tests = (result.getString("appointment_tests_list")).split(",");

				tests[0] = tests[0].substring(1);
				tests[tests.length - 1] = tests[tests.length - 1].replace("]", "");
		
				testsList = new ArrayList<String>();
				for (String test : tests) {
					testsList.add(test);
				}
				
				int id = result.getInt("appointment_id");
				byte date = result.getByte("appointment_date");
				String month = result.getString("appointment_month");
				String time = result.getString("appointment_time");
	
				appointmentsList.add(new Appointment(id, clientId, testsList, date, month, time));
			}
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while getting appointment list by client id.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return appointmentsList;
	}
	
	// TESTS ------------------------------------------------------------------------------
	
	public void createTestsTableIfNotExists() {
		
		String sql = "CREATE TABLE IF NOT EXISTS tests ("
				   + "test_id INTEGER PRIMARY KEY AUTOINCREMENT, "
				   + "appointment_id INTEGER,"
				   + "test_name TEXT, "
				   + "test_result DOUBLE, "
				   + "test_unit TEXT, "
				   + "test_min_value DOUBLE, "
				   + "test_max_value DOUBLE, "
				   + "test_price DOUBLE)";
		try {
			openConnection();
			statement.executeUpdate(sql);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while creating tests table.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param test
	 */
	public void saveTest(Test test) {
		
		String sql = "INSERT INTO tests "
				   + "(appointment_id, test_name, test_result, test_unit, test_min_value, test_max_value, test_price) "
				   + "VALUES ("
				   + test.getAppointmentId() + ", '"
				   + test.getName() + "', " 
				   + test.getResult() + ", '"
				   + test.getUnit() + "', "
				   + test.getMinValue() + ", "
				   + test.getMaxValue() + ", "
				   + test.getPrice()  + ")";

		try {
			openConnection();
			statement.executeUpdate(sql);
			
			System.out.println("Test was added to DB.");
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while saving a test into tests table.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param appointmentId
	 * @return testList
	 */
	public List<Test> getTestListByAppointmentId(int appointmentId) {
		String sql = "SELECT * FROM tests WHERE appointment_id=" + appointmentId;
		List<Test> testList = new ArrayList<Test>();
		
		try {
			openConnection();
			ResultSet resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				int id = resultSet.getInt("test_id");
				String name = resultSet.getString("test_name");
				double result = resultSet.getDouble("test_result");
				String unit = resultSet.getString("test_unit");
				double minValue = resultSet.getDouble("test_min_value");
				double maxValue = resultSet.getDouble("test_max_value");
		
				testList.add(new Test(appointmentId, name, result, unit, minValue, maxValue));
			}
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while getting test list by appointment id from DB.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return testList;
	}
	
	/**
	 * @param result
	 * @param name
	 */
	public void updateTestResultByName(double result, String name) {
		String sql = "UPDATE tests SET (test_result) = (" + result + ") WHERE test_name='" + name + "'";
		
		try {
			openConnection();
			statement.executeUpdate(sql);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while updating test result.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	// EMPLOYEES --------------------------------------------------------------------------
	
	public void createEmployeesTableIfNotExists() {
		
		String sql = "CREATE TABLE IF NOT EXISTS employees ("
				   + "employee_id INTEGER PRIMARY KEY AUTOINCREMENT, "
				   + "employee_username TEXT NOT NULL, "
				   + "employee_password_hash TEXT NOT NULL, "
				   + "employee_first_name TEXT, "
				   + "employee_last_name TEXT, "
				   + "employee_designation TEXT, "
				   + "employee_salary TEXT)";
		try {
			openConnection();
			statement.executeUpdate(sql);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while creating employees table.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param employee
	 */
	public void saveEmployee(Employee employee) {
		
		String sql = "INSERT INTO employees "
				   + "(employee_username, employee_password_hash, employee_first_name, "
				   + "employee_last_name, employee_designation, employee_salary) "
				   + "VALUES ('"
				   + employee.getUsername() + "', '"
				   + employee.getPasswordHash() + "', '"
				   + employee.getFirstName() + "', '"
				   + employee.getLastName() + "', '"
				   + employee.getDesignation().getName() + "', '"
				   + employee.getSalaryRange() + "')";
		System.out.println("Employee account for '" + employee.getFullName() + "' has been added to DB.");
		
		try {
			openConnection();
			statement.executeUpdate(sql);
		} 
		catch (SQLException e) {
			System.err.print("An error occurred while saving employee " + employee.getFullName() + " into DB.");
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}
	
	/**
	 * @param username
	 * @return true if username exists in employees table
	 */
	public boolean isUsernameInDB(String username) {
		
		String sql = "SELECT COUNT (*) employee_username "
				   + "FROM employees "
				   + "WHERE employee_username = '" + username + "'";
		try {
			openConnection();
			ResultSet usernameCountResult = statement.executeQuery(sql);
			int count = usernameCountResult.getInt("employee_username");

			if (count != 0) {
				return true;
			}
			return false;
		}
		catch (SQLException e) {
			System.err.println("An error occurred while checking if the username exists in DB.");
			e.printStackTrace();
			return false;
		}
		finally {
			closeConnection();
		}
	}
	
	/**
	 * @return employeesMap with username as 'key' & passwordHash as 'value'.
	 */
	public HashMap<String, String> getEmployeesLoginInfo() {
		
		String sql = "SELECT * FROM employees";
		HashMap<String, String> employeesMap = new HashMap<String, String>();
		
		try { 
			openConnection();
			ResultSet resultSet = statement.executeQuery(sql);
		
			while (resultSet.next()) {
				String username = resultSet.getString("employee_username");
				String passwordHash = resultSet.getString("employee_password_hash");
				
				employeesMap.put(username, passwordHash);
			}
		}	
		catch (SQLException e) {
			System.err.println("An error occurred while getting employees login info from DB.");
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
		return employeesMap;
	}
	
	/**
	 * @param username
	 * @return id
	 */
	public int getEmployeeIdByUsername(String username) {
		int id = 0;
		String sql = "SELECT employee_id FROM employees WHERE employee_username='" + username + "'";
		
		try { 
			openConnection();
			ResultSet resultSet = statement.executeQuery(sql);
			id = resultSet.getInt("employee_id");
		}	
		catch (SQLException e) {
			System.err.println("An error occurred while getting employee id by his username.");
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
		return id;
	}
	
	/**
	 * @param id
	 * @return employee
	 */
	public Employee getEmployeeDataById(int id) {
		Employee employee = null;
		
		String firstNameSql = "SELECT employee_first_name FROM employees WHERE employee_id=" + id;
		String lastNameSql = "SELECT employee_last_name FROM employees WHERE employee_id=" + id;
		String designationSql = "SELECT employee_designation FROM employees WHERE employee_id=" + id;
		String salaryRangeSql = "SELECT employee_salary FROM employees WHERE employee_id=" + id;
		
		try { 
			openConnection();
			
			ResultSet firstNameResult = statement.executeQuery(firstNameSql);
			String firstName = firstNameResult.getString("employee_first_name");
			
			ResultSet lastNameResult = statement.executeQuery(lastNameSql);
			String lastName = lastNameResult.getString("employee_last_name");
			
			ResultSet designationResult = statement.executeQuery(designationSql);
			DESIGNATION designation = DESIGNATION.getByName(designationResult.getString("employee_designation"));
			
			ResultSet salaryRangeResult = statement.executeQuery(salaryRangeSql);
			SALARY_RANGE salaryRange = SALARY_RANGE.valueOf(salaryRangeResult.getString("employee_salary"));
			
			employee = new Employee(firstName, lastName, designation, salaryRange);
		}	
		catch (SQLException e) {
			System.err.println("An error occurred while getting employee data by his id.");
			e.printStackTrace();
		}
		finally {
			closeConnection();
		}
		return employee;
	}
	
	// CONNECTION --------------------------------------------------------------------------
	
	private void openConnection() throws SQLException {
		connection = DriverManager.getConnection(path);
		statement = connection.createStatement();
	}
	
	private void closeConnection() {
		try {
			connection.close();
		} 
		catch (SQLException e) {
			connection = null;
		}
	}
}
