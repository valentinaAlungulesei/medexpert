package com.medExpert.util;

public abstract class APath {
	
	public static final String BACKGROUND_JPG = "/background.jpg";
	public static final String TESTS_PNG = "/tests.png";
	public static final String PRICES_PNG = "/price.png";
	public static final String CALENDAR_PNG = "/calendar.png";
	public static final String RESULTS_PNG = "/results.png";
	public static final String INFO_PNG = "/info.png";
	public static final String GO_BACK_PNG = "/goback.png";
	public static final String ACCOUNT_PNG = "/account.png";
	public static final String ENTER_PNG = "/enter.png";
	public static final String BACK_PNG = "/back.png";
	
	public static final String TESTS_TXT = "/Users/valentinaAlungulesei/myProjects/MedExpert/res/tests.txt";

}
