package com.medExpert.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Util {
	
	private static Util instance;
	
	private Util() {}
	
	public static Util getInstance() {
		synchronized (Util.class) {
			if (instance == null) {
				instance = new Util();
			}
		}
		return instance;
	}
	
	/**
	 * @param plainData
	 * @return dataHash
	 */
	public String hashPersonalData(String plainData) {

		String dataHash = null;
	
		try {
			byte[] plainBytes = plainData.getBytes("UTF-8");
			MessageDigest digester = MessageDigest.getInstance("SHA-256");
			byte[] digestedBytes = digester.digest(plainBytes);
			
			// Remove byte value = 39 <'> and byte value = 64 <@> from digestedBytes 
			// because they're interfering with SQLite syntax.
			List<Byte> byteList = new ArrayList<Byte>();
			for (int index = 0; index < digestedBytes.length; index++) {
				if (digestedBytes[index] != 39 && digestedBytes[index] != 64) {
					byteList.add(digestedBytes[index]);
				}
			}
			
			byte[] checkedBytes = new byte[byteList.size()];
			for (int index = 0; index < byteList.size(); index++) {
				checkedBytes[index] = byteList.get(index);
			}
			
			dataHash = new String(checkedBytes);
//			System.out.println(new String(checkedBytes));
		} 
		catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			System.err.println("An error occurred while hashing data!");
			e.printStackTrace();
		}	
		return dataHash;
	}
	
	/**
	 * @param passwordInput
	 * @return true, if passwordInput has at least 1 digit, 1 lower case, 1 upper case,
	 *  	   1 special char, has no white spacing and contains from 5 to 20 chars
	 */
	public boolean checkPasswordComplexity(String passwordInput) {
		String complexityRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{5,}$";
		
		if (passwordInput.matches(complexityRegex)) {
			return true;
		}
		return false;	
	}
	
	/**
	 * @param name
	 * @return Name
	 */
	public String capitalizeAllFirstLetters(String name) {
		
		char[] nameArray = name.toCharArray();
		
		// Capitalize first letter.
		nameArray[0] = Character.toUpperCase(nameArray[0]);
		
		// Capitalize all letters that follows after a white space.
		for (int index = 1; index < name.length(); index++) {
			if (Character.isWhitespace(nameArray[index - 1])) {
				nameArray[index] = Character.toUpperCase(nameArray[index]);
			}
		}
		return new String(nameArray);
	}
}
