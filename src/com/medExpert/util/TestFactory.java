package com.medExpert.util;

import com.medExpert.mvc.model.Test;

public class TestFactory implements ITestFactory {
	
	private static TestFactory instance;
	
	private TestFactory() {}
	
	public static TestFactory getInstance() {
		synchronized (TestFactory.class) {
			if (instance == null) {
				instance = new TestFactory();
			}
		}
		return instance;
	}

	@Override
	public Test createTest(String[] values) {
		if (values != null || values.length != 0) {
			String name = values[TESTS_FILE_DEFINITION.NAME.getColIndex()];
			double price = Double.parseDouble(values[TESTS_FILE_DEFINITION.PRICE.getColIndex()]);
			String unit = values[TESTS_FILE_DEFINITION.UNIT.getColIndex()];
			double minValue = Double.parseDouble(values[TESTS_FILE_DEFINITION.MIN_VALUE.getColIndex()]);
			double maxValue = Double.parseDouble(values[TESTS_FILE_DEFINITION.MAX_VALUE.getColIndex()]);
			
			return new Test(name, price, unit, minValue, maxValue);
		} else {
			return null;
		}
	}
}
