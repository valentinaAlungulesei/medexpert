package com.medExpert.util;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import com.medExpert.mvc.model.Test;

public class TestsListStore {

	private static TestsListStore instance;
	private List<Test> testList;
	private TreeMap<String, Double> testPricesMap;
	
	private TestsListStore() {
		testList = new ArrayList<Test>();
		testPricesMap = new TreeMap<String, Double>();
	}
	
	public static TestsListStore getInstance() {
		synchronized (TestsListStore.class) {
			if (instance == null) {
				instance = new TestsListStore();
			}
		}
		return instance;
	}
	
	public void addTestToList(Test test) {
		testList.add(test);
		addTestToPricesMap(test);
	}
	
	public List<Test> getTestList() {
		return testList;
	}
	
	public TreeMap<String, Double> getTestPricesMap() {
		return testPricesMap;
	}
	
	public Test getTestByName(String name) {
		for (Test test : getTestList()) {
			if(name.equals(test.getName())) {
				return test;
			}
		}
		return null;
	}
	
	private void addTestToPricesMap(Test test) {
		testPricesMap.put(test.getName(), test.getPrice());
	}
}
