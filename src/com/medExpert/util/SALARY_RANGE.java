package com.medExpert.util;

public enum SALARY_RANGE {
	
	ENTRY_LEVEL("$10,000 - $30,000"),
	MIDDLE("$30,000 - $50,000"),
	UPPER("$50,000 - $80,000"),
	HIGH("$80,000 - $100,000");
	
	private String salary;
	
	private SALARY_RANGE(String salary) {
		this.salary = salary;
	}
	
	public String getSalary() {
		return salary;
	}
	
	public static SALARY_RANGE getBySalaryRange(String salary) {
		for (SALARY_RANGE salary_range : SALARY_RANGE.values()) {
			if (salary.equals(salary_range.getSalary())) {
				return salary_range;
			}
		}
		return null;
	}
}
