package com.medExpert.util;

public enum DESIGNATION {
	
	DOCTOR("Doctor"),
	MANAGER("Manager");

	private String name;
	
	private DESIGNATION(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static DESIGNATION getByName(String name) {
		for (DESIGNATION designation : DESIGNATION.values()) {
			if (name.equals(designation.getName())) {
				return designation;
			}
		}
		return null;
	}
}
