package com.medExpert.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.medExpert.exceptions.InvalidTestsFileException;
import com.medExpert.mvc.model.Test;

public class TestDataReader {
	
	private static TestDataReader instance;
	
	private TestDataReader() {}
	
	public static TestDataReader getInstance() {
		synchronized (TestDataReader.class) {
			if (instance == null) {
				instance = new TestDataReader();
			}
		}
		return instance;
	}
	
	public void readFromFile() {
		File file = new File(APath.TESTS_TXT);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = null;
			String[] lineValues;
			int count = 1;
			
			while ((line = reader.readLine()) != null) {
				lineValues = line.split(",");
				trimEmptySpaces(lineValues);
				
				if (count == 1) {
					validateFileStructure(lineValues);
				} else {
					Test test = TestFactory.getInstance().createTest(lineValues);
					TestsListStore.getInstance().addTestToList(test);
				}
				count++;
			}
			reader.close();
			
		} catch (IOException e) {
			System.err.println("There was an error with reading tests data.");
			e.printStackTrace();
		} catch (InvalidTestsFileException e) {
			e.printStackTrace();
		}
		
	}

	private void validateFileStructure(String[] lineValues) throws InvalidTestsFileException {
		for (int colIndex = 0; colIndex < TESTS_FILE_DEFINITION.values().length; colIndex++) {
			if (!lineValues[colIndex].matches(TESTS_FILE_DEFINITION.values()[colIndex].getColName())) {
				throw new InvalidTestsFileException("Invalid file structure for 'tests.txt' (Column " 
						+ (colIndex + 1)
						+ ").");
			}
		}
	}

	private void trimEmptySpaces(String[] lineValues) {
		for(int index = 0; index < lineValues.length; index++) {
			lineValues[index] = lineValues[index].trim();
		}
	}
}
