package com.medExpert.util;

public abstract class AMessages {

	public final static String EMPTY_FIELD = "Fields marked with * must be completed.";
	public final static String LIMIT_13_TOOLTIP = "Personal ID cannot contain more than 13 digits.";
	public final static String ID_SIZE = "Personal ID must contain 13 digits.";
	public final static String WRONG_CREDENTIALS = "Wrong password or personal ID.";
	public final static String NOT_STRONG_PASSWORD = "Password is not strong enought.";
	public final static String DIFFERENT_PASSWORD = "Passwords do not match.";
	public final static String INVALID_FIRST_NAME = "First name must contain only letters.";
	public final static String INVALID_LAST_NAME = "Last name must contain only letters.";
	public final static String COMBO_BOX_SELECTION = "Empty selection.";
	public final static String LOGIN_WELCOME = "You have been logged in successfully!";
	public final static String WEEKEND_DAYS = "An appointment cannot be scheduled on weekends.";
	public final static String PAST_DAYS = "An appointment cannot be scheduled in the past.";
}
