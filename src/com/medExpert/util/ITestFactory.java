package com.medExpert.util;

import com.medExpert.mvc.model.Test;

public interface ITestFactory {
	
	public Test createTest(String[] values);
}
