# <p align="center"> 🧬 MedExpert<p>

# 📃 Table of contents
1. *Project description*
2. *Languages*
3. *Programming concepts*
4. *Clients side UI*
5. *Employees side UI*

# 1. 🔬 Project description
**MedExpert**  is a java based application designed to be used by both clients and employees of a medical tests laboratory.
The clients side project's usage consists in creating user accounts, viewing available tests, their prices, results or good to know tips and making appointments for selected tests. In addintion to that, the employees side has a "Enter results" function. Althought the project's usage was just mentioned, the main purpose of it was to learn and practice concepts like OOP, MVC architecture, Design Patterns or interaction with a DB.

# 2. 💻 Languages
The project was developed with the following languages:
* <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="Java icon" width="30" height="30"/> Java, Swing
* <img src="https://www.vectorlogo.zone/logos/sqlite/sqlite-icon.svg" alt="SQLite icon" width="30" height="30"/> SQLite

# 3. ⚙ Programming concepts
Are implemented concepts like:
* MVC architecture
* OOP
* Design Patterns: Sigletone, Abstract Factory, Listener
* Threading

<p align="center"><img src="res/log_out_progress_bar.gif" alt="Log out progress bar" height=160><p>

* Regex

````java
	/**
	 * @param passwordInput
	 * @return true, if passwordInput has at least 1 digit, 1 lower case, 1 upper case,
	 *  	   1 special char, has no white spacing and contains from 5 to 20 chars
	 */
	public boolean checkPasswordComplexity(String passwordInput) {
		String complexityRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{5,}$";
		
		if (passwordInput.matches(complexityRegex)) {
			return true;
		}
		return false;	
	}
````

* Hashing data

<p align="center"><img src="res/personal_data_hashes.png" alt="Personal data hashes"  height=50><p>

* JUnit Testing

# 4. 👤 Clients side UI
* Menu presentation: 

<p align="center"><img src="res/clients_menu_ui.gif" alt="Clients menu UI"  height=340><p>


# 5. 🧪 Employees side UI
* Menu presentation: 

<p align="center"><img src="res/employees_menu_ui.gif" alt="Employees menu UI"  height=340><p>

* Sign in:

<p align="center"><img src="res/employee_sign_in.gif" alt="Employee sign in UI" heigh=340><p>